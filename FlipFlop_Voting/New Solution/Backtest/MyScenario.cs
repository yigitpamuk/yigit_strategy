using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;


namespace OpenQuant
{
	public partial class Scenerio : Scenario
	{
		List<string> listCurrencies = new List<string> {"EURUSD"};
		private long barSize = 60 * 60;

		public Scenerio(Framework framework)
			: base(framework)
		{
		}

		public override void Run()
		{
			Console.WriteLine("here");
			StrategyManager.Mode = StrategyMode.Backtest;
			//	StrategyManager.Persistence = StrategyPersistence.Full;
			strategy = new MyStrategy(framework, "Backtest");
			BarFactory.Clear();

			//strategy.DataProvider = ProviderManager.GetDataProvider("IB");

			foreach (string curr in listCurrencies)
			{
				Instrument instrument = InstrumentManager.GetBySymbol(curr);
				strategy.AddInstrument(instrument, strategy.DataProvider);
				BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle, 0);
			}

			DataSimulator.DateTime1 = new DateTime(2011, 1, 1);
			DataSimulator.DateTime2 = new DateTime(2020, 11, 1);
			//Framework.EventBus.ReminderOrder = ReminderOrder.After;
			// strategy.ExecutionSimulator.CommissionProvider.Type = CommissionType.Percent;

			// strategy.ExecutionSimulator.CommissionProvider.Commission = 0.00002;//slippage and comission
			// strategy.ExecutionSimulator.SlippageProvider.Slippage= 0.00002;

			//strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");

			Console.WriteLine("here");
			StartStrategy();
		}
	}
}

