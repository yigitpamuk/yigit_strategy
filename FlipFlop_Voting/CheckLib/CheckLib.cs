using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;
using Genesis;

namespace CheckLib
{

	public static class MarketCheck
	{

		public static  int checkMarketTimeandPrice(Instrument instrument, double latest_bid, double latest_ask, DateTime currentTime, Clock Clock)

		{
			// this function will be called by OnBid, OnAsk and OnBar 

			//10-Year U.S. Treasury Note Futures	PreOpenSunday 16:00	Sunday 17:00-16:00	PreOpenWeekDay 16:45	Weekday 17:00-16:00
			int checker=0;

			if (latest_bid<=0 || latest_ask<=0)
			{
				checker++;
				myConsole.WriteLine("Bid and Ask data is not healty");
			}
			
			TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
			DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);
			DateTime nowTime = Clock.DateTime;
			TimeSpan span = nowTime.Subtract(currentTime);
			// if the time passes this Date we should not enter any positions and consider starting out new algo

			if (span.Minutes > 5){
				myConsole.WriteLine("No Updates for 5 Minutes");
				checker++;	
			}
			if ((USTime.DayOfWeek==DayOfWeek.Saturday))
			{
				myConsole.WriteLine("Market Is Closed DoW Condition");
				checker++;	
			}
			
			if ((USTime.DayOfWeek==DayOfWeek.Friday) && (USTime.Hour>=17 ))
			{
				myConsole.WriteLine("Market Is Closed Friday Condition");
				checker++;	
			}			
			
			if ((USTime.DayOfWeek==DayOfWeek.Sunday) && (USTime.Hour <17 && USTime.Minute <16))
			{
				myConsole.WriteLine("Market Is Closed Sunday Condition");
				checker++;	
			}
			
			if (USTime.Hour==17)
				if (USTime.Minute<16)
				{
					myConsole.WriteLine("Market Is Closed Weekday Close Condition");
					checker++;	
				}
			

			if (checker>0){
				return 0;
			}

			else
			{
				return 1;
			}
		}


	

	}
	
	public static class myConsole
	{
	    
		public static void WriteLine(string str)
		{
			bool debug=false;
			if (debug==true)
			{
                Logger logger = new Logger("C:\\Users\\yigit.pamuk\\oq_logs\\flipflop\\log1.txt", true);
                logger.Print(str);
				logger.Dispose();
			}
		}

	}


}












