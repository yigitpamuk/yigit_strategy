using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;


using CheckLib;
using Genesis;

namespace OpenQuant
{
	public partial class Scenerio : Scenario
	{	
		List<string> listCurrencies = new List<string> {"EURUSD"};

		
		
		private long barSize = 5*60;
		public Scenerio(Framework framework)
			: base(framework)
		{
		}
		
		public override void Run()
		{	

			//StrategyManager.Mode = StrategyMode.Live;
			StrategyManager.Mode = StrategyMode.Backtest;
            //	StrategyManager.Persistence = StrategyPersistence.Full;

     
           
            string datesting = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() +"_"+ DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();
            string filename = datesting + "_BACKTEST_LOGS.txt";
            string location = "C:\\Users\\yigit.pamuk\\oq_logs\\flipflop\\" + filename;
            Debugger mydeBug = new Debugger(location, false);
			
			string name = "FlipFlop_1_Live";
            strategy = new MyStrategy(framework, name,mydeBug );
			BarFactory.Clear();
			
			//strategy.DataProvider = ProviderManager.GetDataProvider("IB");

			foreach (string curr in listCurrencies)
			{
				Instrument instrument = InstrumentManager.GetBySymbol(curr);
				Console.WriteLine(curr);
				strategy.AddInstrument(instrument, strategy.DataProvider);
				BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle,0);		
			}
			
			DataSimulator.DateTime1 = new DateTime(2015, 1, 1);
			DataSimulator.DateTime2 = new DateTime(2020, 1, 1);
			//Framework.EventBus.ReminderOrder = ReminderOrder.After;
			strategy.ExecutionSimulator.CommissionProvider.Type = CommissionType.Percent;
			strategy.ExecutionSimulator.CommissionProvider.MinCommission  = 2;
			strategy.ExecutionSimulator.CommissionProvider.Commission = 0.00002;//slippage and comission
			strategy.ExecutionSimulator.SlippageProvider.Slippage= 0.00002;
			
			//strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");
			StartStrategy();
		}

	}
}



















































