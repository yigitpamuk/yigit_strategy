using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;


namespace OpenQuant
{
    public class MyStrategy : Strategy
    {



        public MyStrategy(Framework framework, string name)
            : base(framework, name)
        {

        }




        protected override void OnStrategyStart()
        {


			AddReminder(Clock.DateTime.AddSeconds(5));


        }

        protected override void OnBid(Instrument instrument, Bid bid)
        {
			

        }

		protected override void OnReminder(DateTime signalTime, object data)
		{
            Console.WriteLine("<-------------Heartbeat "+ Clock.DateTime + " ---------------------->");
			AddReminder(Clock.DateTime.AddSeconds(5));
			
		}


        protected override void OnAsk(Instrument instrument, Ask ask)
        {

		}


        protected override void OnBar (Instrument   instrument  , Bar bar)
        {

            BarSeries historicalBars = getBarsForMe(instrument);

            Console.WriteLine("Bars Downloaded Count: "+ historicalBars.Count);
		}

        public  BarSeries getBarsForMe(Instrument instrument)
        {
            DateTime historicalEnd = Clock.DateTime;
            DateTime start = Clock.DateTime.AddHours((20000 )*-1).Date;
            BarSeries response = new BarSeries();
            if (StrategyManager.Mode == StrategyMode.Live)

            {

                response = DataManager.GetHistoricalBars("IB", instrument, start, historicalEnd, BarType.Time, 60 * 60);


            }
            else
            {

                response = DataManager.GetHistoricalBars(instrument, start, historicalEnd, BarType.Time, 60 * 60);

            }

            return response;


        }

    }
}






































