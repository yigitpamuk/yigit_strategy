using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;


namespace OpenQuant
{
	public partial class Realtime : Scenario
	{	
		List<string> listCurrencies = new List<string> {"EURUSD"};

		
		
		private long barSize = 60*5;
		public Realtime(Framework framework)
			: base(framework)
		{
		}
		
		public override void Run()
		{

            StrategyManager.Mode = StrategyMode.Live;
            
			string name = "dtester";

            strategy = new MyStrategy(framework, name );
			BarFactory.Clear();
			
			strategy.DataProvider = ProviderManager.GetDataProvider("IB");

			foreach (string curr in listCurrencies)
			{
				Instrument instrument = InstrumentManager.GetBySymbol(curr);
				strategy.AddInstrument(instrument, strategy.DataProvider);
				BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle,0);		
			}
 

            StartStrategy();
		}

	}
}





















































