using System;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;
using CalcLib;
using UtilLib;
using QuantLib;
//using SmartQuant.IB;
//using MathNet.Numerics;
//using QuantCalcs;
//using Execution;
//using Genesis;
namespace OpenQuant
{
    public class MyStrategy : Strategy
    {
		// STRATEGY PARAMETER SETS		
		public List<int> LookBacks = new List<int>(){200,500,800};
		public List<int> Exits = new List<int>(){200,400};
		public List<double> Bands = new List<double>(){2,3};
		public List<int> RecalculateIntervals = new List<int>(){100};
		public List<double> ComissionTholds = new List<double>(){1.5,2};

		[Parameter]
		public double MoneyForInstrument = 100000;
		
		[Parameter]
		public double  illogicThold= 0.2;		//remove the datapoint if it is more than this amount of current price
		
		[Parameter]
		public double  lotExposure= 1;		//Amount of Lots to trade per substrategy for classifier, the features will be scaled according to this value


		DictShapers myDictShaper = new DictShapers();
		CombinGenerator Combinator = new CombinGenerator();
		SubStrategy SubStrat = new SubStrategy();
		TimeOperations TimeOps = new TimeOperations();
		DataProcess DataOps = new DataProcess();


		public Dictionary<Instrument,double> bidDict;
		public Dictionary<Instrument,double> askDict;
		public Dictionary<Instrument,int> marketAskDict;
		public Dictionary<Instrument,int> marketBidDict;
		public Dictionary<int,Instrument> idDict;
		public Dictionary<int,double> tickConversion;


		public List<List<int>> combinListnew;
		public List<SubStrategy> subStratList;
		public List<Instrument> instrumentCollection;
		
		List<List<int>> filterListStrat = new List<List<int>>();
		public List<Dictionary<DateTime,Dictionary<Instrument,double>>> priceDictList;


		
		public DateTime LastUpdate;
		public DateTime CurrentTime;
		public int CallCounter;
		// OnBar updates  list of SubStrategies
		// Creates Total Target and rounds the values
		// send the total target to executor engine
		// executor engine enters the trades


		
		public MyStrategy(Framework framework, string name, List<List<int>> filterList)
            : base(framework, name)
        {
			filterListStrat=filterList;
		}
		

		
	    public void setTickConversions()
	    {
			tickConversion = new Dictionary<int,double>();
	    	double tVal = 31.25; // (1/32)*31.25

	    	tickConversion[30]=tVal;
	    	tickConversion[31]=tVal;
	    	tickConversion[38]=tVal;
	    	tickConversion[39]=tVal*2;

	    }

	    public void initBidAskIdIns()
	    {
			bidDict = new Dictionary<Instrument,double>();
			askDict = new Dictionary<Instrument,double>();
			idDict = new Dictionary<int,Instrument>();
			marketAskDict = new  Dictionary<Instrument,int>();
			marketBidDict = new  Dictionary<Instrument,int>();
			instrumentCollection = new List<Instrument>();
			foreach (Instrument instrument in Instruments)
			{
		
				bidDict[instrument]=0;
				askDict[instrument]=0;
				idDict[instrument.Id]=instrument;
				marketBidDict[instrument]=0;
				marketAskDict[instrument]=0;
				instrumentCollection.Add(instrument);
		
			}

			
	    }


		protected override void OnStrategyStart()
		{
			Console.WriteLine("Init STRAT");
			
			CallCounter=0;
			CurrentTime = Clock.DateTime;

			// initbidask

			initBidAskIdIns();

			// a list of lists with the classifier in the beginning

			combinListnew=  Combinator.generateCombinationsAndFilter(filterListStrat,instrumentCollection);

			// add tick conversion factors to tickdictionary

			setTickConversions();

			//generate parameter combinations for all lookbacks
			subStratList=SubStrat.initSubStrategies(combinListnew, idDict ,LookBacks, Bands, ComissionTholds, RecalculateIntervals, Exits );
		// STRATEGY PARAMETER SETS		

		//	List parameter, lookback , exit , band, recalc, comissiomn

	}

		
		public  void  marketCheck(Instrument instrument, int Caller)
		{
			if (StrategyManager.Mode == StrategyMode.Live)
			{ 
			// CHECK THE BID AND ASK AND IF THEY ARE HEALTY REALTIME //TO Do ADD SCHEDULE FROM IB

				double latest_bid=askDict[instrument];
				double latest_ask=bidDict[instrument];
				int checkerInt = TimeOps.checkMarketTimeandPrice(instrument,latest_bid,latest_ask,CurrentTime);
				if (Caller==2 && checkerInt==0){
					marketBidDict[instrument]=0;
				}
				else if (Caller==2 && checkerInt==1){
					marketBidDict[instrument]=1;
				}
				else if (Caller==1 && checkerInt==0){
					marketAskDict[instrument]=0;
				}
				else if (Caller==1 && checkerInt==1){
					marketAskDict[instrument]=1;
				}
				else if (Caller==3 && checkerInt==0){
					marketAskDict[instrument]=0;
					marketBidDict[instrument]=0;
				}
				else if (Caller==3 && checkerInt==1){
					marketAskDict[instrument]=1;
					marketBidDict[instrument]=1;
				}

			}
			else
			{
				marketAskDict[instrument]=1;
				marketBidDict[instrument]=1;
			}	
		}   

		protected override void OnAsk(Instrument instrument, Ask ask)
		{
			askDict[instrument]=instrument.Ask.Price;			
			if (instrument.Ask.DateTime>CurrentTime && instrument.Ask.Price>0)
			{
				CurrentTime=instrument.Ask.DateTime;
		
				
			}
			marketCheck(instrument,1);
			
		}


		protected override void OnBid(Instrument instrument, Bid bid)
        {

			bidDict[instrument]=instrument.Bid.Price;
			if (instrument.Bid.DateTime>CurrentTime && instrument.Bid.Price>0)
			{
				CurrentTime=instrument.Bid.DateTime;

			}
			marketCheck(instrument,2);


        }
		
		private BarSeries GetBarsForMe(Instrument instrument, int lookback_hours)
			{
				DateTime historicalEnd = DateTime.Now;
				DateTime start = DateTime.Now.AddHours((lookback_hours+50)*-1).Date;

				if (StrategyManager.Mode == StrategyMode.Live)
				
				{
					
					return 	DataManager.GetHistoricalBars("IB",instrument, start, historicalEnd, BarType.Time, 60*60);
				}
				else 
				{
					Console.WriteLine("Not Implemented Path Returns!");
						
					return 	DataManager.GetHistoricalBars("IB",instrument, start, historicalEnd, BarType.Time, 60*60);
			
				}
			}
		
		
		protected override void OnBar (Instrument instrument, Bar bar)
		{	
			// check ask and bid dict and see if the sum up to 1 TO DO EXECUTION
			CallCounter++;
			BarSeries historicalBars = GetBarsForMe(instrument,LookBacks.Max()); 
			Dictionary<DateTime,Dictionary<Instrument,double>> dataDict =  myDictShaper.ConvertBarsToDict(instrument,historicalBars);
			marketCheck(instrument,3);//check the market time and latest bid ask
			if (CallCounter==1)
				{			 
					priceDictList = new  List<Dictionary<DateTime,Dictionary<Instrument,double>>>();
					priceDictList.Add(myDictShaper.checkDatapoints(dataDict,illogicThold,askDict,bidDict));
					
				}
			else if (CallCounter<Instruments.Count)
				{	
					
					priceDictList.Add(myDictShaper.checkDatapoints(dataDict,illogicThold,askDict,bidDict));
						
				}
			else
				{			 
					priceDictList.Add(myDictShaper.checkDatapoints(dataDict,illogicThold,askDict,bidDict));
					CallCounter=0;	
					
					//get aligned list aligned accounding to instrument list order
					List<List<double>> alignedlist = myDictShaper.alignedListGenerator(priceDictList,instrumentCollection,false);
					Console.WriteLine("Aligned List Generated");
	

					Console.WriteLine("-------------------------------------- ");

					Dictionary<Instrument,double []> vectorDict = new Dictionary<Instrument,double []>();
					vectorDict=DataOps.getTheoDollar(alignedlist , tickConversion,instrumentCollection, false);

					// for each SubStrategy , iterate, filter the arrays and generate coefficients


					// for each combination set prepare tick adjusted series
					foreach (SubStrategy  subS in subStratList){
						//generate feature array and classifier array for instruments
						Instrument classifier = subS.classifierIns;
						List<Instrument> featuresList = subS.featureInstruments;
						List<double[]> featuresArrayList = new List<double[]>(); 
						foreach (Instrument ins in featuresList){
							featuresArrayList.Add(vectorDict[ins]);
						}
						
						double[,] resArray = new double[featuresList.Count,featuresArrayList[0].Length];
						resArray = myDictShaper.To2DArray(featuresArrayList);
						Console.WriteLine("called");
						//classifierArray = vectorDict[]

						// update the following parameters
						// public double comissionCosts {get;set;} 
						//	public List<double> featWeights {get;set;}
						// public List<double> targetFeat {get;set;}
						// public double targetClass {get;set;}
						// public double stdevofResid {get;set;}
						//public int incrementSinceUpdate {get;set;}
						//public DateTime LastUpdated {get;set;}

					}
				
						//regress according to coefficients
						//regress according to coefficients
				

					
				}
			
		
			}

    }
}













































