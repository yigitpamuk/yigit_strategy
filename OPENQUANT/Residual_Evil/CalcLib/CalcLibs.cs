using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using SmartQuant;
namespace CalcLib
{
	public  class CombinGenerator
	{
		public  bool NextCombination(IList<int> num, int n, int k)
		{
			bool finished;
 
			var changed = finished = false;
 
			if (k <= 0) return false;
 
			for (var i = k - 1; !finished && !changed; i--)
			{
				if (num[i] < n - 1 - (k - 1) + i)
				{
					num[i]++;
 
					if (i < k - 1)
						for (var j = i + 1; j < k; j++)
							num[j] = num[j - 1] + 1;
					changed = true;
				}
				finished = i == 0;
			}
 
			return changed;
		}
 
		public  IEnumerable Combinations<T>(IEnumerable<T> elements, int k)
		{
			var elem = elements.ToArray();
			var size = elem.Length;
 
			if (k > size) yield break;
 
			var numbers = new int[k];
 
			for (var i = 0; i < k; i++)
				numbers[i] = i;
 
			do
			{
				yield return numbers.Select(n => elem[n]);
			} while (NextCombination(numbers, size, k));
		}
 

	

	public List<List<int>> generateCombinationsAndFilter(List<List<int>> filterListStrat, List<Instrument> Instruments)
		{
			//CombinGenerator CombinGener = new CombinGenerator();
			List<List<int>> combinList = new List<List<int>>();
					
			foreach (Instrument insa in Instruments){
				
				List<int> iterList = new List<int>();
				
				foreach (Instrument insb in Instruments){
					if (insa!=insb){
			
						iterList.Add(insb.Id);
					}
				}
				
			for ( int ii=1; ii<=3 ; ii++)
			{    
				foreach (IEnumerable<int> i in Combinations(iterList, ii)){
					List<int> subcomb_list = new List<int>();
					subcomb_list.Add(insa.Id);
					foreach (int insid in i ){
						subcomb_list.Add(insid);
							
					}
					combinList.Add(subcomb_list);
				}
			}
		
			}
			
			
				
			Console.WriteLine("Total Instrument Combinations Generated: " + combinList.Count);
			List<List<int>> combinListnew = new List<List<int>>();
			// Filter the combinations:
			foreach (List<int> comb in combinList){

				foreach (List<int> filter in filterListStrat){
					if (filter.SequenceEqual(comb)){
						combinListnew.Add(comb);
					}
				}
				}
				
			Console.WriteLine("Total Instrument Combinations Used: " + combinListnew.Count);
			Console.WriteLine("-------------------------------------- ");
			Console.WriteLine("Instrument Combination List: ");
			foreach (List<int> comb in combinListnew){
				foreach (int i in comb ){
					Console.Write(i + " ");
				}
				Console.WriteLine(" ");
	        }
	        Console.WriteLine("-------------------------------------- ");
	        return combinListnew;
	    }
    }

}





