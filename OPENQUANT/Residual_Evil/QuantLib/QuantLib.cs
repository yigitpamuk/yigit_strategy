using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilLib;
using SmartQuant;

namespace QuantLib
{
    public class DataProcess 
    {

		public Dictionary<Instrument,double []> getTheoDollar  ( List<List<double>> alignedlist , Dictionary<int,double> tickConversion,List<Instrument> Instruments,bool verbose = false)
		{
			int columnCount=Instruments.Count;
			// first convert the aligned list in to jagged array
			double[][] priceVector = alignedlist.Select(a => a.ToArray()).ToArray();
			double[,] theoDollarDiffVector = new double[columnCount,priceVector.Length-1];
			double[,] theoDollarVector = new double[columnCount,priceVector.Length-2];
			
			// loop through each columns and create a diff jagged array
			Dictionary<Instrument,double []> arrayDict = new Dictionary<Instrument,double []>();	
			for (int col=0; col<columnCount ; col++){				
				double oldVal=0;
				int instrumentID=Instruments[col].Id;
				double conversionFactor=tickConversion[instrumentID];

				for (int row=0; row<priceVector.Length; row++){
					
					if (row==0){
						oldVal = priceVector[row][col]; 

					}
					else
					{
						double latestVal = priceVector[row][col];
						double dollarDiff=((latestVal-oldVal)/(0.03125))*conversionFactor;
						theoDollarDiffVector[col,row-1]=dollarDiff;
						oldVal=latestVal;
					}		
				}
			}
			


			//generate a cumsum list from tick conversion values
			int cols = theoDollarDiffVector.GetLength(0);
			int rows = theoDollarDiffVector.GetLength(1);
			for (int col=0; col<cols ; col++){				
				double oldVal=0;
				for (int row=0; row<rows; row++){
		
					if (row==0){
						oldVal = theoDollarDiffVector[col,row]; 
					}
					else
					{
						double latestVal = theoDollarDiffVector[col,row];
						theoDollarVector[col,row-1]=oldVal+latestVal;
						oldVal=latestVal+oldVal;
					}		
				}
			}

			int colsa = theoDollarVector.GetLength(0);
			int rowsa = theoDollarVector.GetLength(1);
			//verbose

			if (verbose == true){


			for (int i=0; i<rowsa ; i++)
  			{
    			for (int j=0; j<colsa ; j++)
    			{
      				Console.Write(string.Format(theoDollarVector[j, i].ToString() + " "));
			    }
    			Console.WriteLine();
  			}

			}
			// create a dictionary that contains each individual arrays 
			for (int j=0; j<colsa ; j++)
			{
				double[] myArray = new double[rows]; 
				Instrument ins = Instruments[j];
				for (int i=0; i<rowsa ; i++) 
				{
					myArray[i]=theoDollarVector[j,i];

      				
			 	}
			 	arrayDict[ins]=myArray;

			}


			Console.WriteLine(" Theoritical Dollar Delta Array Dict Populated");
			//get classifier and feature dict from this theo dollar list

			//return this dict
			return arrayDict;

		}

	}
	public class QuantProcess
	{

		
	} 
    
}

