using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;
using System.Web.Script.Serialization;
namespace UtilLib
{
    public class DictShapers 
    {
 	
    	public double[,]  To2DArray(List<double[]> arrayList)
		{
	    	
			int rows =arrayList[0].Length; 
			int cols =arrayList.Count;

			double[,] combinedArray = new double[cols,rows];
	    	for (int i = 0; i < rows; i++) 
	    	{
	    		for (int j = 0; j < cols; j++)
	    		{

	    			combinedArray[j,i]=arrayList[j][i];
	    		} 

	    	}
			
	    	return combinedArray;
	    }



		public Dictionary<DateTime,Dictionary<Instrument,double>> ConvertBarsToDict(Instrument instrument, BarSeries myseries)
		{			
			// Convert Bar Objects to Dictionary
			Dictionary<DateTime,Dictionary<Instrument,double>> histDict = new Dictionary<DateTime,Dictionary<Instrument,double>>();
			Dictionary<Instrument, BarSeries> historicalBarDict = new Dictionary<Instrument,BarSeries>();
			List<DateTime> dateList = new List<DateTime>();
		
			historicalBarDict.Add(instrument,myseries);
		
			Console.WriteLine(String.Format("Got historical bars for : {0} {1}",instrument, myseries.Count));
			foreach(var histBar in historicalBarDict){
				foreach(Bar bar in histBar.Value){
					bool isInDateList = false;
					
				//CHECK DUPLICATES
					foreach(DateTime date in dateList){
						if(date==bar.CloseDateTime){
							isInDateList=true;
						}
					}
					if(isInDateList==false){
						dateList.Add(bar.CloseDateTime);
						histDict.Add(bar.CloseDateTime,new Dictionary<Instrument, double>());
					}
					
				}
			}
			
			foreach(var histBar in historicalBarDict)
			{
				foreach(Bar bar in histBar.Value){
					histDict[bar.CloseDateTime].Add(histBar.Key,bar.Close);
				}
				
			}
			
			return histDict;
		
		}    

		public Dictionary<DateTime,Dictionary<Instrument,double>> checkDatapoints (Dictionary<DateTime,Dictionary<Instrument,double>> input_dict ,
		 double illogicalThold, Dictionary<Instrument,double> askDict, Dictionary<Instrument,double> bidDict)
		{	
			//clean the ib data
			List<DateTime> DelList = new List<DateTime>();
			foreach (DateTime keyd in input_dict.Keys ){
				foreach (Instrument key in input_dict[keyd].Keys ){
					double myval = input_dict[keyd][key];
					int check_int = checkVal(myval,illogicalThold,key,keyd,askDict,bidDict);
					if (check_int==0){
						DelList.Add(keyd);
					}		
				}
			}
			

			if (DelList.Count>0){
				foreach (DateTime deldate in DelList){
					input_dict.Remove(deldate);
				}
			}

			return  input_dict;
			
		}

		public int checkVal (double myval,double thold, Instrument instrument,DateTime mydate, 
			Dictionary<Instrument,double> askDict, Dictionary<Instrument,double> bidDict){
			//check the zeros and illogicla values in the dict
			if (mydate> DateTime.Now){
				Console.WriteLine(String.Format("Forward Date Detected! Instrument: {0} DateTime: {1} Value {2} ",instrument,mydate,myval));
				return 0;
			}
			
			else if (myval<=0){
				Console.WriteLine(String.Format("Neg or Zero Value Detected! Instrument: {0} DateTime: {1} Value {2} ",instrument,mydate,myval));
				return 0;
			}
			else
			{
				double last_mid = (askDict[instrument]+bidDict[instrument])/2;
				double difference = Math.Abs(myval - last_mid)/myval;
				
				if (difference>thold)
				{
					Console.WriteLine(String.Format("Illogical Value Detected! Instrument: {0} DateTime: {1} Value {2} Last Mid {3} ",instrument,mydate,myval,last_mid));
					return 0; 
				}
				else
				{	
					return 1;
				}
			}				
		}
		public List<List<double>>  alignedListGenerator (List<Dictionary<DateTime,Dictionary<Instrument,double>>> input_dict, List<Instrument> Instruments , bool verbose=false)
		{
			HashSet<DateTime> h1 = new HashSet<DateTime>();
			//Generate uniq keys

			foreach (var dicts  in input_dict){
				{
				h1.UnionWith(dicts.Keys);
			}
			
				}
			
			
			//now for each datetime, we create a classifier array and feature array and label list
			// this array list will go to numerification and regression.
			
			List<List<double>> glob_list = new List<List<double>>();
			foreach (DateTime k in h1)
			{
				//if all available, than append it to dictionary
				int counter=0;
				foreach (var dicts  in input_dict){
					if (dicts.ContainsKey(k)){
						counter++;
					}
				}

				if (counter==Instruments.Count){
					List<double> sub_list = new List<double>();
					foreach (Instrument ins in Instruments){
						foreach (var dicts  in input_dict){
							//check if key is exists, if not than skip this date for all instruments.
							if (dicts[k].ContainsKey(ins)){
								sub_list.Add(dicts[k][ins]);
							}
						}
						
						
					}
					
					glob_list.Add(sub_list);
					
				}
				
			}
			if (verbose == true){
			foreach (List<double> row in glob_list)
  			{
    			foreach (double element in row)
    			{
      			Console.Write(element.ToString() + " ");
			    }
    		Console.WriteLine();
  			}
			}



			return glob_list;

			
		}
	}


	public  class SubStrategy
	// This class is the backbone for multi strategy implementation, we will loop throgh list of SubStrategies
		{
			public int classifierId {get;set;}
			public Instrument classifierIns {get;set;}
				
		    		
			public List<int> featureIds {get;set;}
			public List<Instrument> featureInstruments {get;set;}
			
			public int lookBack {get;set;}
			public double bandWidth {get;set;}
			public double comissionThold{get;set;}
			public int recalcInterval {get;set;}
			public int exitLimit {get;set;}
			public DateTime LastUpdated {get;set;}  		
			public int inpos {get;set;}	
			public int incrementSinceUpdate {get;set;}

			//below cannot be filled before we actually process the data
			
			public List<double> featWeights {get;set;}
			public List<double> targetFeat {get;set;}
			public double targetClass {get;set;}
			


			public double comissionCosts {get;set;} 
			public double stdevofResid {get;set;}
			public int side {get;set;}
	

		
		public List<SubStrategy> initSubStrategies(List<List<int>> combinlist,  Dictionary<int,Instrument> idDict ,
			// creates a list of SubStrats for the beginning
			List<int> lookBacks, List<double> bandWidths, List<double> comissionTholds, List<int> recalcIntervals, List<int> exitLimits )
			{
				List<SubStrategy> resultList = new List<SubStrategy>(); 
				foreach (List<int> combinline in  combinlist){


					List<int> featids = new List<int>();
					List<Instrument> featInstruments = new List<Instrument>();
					
					List<double> featW = new List<double>();
					List<double> targetF = new List<double>();


					for (int i=1; i<combinline.Count; i++ ){
						featids.Add(combinline[i]);
						featInstruments.Add(idDict[combinline[i]]);
						featW.Add(0);
						targetF.Add(0);
					}

					foreach (int lb in lookBacks ){
						foreach (double bw in bandWidths ){
							foreach (double cthold in comissionTholds ){
								foreach (int rcalc in recalcIntervals ){
									foreach (int exit in exitLimits ){
										SubStrategy mySubStrat =  new SubStrategy();
										mySubStrat.classifierId=combinline[0];
										mySubStrat.classifierIns=idDict[mySubStrat.classifierId];
										mySubStrat.featureIds=featids;
										mySubStrat.featureInstruments=featInstruments;
										mySubStrat.LastUpdated= DateTime.Now;						
										mySubStrat.lookBack=lb;
										mySubStrat.bandWidth=bw;
										mySubStrat.comissionThold=cthold;
										mySubStrat.recalcInterval=rcalc;
										mySubStrat.exitLimit=exit;
										mySubStrat.featWeights = featW;
										mySubStrat.targetFeat = targetF;
										mySubStrat.incrementSinceUpdate= 0;
										mySubStrat.inpos= 0;
										mySubStrat.targetClass=0;
										mySubStrat.comissionCosts=0;
										mySubStrat.stdevofResid=0;
										mySubStrat.side=0;


										resultList.Add(mySubStrat);
									}
								}
							}
						}
					}


					}
			int mycheck = lookBacks.Count*bandWidths.Count*comissionTholds.Count*recalcIntervals.Count*exitLimits.Count* combinlist.Count;	
			Console.WriteLine(" Instrument Parameter Combinations Used: " + resultList.Count + " | CheckSum: " + mycheck);
			Console.WriteLine("--------------------------------");
			/*
			foreach (SubStrategy item in resultList){
				JavaScriptSerializer js = new JavaScriptSerializer();
  				string json = js.Serialize(item);
				Console.WriteLine(json );				}
			*/
			return resultList;
				
			}
		

		}

	public class TimeOperations
	{

		public int checkMarketTimeandPrice(Instrument instrument, double latest_bid, double latest_ask, DateTime currentTime)
		{
			// this function will be called by OnBid, OnAsk and OnBar 

			//10-Year U.S. Treasury Note Futures	PreOpenSunday 16:00	Sunday 17:00-16:00	PreOpenWeekDay 16:45	Weekday 17:00-16:00
			int checker=0;
			if (latest_bid<=0 && latest_ask<=0)
			{
				checker++;
				Console.WriteLine("Bid and Ask data is not healty");
			}
			
			TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
			DateTime USTime = TimeZoneInfo.ConvertTime(DateTime.Now.ToUniversalTime(), convertZone);
			DateTime nowTime = DateTime.Now;
			TimeSpan span = nowTime.Subtract(currentTime);


			if (span.Minutes > 5){
				Console.WriteLine("No Updates for 5 Minutes");
				checker++;	
			}
			if ((USTime.DayOfWeek==DayOfWeek.Saturday))
			{
				Console.WriteLine("Market Is Closed DoW Condition");
				checker++;	
			}
			
			if ((USTime.DayOfWeek==DayOfWeek.Friday) && (USTime.Hour>=16 ))
			{
				Console.WriteLine("Market Is Closed Friday Condition");
				checker++;	
			}			
			
			if ((USTime.DayOfWeek==DayOfWeek.Sunday) && (USTime.Hour <17))
			{
				Console.WriteLine("Market Is Closed Sunday Condition");
				checker++;	
			}
			
			if (USTime.Hour > 16 && USTime.Hour <17)
			{
				Console.WriteLine("Market Is Closed Weekday Close Condition");
				checker++;	
			}
			if (checker>0){
				return 0;
			}
			else
			{
				return 1;
			}
		}



	}

}


/*



 */



