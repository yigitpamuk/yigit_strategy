using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;
using SubStratLib;
using QuantLib;
using DataOpsLib;
using CheckLib;
using ExecutionLib;



namespace OpenQuant
{
    public class MyStrategy : Strategy
    {
        public double latestAsk;
        public double latestBid;

        public DateTime latestAskTime;
        public DateTime latestBidTime;
        public DateTime CurrentTime;


        double TOTAL_EXPOSURE = 5000;
        double illogicThold = 0.9;

        string StratName;
        public List<SubStrategy> subStratList;
        bool connected;
        List<List<string>> pairs = new List<List<string>>();
        List<int> lookBacks;
        Debugger mydebug;
        Dictionary<Instrument,BarSeries> dataBase;
        public MyStrategy(Framework framework, string name, Debugger mydeBug)
            : base(framework, name)

        {
            mydebug = mydeBug;
            StratName = name;
        }

        protected override void OnStrategyStart()
        {
            mydebug.WriteLine("STRATNAME: KEEP FOR PERSISTENCE: "+ StratName);
            connected = true;
            CurrentTime = Clock.DateTime;
            //init substrategies
            mydebug.WriteLine("Start " + CurrentTime.ToString());

            // STRATEGY PARAMETERS
			GetPositions();
			
           // List<string> p1 = new List<string>() { "EWA", "EWC" };
            List<string> p2 = new List<string>() { "EZA", "EEM" };
            List<string> p3 = new List<string>() { "EZA", "EWA" };
            List<string> p4 = new List<string>() { "EZA", "EWC" };
            //List<string> p5 = new List<string>() { "EWA", "EWT" };
            //List<string> p6 = new List<string>() { "EWA", "EWH" };
            //List<string> p7 = new List<string>() { "EZA", "EWH" };
            //List<string> p8 = new List<string>() { "EWS", "EZA" };
            lookBacks = new List<int>() { 21,21*3,21*6,21*9,21*12};
            //lookBacks = new List<int>() { 21*6,21*12,21*18,21*24};


           // pairs.Add(p1);
            pairs.Add(p2);
            pairs.Add(p3);
            pairs.Add(p4);
            //pairs.Add(p5);
            //pairs.Add(p6);
            //pairs.Add(p7);
            //pairs.Add(p8);



			mydebug.WriteLine("TOTAL EXPOSURE " + TOTAL_EXPOSURE.ToString());
            Portfolio.Account.Deposit(TOTAL_EXPOSURE, CurrencyId.USD, "Initial allocation");
            double combinations = (double)lookBacks.Count() * (double)pairs.Count();
            double allocationPerStrategy = TOTAL_EXPOSURE / combinations;
            subStratList = SubStrategyOps.initSubStrategies(pairs,lookBacks,InstrumentManager, allocationPerStrategy,mydebug);


            dataBase = new Dictionary<Instrument, BarSeries>();
            mydebug.WriteLine("Getting Init Data");

            if (StrategyManager.Mode == StrategyMode.Live)
            {
                AddReminder(Clock.DateTime.AddSeconds(60));
            }
                


            foreach (Instrument instrument in Instruments)
            {
                BarSeries mydata = DataOps.getBarsForMe(instrument, 500, StrategyManager, DataManager, Clock, illogicThold,mydebug);
                dataBase[instrument]=mydata;  
                
            }



        }
        protected override void OnReminder(DateTime signalTime, object data)
        {
            mydebug.WriteLine("<-------------Heartbeat " + Clock.DateTime + " ---------------------->");
            AddReminder(Clock.DateTime.AddSeconds(60));

        }
		
		public void GetPositions()
		{
			mydebug.WriteLine("------Positions-------" + DateTime.Now+"--------reconcile---------");
			foreach (Instrument ins in Instruments)
			{
				Instrument myins = ins;
    
				if (Portfolio.HasPosition(myins))
				{
					string entry = myins.Symbol + "|" + Portfolio.GetPosition(myins).Amount;
					mydebug.WriteLine(entry);

				}
			}
			mydebug.WriteLine("------Positions-------" + Clock.DateTime+"--------reconcile---------");
		}
		
		
        protected override void OnBar(Instrument instrument, Bar bar)
        {
			if (StrategyManager.Mode == StrategyMode.Live)
			{
				mydebug.WriteLine("------------------BAR CALLED "+bar.CloseDateTime+"----------------");
				GetPositions();
			}
			
			isConnected();
            
            int marketCheck = CheckOps.checkMarketTimeandPrice(Clock,mydebug);
            int callCheck = UpdateDateTime(bar);
            int timeCheck = CheckOps.checkTime(bar,Clock);
            if (marketCheck == 1 && callCheck == 1 && timeCheck == 1 && connected)
            {

                TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);
                // data requirements
                mydebug.WriteLine("NYTime: " + USTime.ToString() + "-------------" + "LocalTime: " + Clock.DateTime.ToString());
                updateDatabase();
                int marketStatus = CheckOps.checkMarketStatus(dataBase,Instruments, mydebug);
                if (marketStatus == 1)
                {
                    mydebug.WriteLine("------------------REBALANCE TIME  " + bar.CloseDateTime + "----------------");
                    Dictionary<Instrument, List<Bar>> alignedDict = DataOps.processResampled(dataBase,InstrumentManager,Instruments);
    
                    int alignedDictCheckerInt = CheckOps.alignedDictChecker(alignedDict, mydebug,lookBacks);
                    if (alignedDictCheckerInt==1)
                    {
                        //update Substrategy data
                        SubStrategyOps.updateData(subStratList, alignedDict,mydebug);
                        QuantOps.updateTargetWeights(subStratList,mydebug);
                        //consolidatePositions(subStratList, Instruments);
                        ExecutionOps.executionLogic(subStratList, Instruments,this,Portfolio,mydebug,TOTAL_EXPOSURE);

                    }

                    mydebug.WriteLine("State Updated and Rebalance Done");
                }
                else
                {
                    mydebug.WriteLine("Failed to Update State: Market is Closed or Database is not aligned");
                }
                
            }
            Log(Portfolio.Value, "Equity");
            Portfolio.Performance.Update();
        }
        protected override void OnFill(Fill fill)
        {
            Log(fill, "Fills");
            mydebug.WriteLine(fill.ToString());


        }

        protected override void OnStrategyStop()
        {

            mydebug.Dispose();
        }


        //UPDATE OPERATIONS
        public void updateDatabase()
        {
            foreach (Instrument ins in Instruments)
            {

                BarSeries mydata = DataOps.getBarsForMe(ins, 21, StrategyManager, DataManager, Clock, illogicThold,mydebug);
                BarSeries records = dataBase[ins];


                foreach (Bar mbar in mydata)
                {
                    DateTime bartime = mbar.DateTime;
                    if (records.Contains(bartime) == false)
                    {
                        records.Add(mbar);
                    }


                }
                dataBase[ins] = records;

            }
        }



        public int UpdateDateTime(Bar bar)
        {
            if (bar.DateTime != CurrentTime)
            {
                CurrentTime = bar.DateTime;
                return 1;
            }
            else { return 0; }
        }

        private void isConnected()
        {
            if (StrategyManager.Mode == StrategyMode.Live)
                if (DataProvider.IsConnected == true)
                    connected = true;
                else
                    connected = false;
        }
       


    }


}























