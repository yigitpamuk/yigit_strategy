using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using Genesis;



namespace CheckLib
{
	public static class CheckOps
	{

		public static int checkMarketTimeandPrice(Clock Clock,Debugger mydebug)

		{
			// this function will be called by OnBid, OnAsk and OnBar 

			//10-Year U.S. Treasury Note Futures	PreOpenSunday 16:00	Sunday 17:00-16:00	PreOpenWeekDay 16:45	Weekday 17:00-16:00
			int checker = 0;



			TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
			DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);

			// if the time passes this Date we should not enter any positions and consider starting out new algo

			if ((USTime.DayOfWeek == DayOfWeek.Saturday || USTime.DayOfWeek == DayOfWeek.Sunday))
			{
				mydebug.WriteLine("Market Is Closed Weekend Condition");
				checker++;
			}

			if (USTime.Hour <= 9)
			{
				mydebug.WriteLine("Market Is Closed Weekday Close Condition");
				checker++;
			}
			if (USTime.Hour > 16)
			{
				mydebug.WriteLine("Market Is Closed Weekday Close Condition");
				checker++;
			}



			if (checker > 0)
			{
				return 0;
			}

			else
			{
				return 1;
			}
		}

        public static int alignedDictChecker(Dictionary<Instrument, List<Bar>> inputDict, Debugger mydebug, List<int> lookBacks)
        {
            int count = inputDict[inputDict.Keys.ToList()[0]].Count();
            mydebug.WriteLine("count:" + count);
            mydebug.WriteLine("MAXLOOKBACK:" + lookBacks.Max());
            foreach (Instrument ins in inputDict.Keys)
            {
                if (inputDict[ins].Count() != count)
                {
                    Console.WriteLine("count:" + inputDict[ins].Count() + " " + ins);

                    mydebug.WriteLine("List is not correctly aligned");
                    return 0;
                }

                else if (inputDict[ins].Count() < lookBacks.Max())
                {
                    mydebug.WriteLine("Not Enough Data");
                    return 0;
                }

            }
            return 1;
        }

        public static int checkMarketStatus(Dictionary<Instrument, BarSeries> dataBase,InstrumentList Instruments, Debugger mydebug)
		{
			int response = 0;
			foreach (Instrument ins in dataBase.Keys)
			{


				BarSeries data = dataBase[ins];
				DateTime lastTimeStamp = data[data.Count - 1].OpenDateTime;
				TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
				DateTime USTime = TimeZoneInfo.ConvertTime(lastTimeStamp.ToUniversalTime(), convertZone);

				if (USTime.Hour == 14 && USTime.Minute == 30)
				{
					response++;
				}
				else
				{
					mydebug.WriteLine("No recent data for : " + ins);
				}

			}
			if (response == Instruments.Count())
			{
				return 1;
			}

			else
			{
				return 0;/// CHANGE TO 0 TESTING PURPOSES
			}

		}
		public static int checkTime(Bar bar,Clock Clock)
		{
			TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
			DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);
			if (USTime.Hour == 15 && USTime.Minute <= 15)
			{
				return 1;
			}
			else
			{
				return 0;//TESTING CHANGE THIS!!!!!
			}
		}
	}
		
        public class Debugger

        {
            public string logLocation { get; set; }
            public bool deBug { get; set; }
            public Logger logger { get; set; }
            public Debugger(string location, bool debug)
            {

                logLocation = location;
                Logger nlogger = new Logger(location, debug);
                logger = nlogger;
                deBug = debug;
                logger.Print("---------------Logger Initiated-------------");
            }
            public void WriteLine(string str)
            {
                if (deBug == true)
                {
                    logger.Print(str);
                }
            }
            public void Dispose()
            {
                if (deBug == true)
                {
                    logger.Print("---------------Logger Disposed-------------");
                    logger.Dispose();
                }
            }

        }

    
}


