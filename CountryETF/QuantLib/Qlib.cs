using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using SubStratLib;
using CheckLib;
namespace  QuantLib
{

    public static class QuantOps
    {
        public static void updateTargetWeights(List<SubStrategy> mysubStratList,Debugger mydebug)
        {
            foreach (SubStrategy mySub in mysubStratList)
            {
                int lookBack = mySub.lookBack;
                List<Bar> datap1 = mySub.Ins1Data;
                List<Bar> datap2 = mySub.Ins2Data;

				List<Bar> trimdatap1 = datap1.Skip(Math.Max(0, datap1.Count() - lookBack)).ToList();
                List<Bar> trimdatap2 = datap2.Skip(Math.Max(0, datap2.Count() - lookBack)).ToList();

                double p1price = (double)trimdatap1[trimdatap1.Count() - 1].Close;
                double p2price = (double)trimdatap2[trimdatap2.Count() - 1].Close;

                List<Bar> last5p1 = datap1.Skip(Math.Max(0, datap1.Count() - lookBack)).ToList();
                List<Bar> last5p2 = datap2.Skip(Math.Max(0, datap2.Count() - lookBack)).ToList();

                double spread_current = p1price / p2price;

                List<double> spreadList = new List<double>();
                for (int i = 0; i < lookBack; i++)
                {
                    double spr = (double)trimdatap1[i].Close / (double)trimdatap2[i].Close;
                    spreadList.Add(spr);
                }

                List<double> last5p1close = new List<double>();
                List<double> last5p2close = new List<double>();
                for (int i = 0; i < lookBack; i++)
                {
                    double valp1 = (double)last5p1[i].Close;
                    double valp2 = (double)last5p2[i].Close;
                    last5p1close.Add(valp1);
                    last5p2close.Add(valp2);
                }



                double mean_last = spreadList.Skip(Math.Max(0, spreadList.Count() - 2)).ToList().Average();
                double mean = spreadList.Average();
                double std = CalculateStdDev(spreadList);
                double up_limit = mean + std;
                double down_limit = mean - std;

                double x_smooth = last5p1close.Average();
                double y_smooth = last5p2close.Average();
                double x_quantity = mySub.allocation*0.5 / x_smooth;
                double y_quantity = mySub.allocation*0.5 / y_smooth;

                double lastp1 = last5p1[last5p1.Count-1].Close;
                double lastp2 = last5p2[last5p2.Count - 1].Close;
				double mean_speed = mean - mean_last;
                int inpos = mySub.inpos;
                mySub.Ins1Last = lastp1;
                mySub.Ins2Last = lastp2;
                mySub.Exposure = (lastp1 * Math.Abs(mySub.targetpositionIns1) + lastp2 * Math.Abs(mySub.targetpositionIns2));


                if (inpos == 0)
                {
                    if (spread_current > up_limit )
                    {
                        mySub.targetpositionIns1 = x_quantity * -1;
                        mySub.targetpositionIns2 = y_quantity;
                        mySub.inpos = -1;
                        mySub.change = 1;
                        mydebug.WriteLine(mySub.strategyId+" "+"SHORT ENTRY");

                    }
                    else if (spread_current < down_limit )
                    {
                        mySub.targetpositionIns1 = x_quantity;
                        mySub.targetpositionIns2 = y_quantity * -1;
                        mySub.inpos = 1;
                        mySub.change = 1;
                        mydebug.WriteLine(mySub.strategyId + " " + "LONG ENTRY");
                    }
                    else
                    {
                        mySub.targetpositionIns1 = 0;
                        mySub.targetpositionIns2 = 0;
                        mySub.inpos = 0;
                
                    }
                }
                else if (inpos == 1)
                {
                    if (spread_current > mean)
                    {
                        mySub.targetpositionIns1 = 0;
                        mySub.targetpositionIns2 = 0;
                        mySub.inpos = 0;
                        mySub.change = 1;
                        mydebug.WriteLine(mySub.strategyId + " " + "LONG EXIT");
                    }

                }
                else if (inpos == -1)
                {
                    if (spread_current < mean )
                    {
                        mySub.targetpositionIns1 = 0;
                        mySub.targetpositionIns2 = 0;
                        mySub.inpos = 0;
                        mySub.change = 1;
                        mydebug.WriteLine(mySub.strategyId + " " + "SHORT EXIT");
                    }

                }

            }
        }

        public static double CalculateStdDev(List<double> values)
        {
            double ret = 0;
            if (values.Count() > 0)
            {
                //Compute the Average      
                double avg = values.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (values.Count()));
            }
            return ret;
        }


    }
   




}







