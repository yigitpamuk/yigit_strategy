using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using CheckLib;

namespace SubStratLib
{
    public class SubStrategy
    // This class is the backbone for multi strategy implementation, we will loop throgh list of SubStrategies
    {
        public Instrument Ins1 { get; set; }
        public Instrument Ins2 { get; set; }

        public List<Bar> Ins1Data { get; set; }
        public List<Bar> Ins2Data { get; set; }

        public double targetpositionIns1 { get; set; }
        public double targetpositionIns2 { get; set; }
        public int inpos { get; set; }
        public int change { get; set; }

        public int lookBack { get; set; }

        public string strategyId { get; set; }
        public double allocation { get; set; }

        public double Ins1Last { get; set; }
        public double Ins2Last { get; set; }
        public double Exposure { get; set; }
    }

    public static class SubStrategyOps
    {
        public static List<SubStrategy> initSubStrategies(List<List<string>> pairs , List<int> lookBacks, InstrumentManager InstrumentManager,double allocation,Debugger mydebug)
        {

    
            List<SubStrategy> resultList = new List<SubStrategy>();
            foreach (List<string> pair in pairs)
            {
                foreach (int lookBack in lookBacks)
                {
                    
                    SubStrategy mySubStrat = new SubStrategy();
                    mySubStrat.Ins1 = InstrumentManager.GetBySymbol(pair[0]);
                    mySubStrat.Ins2 = InstrumentManager.GetBySymbol(pair[1]);
                    mySubStrat.Ins1Data = new List<Bar>();
                    mySubStrat.Ins2Data = new List<Bar>();
                    mySubStrat.targetpositionIns1 = 0;
                    mySubStrat.targetpositionIns2 = 0;
                    mySubStrat.lookBack = lookBack;
                    mySubStrat.allocation = allocation;
                    mySubStrat.inpos = 0;
                    mySubStrat.Ins1Last = 0;
                    mySubStrat.Ins2Last = 0;
                    mySubStrat.Exposure = 0;
                    mySubStrat.change = 0;
                    mySubStrat.strategyId = mySubStrat.Ins1 + " | " + mySubStrat.Ins2 + " | " + mySubStrat.lookBack.ToString();
                    resultList.Add(mySubStrat);
                    mydebug.WriteLine(mySubStrat.strategyId + " Created");
                }
            }
            mydebug.WriteLine(" Instrument Parameter Combinations Created: " + resultList.Count);
            mydebug.WriteLine("--------------------------------");
            return resultList;





        }
        public static void updateData(List<SubStrategy> mysubStratList, Dictionary<Instrument, List<Bar>> alignedDict,Debugger mydebug)
        {
            foreach (SubStrategy mySub in mysubStratList)
            {
                Instrument p1 = mySub.Ins1;
                Instrument p2 = mySub.Ins2;
                mySub.Ins1Data = alignedDict[p1];
                mySub.Ins2Data = alignedDict[p2];

            }

        }
    }




}

