using System;

using SmartQuant;


namespace OpenQuant
{
    public class MyStrategy : InstrumentStrategy
    {
		public MyStrategy(Framework framework, string name)
            : base(framework, name)
        {
        }

        protected override void OnStrategyStart()
        {
        }

        protected override void OnBar(Instrument instrument, Bar bar)
        {
        }
    }
}

