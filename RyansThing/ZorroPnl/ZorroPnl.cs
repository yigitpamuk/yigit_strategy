using System;

using SmartQuant;

namespace OpenQuant
{
    class ZorroPnl : InstrumentStrategy
    {
        public ZorroPnl(Framework framework, string name)
            : base(framework, name)
        {
        }

        protected override void OnStrategyStart()
        {
        }

        protected override void OnBar(Instrument instrument, Bar bar)
        {
        }
    }
}
