using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SmartQuant;
using HtmlAgilityPack;

namespace Zorro
{

    public static class parseOps
    {

        public class PLTable
        // This class is the backbone for multi strategy implementation, we will loop throgh list of SubStrategies
        {
            public string Trade { get; set; }
            public string ID { get; set; }
            public string Lots { get; set; }

            public string Entry_Time { get; set; }
            public string Entry { get; set; }
            public string Price { get; set; }

            public string Stop { get; set; }
            public string Trail { get; set; }
            public string Target { get; set; }

            public string Risk { get; set; }
            public string Profit { get; set; }
            public string Pips { get; set; }
            public float openProfit { get; set; }
            public float intLots { get; set; }
            public string instrumentString { get; set; }

            public string longshort { get; set; }
        }

        public static void getPositions(string location)
        {

            List<PLTable> positions = parseMe(location);

            //logical check of positions


            //parse close pnl
            
            

        }

        public static List<PLTable> parseMe(string location)
        {
            List<string> checkList = new List<string>();
            checkList.Add("Trade");
            checkList.Add("ID");
            checkList.Add("Lots");
            checkList.Add("Entry Time");
            checkList.Add("Entry");
            checkList.Add("Price");
            checkList.Add("Stop");
            checkList.Add("Trail");
            checkList.Add("Target");
            checkList.Add("Risk");
            checkList.Add("Profit");
            checkList.Add("Pips");


            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc = web.Load(location);

            List<PLTable> PLTables = new List<PLTable>();
            foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table"))
            {
                
                ///This is the table.    
                foreach (HtmlNode row in table.SelectNodes("tr"))
                {

                    ///This is the row.
                    List<string> lines = new List<string>(); 
                    foreach (HtmlNode cell in row.SelectNodes("th|td"))
                    {
                        ///This the cell.
                        lines.Add(cell.InnerText);
                    }
                    if (lines.Count==checkList.Count)
                    {
                        if (lines.All(checkList.Contains))
                        {
                            
                            foreach (HtmlNode rowa in table.SelectNodes("tr"))
                            {

                                PLTable myRow = new PLTable();
                                List<string> addItem = new List<string>();
                                foreach (HtmlNode cella in rowa.SelectNodes("th|td"))
                                {
                                    ///This the cell.
                                    ///
                                    if (checkList.Contains(cella.InnerText))
                                    {
                                        //pass

                                    }
                                    else
                                    {
                                        addItem.Add(cella.InnerText);
                                 
                                    }
                                    
                                }
                           

                                if (addItem.Count==12)
                                {
                                    myRow.Trade = addItem[0];
                                    myRow.ID = addItem[1];
                                    myRow.Lots = addItem[2];

                                    myRow.Entry_Time = addItem[3];
                                    myRow.Entry = addItem[4];
                                    myRow.Price = addItem[5];

                                    myRow.Stop = addItem[6];
                                    myRow.Trail = addItem[7];
                                    myRow.Target = addItem[8];

                                    myRow.Risk = addItem[9];
                                    myRow.Profit = addItem[10];
                                    myRow.Pips = addItem[11];
                                    myRow.openProfit=(float.Parse(myRow.Profit.Replace("$", ""), System.Globalization.CultureInfo.InvariantCulture));
                                    myRow.intLots = (float.Parse(myRow.Lots, System.Globalization.CultureInfo.InvariantCulture));
                                    
                                    string[] splitted = myRow.Trade.Split(new[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                                    myRow.instrumentString=(splitted[0].Replace("[","").Replace("/","")+".CFD");
                                    myRow.longshort=(splitted[1][0]).ToString();
                                    PLTables.Add(myRow);
                                }

                                
                                
                            }
                            
                        }

                    }
            }
        }


            return PLTables;
        }


            


        }
    }


	


