using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;

using SubStratLib;
using CheckLib;
using QuantLib;
using ExecutionLib;
using Genesis;
namespace OpenQuant
{
    public class MyStrategy : Strategy
    {
                    // STRATEGY PARAMETER SETS      
        public int NUMBER_OF_STRATEGIES =20;
        public List<int> BUFFERS = new List<int>(){150,200,250};    
        public List<int> PF_RANGE = new List<int>(){5,50};
        public List<int> EXITLIMIT_RANGE = new List<int>(){10,30};
        public List<int> WIDTH_RANGE = new List<int>(){5,10};
        public int START = 5;
        
        // STRATEGY ASSUMPTIONS

        // MAXIMUM POSITION LIMIT = NUMBER_OF_STRATEGIES*CLIPSIZE
        [Parameter]
        public int clipSize = 10000;
        [Parameter]
        public double comissionPerLot = 0.00002;
        [Parameter]
        public double slippagePerLot = 0.00002;
        [Parameter]
        public double  illogicThold= 0.8;// for checking if downloaded data is logical or not
        // INIT UTILITY 
        public List<SubStrategy> subStratList;
        public double latestAsk;
        public double latestBid;

        public DateTime latestAskTime;
        public DateTime latestBidTime;
        public DateTime CurrentTime;
        public DateTime? latestBarTime;

        public int onBarcall;
		public int latestPos;
        bool connected;
		public int validator;
		public BarSeries lastBars;

        public MyStrategy(Framework framework, string name)
            : base(framework, name)
        {


        }




        protected override void OnStrategyStart()
        {
			double AllocationPerInstrument = NUMBER_OF_STRATEGIES* (double)clipSize * 0.1;
			Portfolio.Account.Deposit(AllocationPerInstrument, CurrencyId.USD, "Initial allocation");

            // initiate SubStrategies
            subStratList=SubStrategyOps.initSubStrategies(NUMBER_OF_STRATEGIES,Clock,START,comissionPerLot,slippagePerLot,clipSize);
            CurrentTime=Clock.DateTime;
            latestBarTime=null;
            connected = true;
			validator=0;
			lastBars=new BarSeries();
			onBarcall=0;
			AddReminder(Clock.DateTime.AddSeconds(60));

            List<Instrument> instrumentCollection = new List<Instrument>();
            foreach (Instrument instruments in Instruments)
            {
                instrumentCollection.Add(instruments);
            }

            myConsole.WriteLine("Data Download Starts");
            BarSeries historicalBars = new BarSeries();

            Instrument instrument = instrumentCollection[0];
            
            historicalBars = StatusUpdater.getBarsForMe(instrument, 30000, StrategyManager, DataManager, Clock);

            lastBars = historicalBars;
            myConsole.WriteLine("Data Download Ends");
            // generate utilities
        }

        protected override void OnBid(Instrument instrument, Bid bid)
        {
            latestBid=instrument.Bid.Price;
            latestBidTime=instrument.Bid.DateTime;
			

        }

		protected override void OnReminder(DateTime signalTime, object data)
		{
			myConsole.WriteLine("<-------------Heartbeat "+ Clock.DateTime + " ---------------------->");
			//AddReminder(Clock.DateTime.AddSeconds(60));
		}

        private void isConnected()
        {
            if (StrategyManager.Mode == StrategyMode.Live)
                if (DataProvider.IsConnected == true)
                    connected = true;
                else
                    connected = false;
        }
        protected override void OnAsk(Instrument instrument, Ask ask)
        {
            latestAsk=instrument.Ask.Price;
            latestAskTime=instrument.Ask.DateTime;
			if (validator==1)
			{
				validator=0;
				instantLogic(instrument,lastBars);
			}
		}

        //get data last 1000 bars
        // calculate signal for each strategy and update the substrat
        // execution module



        protected override void OnBar (Instrument   instrument  , Bar bar)
        {
	
            isConnected();
			myConsole.WriteLine("Bar Process Started " + bar.DateTime);
            int callCheck = UpdateDateTime(bar);
            int marketCheck = MarketCheck.checkMarketTimeandPrice( instrument,  latestBid,  latestAsk, CurrentTime, Clock);
            // callcheck and marketcheck
       

            if (callCheck==1 && marketCheck==1 && connected ) //&& 
            {
            // get dta here to check latestbar as a second resort



                //DateTime BarTime = (lastBars[lastBars.Count - 1].OpenDateTime);

			    //int checkStamp=checkBarTime(BarTime);


				validator=1;
				//instantLogic(instrument,lastBars);
					
			    
			}
		
		
			else
			{
				myConsole.WriteLine("-------------------BARTIME : "+bar.DateTime+"-----------------------------");
				myConsole.WriteLine("-------------------State Update Failed: Reason : SameBar or No Connection-");
			}
		            //statusupdater updates all 
		            
		         
	        Log(Portfolio.Value, "Equity");
	        Portfolio.Performance.Update();
		
		}



		public void instantLogic(Instrument instrument,BarSeries historicalBars)
		{

			foreach (SubStrategy SubStrategy in subStratList)
			{
				SubStrategyOps.updateSubStratInit(SubStrategy,BUFFERS,PF_RANGE,EXITLIMIT_RANGE,WIDTH_RANGE, Clock);
		
			}

		
			StatusUpdater.updateBarState(instrument,StrategyManager,DataManager,subStratList,illogicThold,latestBid,latestAsk,Clock,historicalBars);
		                    

		                                        
			executionEngine.run(instrument , subStratList, latestBid, latestAsk,Clock,this,StrategyManager,InstrumentManager);
			// when order is given change inpos, if position is closed change increment since entry to 0.
		}
	
		



        protected override void OnFill (Fill fill)
        {
            Log(fill, "Fills");
            myConsole.WriteLine(fill.ToString());
            
        
        }

        //UTILITY FUNCTIONS
        public int UpdateDateTime(Bar bar)
        {
            if (bar.DateTime != CurrentTime){
                CurrentTime = bar.DateTime;
                return 1;
            }else{ return 0; }
        }

        public int checkBarTime(DateTime BarTime)
        {
            if (latestBarTime==null)
            {
                latestBarTime  = BarTime;
                return 1;
            } 
            else
            {
                if (latestBarTime== BarTime)
                {
                    return 0;
                }
                else
                {
                    latestBarTime  = BarTime;
                    return 1;
                }
            }
        }


    }
}




















