using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Genesis;
using SmartQuant;
using CheckLib;


namespace OpenQuant
{
	public partial class Realtime : Scenario
	{	
		List<string> listCurrencies = new List<string> {"EURUSD"};

		
		
		private long barSize = 60*60;
		public Realtime(Framework framework)
			: base(framework)
		{
		}
		
		public override void Run()
		{
            string datesting = DateTime.Now.Year.ToString()+"_"+ DateTime.Now.Month.ToString()+"_"+ DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString()+"_"+DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();
			string filename = datesting+"_REAL_LOGS.txt";
            //string location = "C:\\Users\\yigit.pamuk\\oq_logs\\flipflop\\" + filename;
			string location = "C:\\home\\source\\csharp\\strat\\FlipFlop_2\\logs\\" + filename;

			Debugger mydeBug = new Debugger(location, true);

            StrategyManager.Mode = StrategyMode.Live;
            
			string name = "FlipFlop_1_Live";

            strategy = new MyStrategy(framework, name , mydeBug);
			BarFactory.Clear();
			
			strategy.DataProvider = ProviderManager.GetDataProvider("IB");

			foreach (string curr in listCurrencies)
			{
				Instrument instrument = InstrumentManager.GetBySymbol(curr);
				strategy.AddInstrument(instrument, strategy.DataProvider);
				BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle,0);		
			}
 

            
            strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");
            framework.EventManager.Filter = new MyEventFilter(framework);
            StartStrategy();
		}

	}
}






















































