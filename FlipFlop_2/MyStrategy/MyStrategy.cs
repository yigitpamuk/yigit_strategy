using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;

using SubStratLib;
using CheckLib;
using QuantLib;
using ExecutionLib;
using Genesis;

namespace OpenQuant
{
    public class MyStrategy : Strategy
    {
                    // STRATEGY PARAMETER SETS      
        public int NUMBER_OF_STRATEGIES =20;
        public List<int> BUFFERS = new List<int>(){150,200,250};    
        public List<int> PF_RANGE = new List<int>(){5,50};
        public List<int> EXITLIMIT_RANGE = new List<int>(){10,30};
        public List<int> WIDTH_RANGE = new List<int>(){5,10};
        public int START = 5;
        //public List<int> inpos_sequence = new List<int>() { 0,0,0,0,1,1,1,1,0,1,0,1,0};
        // STRATEGY ASSUMPTIONS

        // MAXIMUM POSITION LIMIT = NUMBER_OF_STRATEGIES*CLIPSIZE
        [Parameter]
        public int clipSize = 500;
        [Parameter]
        public double comissionPerLot = 0.00002;
        [Parameter]
        public double slippagePerLot = 0.00002;
        [Parameter]
        public double  illogicThold= 0.8;// for checking if downloaded data is logical or not
        [Parameter]
        public int shortDATA = 500;// for checking if downloaded data is logical or not
        [Parameter]
        public int longDATA = 30000;// for checking if downloaded data is logical or not
        // INIT UTILITY 
        public List<SubStrategy> subStratList;
        public double latestAsk;
        public double latestBid;

        public DateTime latestAskTime;
        public DateTime latestBidTime;
        public DateTime CurrentTime;
        public DateTime? latestBarTime;

        public int onBarcall;
		public int latestPos;
        bool connected;
		public int validator;
		public BarSeries lastBars;
        int bcount;

        Debugger mydebug;
        public List<bool> original;
        public MyStrategy(Framework framework, string name, Debugger mydeBug)
            : base(framework, name)
        {

            mydebug = mydeBug;
        }




        protected override void OnStrategyStart()
        {

           

            double AllocationPerInstrument = NUMBER_OF_STRATEGIES* (double)clipSize * 0.1;
			Portfolio.Account.Deposit(AllocationPerInstrument, CurrencyId.USD, "Initial allocation");

            // initiate SubStrategies
            subStratList=SubStrategyOps.initSubStrategies(NUMBER_OF_STRATEGIES,Clock,START,comissionPerLot,slippagePerLot,clipSize);
            CurrentTime=Clock.DateTime;
            latestBarTime=null;
            connected = true;
			validator=0;
			lastBars=new BarSeries();
            original = new List<bool>();
            onBarcall =0;
			AddReminder(Clock.DateTime.AddSeconds(5));
            bcount = 0;
            List<Instrument> instrumentCollection = new List<Instrument>();
            foreach (Instrument instruments in Instruments)
            {
                instrumentCollection.Add(instruments);
            }
			Console.WriteLine("CLIPSIZE: "+clipSize);
			Console.WriteLine("MAX EXPOSURE: "+clipSize*subStratList.Count);
            mydebug.WriteLine("Data Download Starts");
            BarSeries historicalBars = new BarSeries();

            Instrument instrument = instrumentCollection[0];
            
            historicalBars = StatusUpdater.getBarsForMe(instrument, longDATA, StrategyManager, DataManager, Clock,illogicThold,mydebug);

            lastBars = historicalBars;



            foreach (Bar mbar in historicalBars)
            {
                //mydebug.WriteLine(mbar.OpenDateTime + " "+mbar.CloseDateTime +" "+mbar.Close);
                original.Add(true);
            }
			

            mydebug.WriteLine("Data Download Ends");
            // generate utilities
        }

        protected override void OnBid(Instrument instrument, Bid bid)
        {
            latestBid=instrument.Bid.Price;
            latestBidTime=instrument.Bid.DateTime;
			

        }

		protected override void OnReminder(DateTime signalTime, object data)
		{
            mydebug.WriteLine("<-------------Heartbeat "+ Clock.DateTime + " ---------------------->");
			AddReminder(Clock.DateTime.AddSeconds(8));
			
		}

        private void isConnected()
        {
            if (StrategyManager.Mode == StrategyMode.Live)
                if (DataProvider.IsConnected == true)
                    connected = true;
                else
                    connected = false;
        }
        protected override void OnAsk(Instrument instrument, Ask ask)
        {
            latestAsk=instrument.Ask.Price;
            latestAskTime=instrument.Ask.DateTime;
			if (validator==1 &&  StrategyManager.Mode == StrategyMode.Live)
			{
				validator=0;
				instantLogic(instrument,lastBars);
			}
		}


        protected override void OnBar (Instrument   instrument  , Bar bar)
        {
			mydebug.WriteLine("Recent Bar: " + bar.OpenDateTime +" " +bar.CloseDateTime +" "+bar.Close );
            isConnected();
            mydebug.WriteLine("Bar Process Started " + bar.DateTime);
            int callCheck = UpdateDateTime(bar);
            int marketCheck = MarketCheck.checkMarketTimeandPrice( instrument,  latestBid,  latestAsk, CurrentTime, Clock, mydebug);
            // callcheck and marketcheck
            

            if (callCheck==1 && marketCheck==1 && connected )  
            {

                updateData(instrument, bar);

              
                DateTime BarTime = (lastBars[lastBars.Count - 1].OpenDateTime);

			    int checkStamp=checkBarTime(BarTime);

			    if (checkStamp==1 && StrategyManager.Mode == StrategyMode.Live )
			    {
				    validator=1;
	
					
			    }
				else if  (checkStamp==1 && StrategyManager.Mode == StrategyMode.Backtest ) 
				{
					instantLogic(instrument,lastBars);
				}
			}
		
		
			else
			{
                mydebug.WriteLine("-------------------BARTIME : "+bar.DateTime+"-----------------------------");
                mydebug.WriteLine("-------------------State Update Failed: Reason : SameBar or No Connection-");
			}
		            //statusupdater updates all 
		            
		         
	        Log(Portfolio.Value, "Equity");
	        Portfolio.Performance.Update();
		
		}

        public void updateData(Instrument instrument,Bar bar)
        {
            
            int checkInposint = SubStrategyOps.checkInpos(subStratList);

            //bcount++;
            //int checkInposint = inpos_sequence[bcount];

            //mydebug.WriteLine("INPOS:"+checkInposint);
            if (checkInposint == 1)
            {
                mydebug.WriteLine(" Inpos, just add the bar");
                lastBars.Add(bar);
                original.Add(false);

               
            }
            else if (checkInposint == 0)
            {
                // if there is a bar that is not original, delete the series, add original ones to the series and download.
                int originalCount = 0;
               // mydebug.WriteLine("original before " + original.Count);
                for (int i = 0; i < original.Count; i++)
                {
                    if (original[i] == false)
                    {
                        //mydebug.WriteLine("False Values Detected");
                        originalCount++;
                        //original.RemoveAt(i);
                    }

                }


                if (originalCount > 0)
                {
                    mydebug.WriteLine("After inpos data recovery");
                    BarSeries newBars = new BarSeries();
                    List<bool> originaln = new List<bool>();
                    int addCount = 0;
                    foreach (Bar mbar in lastBars)
                    {
                        addCount++;
                        if (addCount < originalCount)
                        {
                            newBars.Add(mbar);
                            originaln.Add(true);
                        }


                    }
                    int requiredBars = originalCount;
                    BarSeries historicalBarsAdd = StatusUpdater.getBarsForMe(instrument, shortDATA*2, StrategyManager, DataManager, Clock, illogicThold, mydebug);

                    foreach (Bar mbar in historicalBarsAdd)
                    {
                        DateTime bartime = mbar.DateTime;
                        if (newBars.Contains(bartime) == false)
                        {
                            newBars.Add(mbar);
                            originaln.Add(true);
                        }


                    }
                    
                    lastBars = newBars;
                    original = originaln;


                }
                else
                {
                    mydebug.WriteLine(" Not Inpos, Short data download");
                    BarSeries historicalBarsAdd = StatusUpdater.getBarsForMe(instrument, shortDATA, StrategyManager, DataManager, Clock, illogicThold, mydebug);

                    foreach (Bar mbar in historicalBarsAdd)
                    {
                        DateTime bartime = mbar.DateTime;
                        if (lastBars.Contains(bartime) == false)
                        {
                            lastBars.Add(mbar);
                            original.Add(true);
                        }


                    }


                }

            }

        }

		public void instantLogic(Instrument instrument,BarSeries historicalBars)
		{

			foreach (SubStrategy SubStrategy in subStratList)
			{
				SubStrategyOps.updateSubStratInit(SubStrategy,BUFFERS,PF_RANGE,EXITLIMIT_RANGE,WIDTH_RANGE, Clock);
		
			}

		
			StatusUpdater.updateBarState(instrument,StrategyManager,DataManager,subStratList,illogicThold,latestBid,latestAsk,Clock,historicalBars,mydebug,longDATA);
		                    

		                                        
			executionEngine.run(instrument , subStratList, latestBid, latestAsk,Clock,this,StrategyManager,InstrumentManager,mydebug);
			// when order is given change inpos, if position is closed change increment since entry to 0.
		}
	
		



        protected override void OnFill (Fill fill)
        {
            Log(fill, "Fills");
            mydebug.WriteLine(fill.ToString());
            
        
        }

        //UTILITY FUNCTIONS
        public int UpdateDateTime(Bar bar)
        {
            if (bar.DateTime != CurrentTime){
                CurrentTime = bar.DateTime;
                return 1;
            }else{ return 0; }
        }

        public int checkBarTime(DateTime BarTime)
        {
            if (latestBarTime==null)
            {
                latestBarTime  = BarTime;
                return 1;
            } 
            else
            {
                if (latestBarTime== BarTime)
                {
                    return 0;
                }
                else
                {
                    latestBarTime  = BarTime;
                    return 1;
                }
            }
        }
        protected override void OnStrategyStop()
        {
			
            mydebug.Dispose();
        }


        }
}








































