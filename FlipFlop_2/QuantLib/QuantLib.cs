using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckLib;
using SmartQuant;
using MathNet.Numerics;
using SubStratLib;
namespace QuantLib
{
    public  class Status

    {


        public double? f_agile {get;set;}
        public double? f_stable {get;set;}
        public double? x_prev {get;set;}
        public double? x_bar {get;set;}
        public double? ucl {get;set;}
        public double? lcl {get;set;}
        public double? forecast {get;set;}

        public List<double?> mw {get;set;}
        public int buf {get;set;}
        public int bufi {get;set;}
        
        public Status(int buf)
        {
        	this.f_agile= null;
        	this.f_stable=null;
        	this.x_prev=null;
        	this.x_bar=null;
        	this.ucl=null;
        	this.lcl=null;
        	this.forecast=0;
        	this.mw=new List<double?>(){0};
        	this.buf=buf;
        	this.bufi=0;
        }
        public double? mean()
        {



        	return this.mw.Average();
        }
        public void append(double? x)
        {
        	if (this.mw.Count()> this.buf)
        	{
        		this.mw[this.bufi]=x;
        		this.bufi = (this.bufi+1) % this.buf;

        	}
        	else
        	{
        		this.mw.Add(x);
        	}
        }
    }

    public class FlipFlop
    {
	    public double l {get;set;}
	    public double u {get;set;}
	    public int buf {get;set;}

	    public FlipFlop(int buf)
	    {
	    	this.l=0.1;
        	this.u=0.9;
        	this.buf=buf;
	    }


	    public double? cses(double? f_t,double? data_t,double? alpha)
		{
			if (f_t==null)
			{
				return data_t;
			}	
			else
			{
				double? res = alpha*data_t+((1-alpha)*f_t);
				return res;
			}
		}

		public List<object> continous (double? x, Status status = null)
		{
			if (status==null)
			{
				status = new Status(this.buf);
			}
			status.f_agile = this.cses(status.f_agile,x,this.u);
			status.f_stable = this.cses(status.f_stable,x,this.l);

			status.x_bar = this.cses(status.x_bar,x,0.5);

			double? mw_average = status.mean();


			double? ucl = status.x_bar +3 * (mw_average/1.128);
			double? lcl = status.x_bar -3 * (mw_average/1.128);

			double? forecast =0;
			status.ucl = ucl;
			status.lcl = lcl;

			if (status.forecast >= lcl && status.forecast <= ucl)
			{
				forecast = status.forecast;
			}
			else
			{
				if (status.f_agile>=lcl && status.f_agile<=ucl)
				{
					forecast = status.f_agile;		
				} 
				else
				{
					forecast = status.f_stable;	
				}
			}

			if (status.x_prev != null)
			{
				double? delta =  Math.Abs((double)x-(double)status.x_prev);
				status.append(delta);
			}

			status.x_prev = x;
			status.forecast = forecast;

			List<object> resList = new List<object>();
			resList.Add(forecast);
			resList.Add(status);  
			return resList;

		}


	}

	public static class filterOps
	{
		public static List<double> generateSignal(List<double> data , int buf)
		{

			List<double?> dataLog = new List<double?>();
			foreach (double? myval in data)
			{
				double? logVal =  Math.Log((double)myval) ;
				dataLog.Add( logVal);

			}  

			Status status = null;
 			
 			List<double> response = new List<double>();  

			FlipFlop ff = new FlipFlop(buf);
			double forecast = 0 ;
			double last_forecast = 0;
			int counter = 1;
            double diff=0;
			foreach (double? val in dataLog)
			{
				List<object> calc = ff.continous(val,status);
				forecast = (double) calc[0];
				status = (Status) calc[1];
				if (forecast==last_forecast)
				{
					last_forecast=forecast;
					counter++;
				}
				else
				{
					if (forecast>last_forecast)
                    {
                        diff=1;
                    }
                    else if (forecast<last_forecast)
                    {
                        diff=-1;
                    } 
                    last_forecast=forecast;
					counter=1;		
				    
                }


			}

			response.Add(Math.Exp(forecast));
			response.Add(counter);
			response.Add(data[data.Count-1]);
            response.Add(diff);
			return response;
		}
	}
	
	public static class StatusUpdater
	{

		public static BarSeries getBarsForMe(Instrument instrument, int lookback_hours, StrategyManager StrategyManager, DataManager DataManager, Clock Clock, double thold,Debugger mydebug)
		{
			DateTime historicalEnd = Clock.DateTime;
			DateTime start = Clock.DateTime.AddHours((lookback_hours * 2) * -1).Date;
			BarSeries response = new BarSeries();
			if (StrategyManager.Mode == StrategyMode.Live)

			{

				response =  DataManager.GetHistoricalBars("IB", instrument, start, historicalEnd, BarType.Time, 60*60);
                
         
			}
			else
			{

				response = DataManager.GetHistoricalBars( instrument, start, historicalEnd, BarType.Time, 60*60);

			}
			
			return clearSeries(response,instrument,thold,Clock,mydebug);


		}
        
		public static BarSeries clearSeries(BarSeries inputSeries, Instrument instrument, double thold,Clock Clock,Debugger mydebug)
		{
			double sum = 0;
			double count = 0;

			foreach (Bar mybar in inputSeries)
			{
				count++;
				sum = sum + mybar.Close;
			}

			double averageClose = sum / count;

			BarSeries responseBars = new BarSeries();

			foreach (Bar mybar in inputSeries)
			{
				int checkBarint = checkVal2(mybar,Clock,thold,instrument,averageClose,mydebug);
				if (checkBarint == 1)
				{
					responseBars.Add(mybar);
				}
			}
            mydebug.WriteLine("Got Bars: "+responseBars.Count);
			return responseBars;
		}


		public static int checkVal2(Bar mybar, Clock Clock, double thold, Instrument instrument, double averageClose, Debugger mydebug)
		{
			//check the zeros and illogicla values in the dict

			//DateTime mydate = mybar.CloseDateTime ;
			double myval = mybar.Close;
			
			DateTime mydate = mybar.OpenDateTime;

			mydate = mydate.AddMinutes(60-mydate.Minute);
            //mydate = mydate.AddMinutes(1);
			if (mydate > Clock.DateTime)
			{
				mydebug.WriteLine(String.Format("Forward Date Detected! Instrument: {0} DateTime: {1} Value {2} ", instrument, mydate, myval));
				return 0;
			}

			else if (myval <= 0)
			{
                mydebug.WriteLine(String.Format("Neg or Zero Value Detected! Instrument: {0} DateTime: {1} Value {2} ", instrument, mydate, myval));
				return 0;
			}
			else
			{
				double last_mid = averageClose;
				double difference = Math.Abs(myval - last_mid) / myval;

				if (difference > thold)
				{
                    mydebug.WriteLine(String.Format("Illogical Value Detected! Instrument: {0} DateTime: {1} Value {2} Last Mid {3} ", instrument, mydate, myval, last_mid));
					return 0;
				}
				else
				{
					return 1;
				}
			}
		}
		
		
        public static int updateBarState(Instrument instrument,StrategyManager StrategyManager,DataManager DataManager ,
            List<SubStrategy> subStratList,double illogicThold, double latestBid,double latestAsk,Clock Clock,BarSeries historicalBars,Debugger mydebug,int checkLong)
        {   
            List<double> dataPoints = new List<double>(); 
            int badData=0;
            mydebug.WriteLine("------------------------------NEW BAR CALLED------------------------------------------------------");
            mydebug.WriteLine("------------------------------DATETIME: "+Clock.DateTime+"----------------------------------------");
    

            List<Dictionary<DateTime,Dictionary<Instrument,double>>> priceDictList = new  List<Dictionary<DateTime,Dictionary<Instrument,double>>>();

            // BarSeries historicalBars = getBarsForMe(instrument,1000,StrategyManager,DataManager,Clock); 


            mydebug.WriteLine("DataCount:"  + historicalBars.Count.ToString());
            if (historicalBars.Count == 0){
                badData++;
                mydebug.WriteLine("No bars to update state!, Check Connection!");
            }
            else
            {
                //check data points
                dataPoints = checkDatapoints ( historicalBars , illogicThold,  latestAsk,  latestBid, Clock,instrument,mydebug,checkLong);
            if (dataPoints.Count != checkLong)
                {
                badData++;
                mydebug.WriteLine("BarSize Len Not Right !, Check Data! BarCount: " +  dataPoints.Count().ToString());    
            }
            }

            

            if (badData==0)
            {

                mydebug.WriteLine("-------------Got Correct Data------------------------- ");
                
                //foreach strategy update vals
                foreach (SubStrategy SubStrategy in subStratList)
                {
                	int buffer = SubStrategy.lookBack;

                	List<double> stateVars = filterOps.generateSignal(dataPoints,buffer);

                	SubStrategy.lastSignal=stateVars[0];
                    SubStrategy.lastReal=stateVars[2];
                    SubStrategy.lastCnt=(int) stateVars[1];
					SubStrategy.lastUpdated= Clock.DateTime; 
                    SubStrategy.lastDif= (int) stateVars[3]; 

                }



                mydebug.WriteLine("State Updated");
                return 1;
            }
            else
            {
                mydebug.WriteLine("State Update Failed!");
                return 0;

            }
        }
        public static List<double> checkDatapoints (BarSeries myBars ,double illogicalThold, double lastAsk, double lastBid,Clock Clock,Instrument instrument,Debugger mydebug, int checkLong)
        {   
            //clean the ib data, get last n 
			double sum = 0;
			double count = 0;

			foreach (Bar mybar in myBars)
			{
				count++;
				sum = sum + mybar.Close;
			}

			double averageClose = sum / count;
			
			
            List<double> resList = new List<double>();
            foreach (Bar myBar in myBars)
            {
                double myval = myBar.Close;
                DateTime myTime = myBar.OpenDateTime;

                myTime = myTime.AddMinutes(60-myTime.Minute);
				//myTime = myTime.AddMinutes(1);

				//checkVal2(Bar mybar, Clock Clock, double thold, Instrument instrument, double averageClose)
                int check_int = checkVal2(myBar,Clock,illogicalThold,instrument,averageClose,mydebug);
                if (check_int==1){
                    resList.Add(myval);

                }
            }

            List<double> resTrimmed =  resList.Skip(Math.Max(0, resList.Count() - checkLong)).ToList();

            mydebug.WriteLine("Trimmed DataSize:" + resTrimmed.Count().ToString());
            return  resTrimmed;
        }

        

	







	}

}










