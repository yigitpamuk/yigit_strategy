using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;
using MathNet.Numerics;
using SubStratLib;
using  CheckLib;

namespace ExecutionLib
{
    public static class executionEngine 
    {
        public static void run(Instrument instrument , List<SubStrategy> subStratList, double latestBid, double latestAsk,Clock Clock,
        	Strategy Strategy,StrategyManager StrategyManager,InstrumentManager InstrumentManager, Debugger mydebug)
        {
        	//run order logic for reach strategy
        	int netAmount = 0;
        	
        	foreach (SubStrategy subStrategy in subStratList)
        	{
        		int orderSize = 	orderLogic(subStrategy,Clock,latestBid,latestAsk,StrategyManager,InstrumentManager,Strategy,instrument,mydebug);
        		netAmount=netAmount+orderSize;
        	}
        	batchExecutor(netAmount,instrument,InstrumentManager,StrategyManager,Strategy,mydebug);



        }

		public static void batchExecutor(int netAmount, Instrument instrument, InstrumentManager InstrumentManager, StrategyManager StrategyManager,Strategy Strategy, Debugger mydebug)
		{

            if (netAmount == 0)
            {
                mydebug.WriteLine("No net positions to execute, batch executor passed.");
            }
            else if (netAmount < 0)
            {
                mydebug.WriteLine("Batch Short Command: Amount: " + netAmount);
                Order order = Strategy.SellOrder(CFD(instrument, StrategyManager, InstrumentManager), netAmount*-1, "Batch Short");
                order.ClientID = "FLIP_FLOP_1";
                Strategy.Send(order);
            }


                //Strategy.Sell(CFD(instrument,StrategyManager,InstrumentManager),netAmount*-1,"Batch Short ");
            

		    else if (netAmount>0)
		    {
                mydebug.WriteLine("Batch Long Command: Amount: " + netAmount);
                    //Strategy.Buy(CFD(instrument,StrategyManager,InstrumentManager),netAmount,"Batch Long ");
                Order order = Strategy.BuyOrder(CFD(instrument, StrategyManager, InstrumentManager), netAmount, "Batch Long");
                order.ClientID = "FLIP_FLOP_1";
                Strategy.Send(order);
            }

		}


		public static int timeCheck(SubStrategy subStrategy,Clock Clock, Debugger mydebug)
		{
			DateTime lastUpdated= subStrategy.lastUpdated;
			DateTime currentTime = Clock.DateTime;
			TimeSpan span = currentTime.Subtract(lastUpdated);
			if (span.Minutes<60)
			{
				return 1;
			}
			else
			{
                mydebug.WriteLine("Cannot Place Order Since The Model is Not Updated");
				return 0;
			}
		}

        public static int orderLogic(SubStrategy subStrategy, Clock Clock,double latestBid,double latestAsk,
        	StrategyManager StrategyManager,InstrumentManager InstrumentManager,Strategy Strategy,Instrument instrument, Debugger mydebug)
		{
			// check if last updated is ;less than 60 minutes
			int response =0;
			int timeChecker=timeCheck(subStrategy,Clock,mydebug);
			if (timeChecker==1)
			{
				int inpos=subStrategy.inpos;
				int tip=subStrategy.incrementSinceEntry;

				int cnt = subStrategy.lastCnt;
				int start =subStrategy.start;
				int width =subStrategy.width;
				int exitLimit=subStrategy.exitLimit;
				double pf = (double) subStrategy.pf;
				int clipSize = subStrategy.clipSize;
				double com=subStrategy.comissionPerLot;
				double slippage = subStrategy.slippagePerLot;
				double signal = subStrategy.lastSignal;
				string id = subStrategy.strategyId;
				double real=(latestAsk+latestBid)/2;;
        		
				int lastDif = subStrategy.lastDif;
				double spread =  latestAsk-latestBid;

				double cost=spread*2+com*2;
				double dif = Math.Abs(signal-real);
				double thold = cost*pf;

                //print the strategy parameters and timestamp here
                mydebug.WriteLine("STRATEGY PRINTOUT FOR "+id+ " TIME: " + Clock.DateTime);
                mydebug.WriteLine("Static Variables: |start: "+start+" |width: "+ width+" |exitLimit: "+exitLimit+ " |pf: "+pf);
                mydebug.WriteLine("Dynamic Variables: |inpos: "+inpos+" |tip: "+ tip+" |cnt: "+cnt+ " |signal: "+signal+ "| realMid: " +real+ " |spread: " +spread*10000+ " |cost: " +cost*10000+ " |dif: " +dif+ " |thold: " + thold + " |lastDifInt: " + lastDif);

				
				if (inpos==0 && cnt>start && cnt<start+width && dif>thold && signal<real && spread<0.00003 && lastDif==-1)
				{
					//enter Long
					subStrategy.inpos=-1;
					response = -1*clipSize;
					subStrategy.entryTarget = latestBid;
                    //Strategy.Sell(CFD(instrument,StrategyManager,InstrumentManager),clipSize,"Short "+id);
                    //Strategy.Log("Short "+id+" Targeted Price: "+latestBid);
                    mydebug.WriteLine("*******> Short "+id+" Targeted Price: "+latestBid + "Target Size:" + clipSize);
				}
				else if (inpos==0 && cnt>start && cnt<start+width && dif>thold && signal>real  && spread<0.00003 && lastDif==1)
				{
					//enter Long
					subStrategy.inpos=1;
					response = clipSize;
					subStrategy.entryTarget = latestAsk;
                    //Strategy.Buy(CFD(instrument,StrategyManager,InstrumentManager),clipSize,"Long "+id);
                    //Strategy.Log("Long "+id+" Targeted Price: "+latestAsk);
                    mydebug.WriteLine("*******> Long "+id+" Targeted Price: "+latestAsk + "Target Size:" + clipSize);
				}	
				else if (inpos==1 && (real>=signal || tip>exitLimit) )
				{
					//Strategy.Sell(CFD(instrument,StrategyManager,InstrumentManager),clipSize,"Long Exit"+id);
					//Strategy.Log("Long Exit"+id+" Targeted Price: "+latestBid);
					double pnl = (latestBid-subStrategy.entryTarget)*(double)clipSize-com*(double)clipSize*2;
					
					response = -1*clipSize;
                    mydebug.WriteLine("*******> Long Exit"+id+" Targeted Price: "+latestBid + "Target Size:" + clipSize + " |Pnl : " + pnl );
					subStrategy.incrementSinceEntry=0;
					subStrategy.inpos=0;
				}
				else if (inpos==-1 && (real<=signal || tip>exitLimit) )
				{
					response = clipSize;
					double pnl = (subStrategy.entryTarget-latestAsk)*(double)clipSize-com*(double)clipSize*2;

                    //Strategy.Buy(CFD(instrument,StrategyManager,InstrumentManager),clipSize,"Short Exit"+id);
                    //Strategy.Log("Short Exit"+id+" Targeted Price: "+latestAsk);
                    mydebug.WriteLine("*******> Short Exit"+id+" Targeted Price: "+latestAsk + "Target Size:" + clipSize +  " |Pnl : " + pnl );
					subStrategy.incrementSinceEntry=0;
					subStrategy.inpos=0;
				}					//enter Long
				

			}	
			return response;		
		}

			
        public static Instrument CFD(Instrument instrument,StrategyManager StrategyManager,InstrumentManager InstrumentManager)
        {

            if (StrategyManager.Mode == StrategyMode.Backtest)
            {
                return instrument;
            }
            else if (StrategyManager.Mode == StrategyMode.Live)
            {
                return InstrumentManager.GetBySymbol(instrument.Symbol + ".CFD");
            }
            else
            {
                return InstrumentManager.GetBySymbol(instrument.Symbol + ".CFD");
            }
            //return instrument;
        }

    }
}

