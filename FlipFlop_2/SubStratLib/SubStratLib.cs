using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;

namespace  SubStratLib
{
    public  class SubStrategy
    // This class is the backbone for multi strategy implementation, we will loop throgh list of SubStrategies
    {
        
        public DateTime lastUpdated {get;set;}          
        public int inpos {get;set;} 

        public Random seed {get;set;}
        //below cannot be filled before we actually process the data
        public double lastSignal {get;set;}
        public double lastReal {get;set;}
        public int lastCnt {get;set;}
        public int side {get;set;}
        
        public int width {get;set;}
        public int pf {get;set;}
        public int lookBack {get;set;}
     
        public string strategyId {get;set;} 
        public int incrementSinceEntry {get;set;}
        public int exitLimit {get;set;}

        public int start {get;set;}

        public int lastDif {get;set;}
        
        public double comissionPerLot {get;set;}
        public double slippagePerLot {get;set;}

        public int clipSize {get;set;}

        public double entryTarget {get;set;}

    }

    public static  class SubStrategyOps
    {
        public static List<SubStrategy> initSubStrategies(int NUMBER_OF_STRATEGIES,Clock Clock,int START,double comissionPerLot,double slippagePerLot,int clipSize)
            {
 
                List<SubStrategy> resultList = new List<SubStrategy>(); 
                for (int i=0;i<NUMBER_OF_STRATEGIES; i++ )
                {
                    SubStrategy mySubStrat =  new SubStrategy();

                    mySubStrat.side=0; 
                    mySubStrat.seed=new Random(i*5);
                    mySubStrat.incrementSinceEntry= 0;
                    mySubStrat.inpos= 0; 
                    mySubStrat.lastUpdated= Clock.DateTime; 
                    mySubStrat.exitLimit=0;
                    mySubStrat.lastSignal=0;
                    mySubStrat.lastReal=0;
                    mySubStrat.lastCnt=0;
                    mySubStrat.width=0;
                    mySubStrat.pf=0;
                    mySubStrat.lookBack=0;
                    mySubStrat.comissionPerLot=comissionPerLot;
                    mySubStrat.slippagePerLot=slippagePerLot;
                    mySubStrat.start=START;
                    mySubStrat.clipSize=clipSize;
                    mySubStrat.lastDif=0;
                    mySubStrat.entryTarget=0;
                    string id = "EURUSD|" + i; 
                    mySubStrat.strategyId= id;
                    resultList.Add(mySubStrat);
                }
            

            Console.WriteLine(" Instrument Parameter Combinations Used: " + resultList.Count);
            Console.WriteLine("--------------------------------");
            return resultList;
                
            }
        


        public static void updateSubStratInit(SubStrategy SubStrategy, List<int> BUFFERS, List<int> PF_RANGE, List<int> EXITLIMIT_RANGE, List<int> WIDTH_RANGE,Clock Clock)
            {
                if (SubStrategy.inpos!=0)
                {
                    SubStrategy.incrementSinceEntry=SubStrategy.incrementSinceEntry+1;
                }

                else
                {
                    int start=PF_RANGE[0];
                    int end=PF_RANGE[1]+1;

                    int[] PF_RANGE_ = Enumerable.Range(start, end - start).ToArray();
                    
                    start=EXITLIMIT_RANGE[0];
                    end=EXITLIMIT_RANGE[1]+1;
                    int[] EXITLIMIT_RANGE_ = Enumerable.Range(start, end - start).ToArray();

                    start=WIDTH_RANGE[0];
                    end=WIDTH_RANGE[1]+1;
                    int[] WIDTH_RANGE_ = Enumerable.Range(start, end - start).ToArray();
                    

                    Random rnd = SubStrategy.seed;
                    
                    int rnd1=rnd.Next(0, BUFFERS.Count-1);
                    SubStrategy.lookBack=BUFFERS[rnd1];
                    //SubStrategy.lookBack=200;

                    int rnd2=rnd.Next(0, PF_RANGE_.Length-1);
                    SubStrategy.pf=PF_RANGE_[rnd2];
                    //SubStrategy.pf=10;
                    int rnd3=rnd.Next(0, EXITLIMIT_RANGE_.Length-1);
                    SubStrategy.exitLimit=EXITLIMIT_RANGE_[rnd3];
                    //SubStrategy.exitLimit=10;
                    int rnd4=rnd.Next(0, WIDTH_RANGE_.Length-1);
                    SubStrategy.width=WIDTH_RANGE_[rnd4];
                    //SubStrategy.width=10;
                    SubStrategy.lastUpdated= Clock.DateTime; 

                }


            }
        public static int checkInpos(List<SubStrategy> subStrategies)
            {
                int inposCount = 0;
                foreach (SubStrategy strategy in subStrategies)
                {
                    if (strategy.inpos!=0)
                    {
                        inposCount++;
                    }
                }
                if (inposCount>0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }

            }


    }




}
