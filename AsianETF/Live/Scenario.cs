using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckLib;
using SmartQuant;
using Microsoft.VisualBasic;
using System.Windows.Forms;


namespace OpenQuant
{
    public partial class Scenerio : Scenario
    {
        List<string> listCurrencies = new List<string> { "EWA", "EWC", "EZA", "EEM" };
        private long barSize = 60 * 30;

        public Scenerio(Framework framework)
            : base(framework)
        {
        }

        public override void Run()
        {

            StrategyManager.Mode = StrategyMode.Live;
			
			
			
			string datesting = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();
			string filename = datesting + "_LIVE_LOGS.txt";
			string location = "C:\\Users\\yigit.pamuk\\oq_logs\\countryetf\\" + filename;
			Debugger mydeBug = new Debugger(location, true);

            DialogResult dialogResult = MessageBox.Show("Use Strategy Persistence?", "Persistance", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                StrategyManager.Persistence = StrategyPersistence.Full;
            }
            else if (dialogResult == DialogResult.No)
            {
                StrategyManager.Persistence = StrategyPersistence.None;
            }

            string name = Interaction.InputBox("Unique string for Strategy Persistance\n If there is no file with this code \n Convention: Strategy Name + Tracking Number \n (Ex: CountryETF1)", "Strategy Persistance", "CountryETF1", -1, -1);



            strategy = new MyStrategy(framework, name,mydeBug);
            BarFactory.Clear();

            strategy.DataProvider = ProviderManager.GetDataProvider("IB");

            foreach (string curr in listCurrencies)
            {
                Instrument instrument = InstrumentManager.GetBySymbol(curr);
                strategy.AddInstrument(instrument, strategy.DataProvider);
                BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle, 0);
            }



            strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");


            StartStrategy();
        }
    }
}











