using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using SubStratLib;
using CheckLib;

namespace ExecutionLib
{
    public static class ExecutionOps
    
    {

        public static Dictionary<Instrument, double> consolidatePositionsDaily(List<SubStrategy> subStratList, InstrumentList Instruments, Debugger mydebug,double TOTAL_EXPOSURE,Dictionary<Instrument, double> lastPrices)
        {
            Dictionary<Instrument, double> targetWeigts = new Dictionary<Instrument, double>();
			Dictionary<Instrument, double> targetWeigts2 = new Dictionary<Instrument, double>();
            
            foreach (Instrument ins in Instruments)
            {
                targetWeigts[ins] = 0;
            }
            foreach (SubStrategy mySub in subStratList)
            {
                Instrument ins1 = mySub.Ins1;
                Instrument ins2 = mySub.Ins2;

                double ins1W = mySub.targetpositionIns1;
                double ins2W = mySub.targetpositionIns2;

                targetWeigts[ins1] = targetWeigts[ins1] + ins1W;
                targetWeigts[ins2] = targetWeigts[ins2] + ins2W;

            }

            double totalLong = 0;
            double totalShort = 0;
            foreach (Instrument ins in targetWeigts.Keys)
            {
                double myval = targetWeigts[ins];
                double lastPrice = lastPrices[ins];
                if (myval>0)
                {
                    totalLong = totalLong + myval* lastPrice;
                }
                else if (myval < 0)
                {
                    totalShort = totalShort + myval* lastPrice;
                }
                
            }

            foreach (Instrument ins in targetWeigts.Keys)
            {
                double myval = targetWeigts[ins]* lastPrices[ins];
                double percent = 0;
                if (myval > 0)
                {
                    percent = myval/totalLong;
                }
                else if (myval < 0)
                {
                    percent = myval / totalShort*-1;
                }
                mydebug.WriteLine("PERCENT "+ins+" "+percent);
				targetWeigts2[ins]=percent;

            }
			foreach (Instrument ins in targetWeigts2.Keys)
			{
				double myval = targetWeigts2[ins];
				double target = (TOTAL_EXPOSURE / 2*myval)/lastPrices[ins];

				mydebug.WriteLine("TARGET "+ins+" "+target);
				if (target>0)
				{
					targetWeigts[ins]=Math.Floor(target);
				}
				if (target<0)
				{
					targetWeigts[ins]=Math.Ceiling(target);
				}							
			
			}


            double sumexposure = 0;
            double shortexposure = 0;
            double longexposure = 0;

            foreach (Instrument ins in targetWeigts.Keys)
            {
                double exposure = targetWeigts[ins] * lastPrices[ins];
                mydebug.WriteLine(ins + " " + targetWeigts[ins] + " Exposure: " + exposure);
                sumexposure = sumexposure + exposure;
                if (exposure < 0)
                {
                    shortexposure = shortexposure + exposure;
                }
                if (exposure > 0)
                {
                    longexposure = longexposure + exposure;
                }

            }
            mydebug.WriteLine("Sum Exposure: " + sumexposure);
            mydebug.WriteLine("Long Exposure: " + longexposure);
            mydebug.WriteLine("Short Exposure: " + shortexposure);
            double absExposure = Math.Abs(shortexposure) + longexposure;
            mydebug.WriteLine("Abs Exposure: " + absExposure);
            return targetWeigts;
        }

        public static Dictionary<Instrument, double> priceDict(List<SubStrategy> subStratList)
        {
            Dictionary<Instrument, double> lastPrices = new Dictionary<Instrument, double>();
            foreach (SubStrategy mySub in subStratList)
            {
                Instrument ins1 = mySub.Ins1;
                Instrument ins2 = mySub.Ins2;

                lastPrices[ins1] = mySub.Ins1Last;
                lastPrices[ins2] = mySub.Ins2Last;
            }
            return lastPrices;
        }

        public static int changed(List<SubStrategy> subStratList)
        {
            int changed = 0;
            foreach (SubStrategy mySub in subStratList)
            {
                if (mySub.change==1)
                {
                    changed++;
                    mySub.change = 0;

                }
            }
            if (changed>0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }



        public static void executionLogic(List<SubStrategy> subStratList, InstrumentList Instruments, Strategy Strategy, Portfolio Portfolio, Debugger mydebug,double TOTAL_EXPOSURE)
        {
            Dictionary<Instrument, double> lastPrices = priceDict(subStratList);


            double stable_exposure = exposureCalc(Instruments, lastPrices, Portfolio);
            double delta = Math.Abs(stable_exposure - TOTAL_EXPOSURE) / TOTAL_EXPOSURE;
            int isChanged = changed(subStratList);
            mydebug.WriteLine("BEFORE REBALANCE: Exposure: " + stable_exposure + " Delta: " + delta+ " Changed: "+isChanged);
			Dictionary<Instrument, double> targets = consolidatePositionsDaily(subStratList, Instruments, mydebug,TOTAL_EXPOSURE,lastPrices);
			executionEngine(targets, Strategy, Portfolio, mydebug);  
           
                
        }


        public static  void executionEngine(Dictionary<Instrument, double> targetWeigts,Strategy Strategy,Portfolio Portfolio, Debugger mydebug)
        {
            // get current and targeted exposure, if it is larger than 10% reduce else add mint to the porfolio.



            foreach (Instrument ins in targetWeigts.Keys)
            {
                int target = (int)targetWeigts[ins];
                int current = (int)Portfolio.GetPosition(ins).Amount;
                mydebug.WriteLine("Instrument: " + ins + " Target: " + target + " Current: " + current);
                executor(ins, target, current, Strategy,mydebug);

            }


        }

        public static double exposureCalc(InstrumentList Instruments, Dictionary<Instrument, double> lastPrices,Portfolio Portfolio)
        {
            double total_exposure = 0;
            foreach (Instrument ins in Instruments)
            {
                double current = (double)Portfolio.GetPosition(ins).Amount;
                double exposure = lastPrices[ins] * current;
                total_exposure = total_exposure + Math.Abs(exposure);
            }
            return total_exposure;
        }




        public static void executor(Instrument ins, int targetPosition, int currentPosition,Strategy Strategy, Debugger mydebug)
        {
            int lots = 0;
            if (targetPosition > 0)
            {
                if (currentPosition > 0)
                {
                    int dif = targetPosition - currentPosition;
                    if (dif > 0)
                    {
                        lots = dif;
                        Order order = Strategy.SellOrder(ins, lots, "Rebalance Buy Long to More Long");
                        order.ClientID = "ASIAN_ETF";
                        Strategy.Send(order);

                       
                    }
                    else if (dif < 0)
                    {
                        lots = dif * -1;
                        Order order = Strategy.SellOrder(ins, lots, "Rebalance  Sell Long to Less  Long");
                        order.ClientID = "ASIAN_ETF";
                        Strategy.Send(order);

    
                    }
                    else
                    {

                    }


                }
                else if (currentPosition == 0)
                {

                    lots = targetPosition;
                    Order order = Strategy.BuyOrder(ins, lots, "Rebalance Buy Zero to Long");
                    order.ClientID = "ASIAN_ETF";
                    Strategy.Send(order);

                }
                else if (currentPosition < 0)
                {
                    lots = targetPosition - currentPosition;

                    Order order = Strategy.BuyOrder(ins, lots, "Rebalance  Buy Short to Long");
                    order.ClientID = "ASIAN_ETF";
                    Strategy.Send(order);


                }
            }

            else if (targetPosition < 0)
            {
                if (currentPosition > 0)
                {
                    lots = currentPosition - targetPosition;

                    Order order = Strategy.SellOrder(ins, lots, "Rebalance  Long to Short");
                    order.ClientID = "ASIAN_ETF";
                    Strategy.Send(order);


                    //Strategy.Sell(ins, lots, "Rebalance  Long to Short");
                }
                else if (currentPosition == 0)
                {
                    lots = targetPosition * -1;

                    Order order = Strategy.SellOrder(ins, lots, "Rebalance  Zero to Short");
                    order.ClientID = "ASIAN_ETF";
                    Strategy.Send(order);

                    //Strategy.Sell(ins, lots, "Rebalance  Zero to Short");
                }
                else if (currentPosition < 0)
                {
                    int dif = targetPosition - currentPosition;//-2,-10
                    if (dif > 0)
                    {
                        //current>target -2,-5
                        lots = dif;
                        Order order = Strategy.BuyOrder(ins, lots, "Rebalance Sell Short to More Short");
                        order.ClientID = "ASIAN_ETF";
                        Strategy.Send(order);
                        //Strategy.Buy(ins, lots, "Rebalance Sell Short to More Short");
                    }
                    else if (dif < 0)
                    {
                        //current<target -5,-2
                        lots = dif * -1;
                        Order order = Strategy.SellOrder(ins, lots, "Rebalance Sell Short to MoreShort");
                        order.ClientID = "ASIAN_ETF";
                        Strategy.Send(order);

                        //Strategy.Sell(ins, lots, "Rebalance Sell Short to MoreShort");
                    }
                    else
                    {

                    }


                }
            }

            else if (targetPosition == 0)
            {
                if (currentPosition > 0)
                {
                    lots = currentPosition;
                    Order order = Strategy.SellOrder(ins, lots, "Rebalance Sell Long to Zero");
                    order.ClientID = "ASIAN_ETF";
                    Strategy.Send(order);
                    //Strategy.Sell(ins, lots, "Rebalance Sell Long to Zero");

                }
                else if (currentPosition == 0)
                {

                }
                else if (currentPosition < 0)
                {
                    lots = currentPosition * -1;
                    Order order = Strategy.BuyOrder(ins, lots, "Rebalance Buy Sell to Zero");
                    order.ClientID = "ASIAN_ETF";
                    Strategy.Send(order);
                   // Strategy.Buy(ins, lots, "Rebalance Buy Sell to Zero");

                }
            }

        }

        

        public static Dictionary<Instrument, double> consolidatePositions(List<SubStrategy> subStratList, InstrumentList Instruments,Debugger mydebug)
        {
            Dictionary<Instrument, double> targetWeigts = new Dictionary<Instrument, double>();
            Dictionary<Instrument, double> lastPrices = new Dictionary<Instrument, double>();
            foreach (Instrument ins in Instruments)
            {
                targetWeigts[ins] = 0;
            }
            foreach (SubStrategy mySub in subStratList)
            {
                Instrument ins1 = mySub.Ins1;
                Instrument ins2 = mySub.Ins2;

                double ins1W = mySub.targetpositionIns1;
                double ins2W = mySub.targetpositionIns2;

                targetWeigts[ins1] = targetWeigts[ins1] + ins1W;
                targetWeigts[ins2] = targetWeigts[ins2] + ins2W;
                lastPrices[ins1] = mySub.Ins1Last;
                lastPrices[ins2] = mySub.Ins2Last;
            }
            double sumexposure = 0;
            double shortexposure = 0;
            double longexposure = 0;

            foreach (Instrument ins in targetWeigts.Keys)
            {
                double exposure = targetWeigts[ins] * lastPrices[ins];

                mydebug.WriteLine(ins + " " + targetWeigts[ins] + " Exposure: " + exposure);
                sumexposure = sumexposure + exposure;
                if (exposure < 0)
                {
                    shortexposure = shortexposure + exposure;
                }
                if (exposure > 0)
                {
                    longexposure = longexposure + exposure;
                }

            }
            mydebug.WriteLine("Sum Exposure: " + sumexposure);
            mydebug.WriteLine("Long Exposure: " + longexposure);
            mydebug.WriteLine("Short Exposure: " + shortexposure);
            double absExposure = Math.Abs(shortexposure) + longexposure;
            mydebug.WriteLine("Abs Exposure: " + absExposure);

            //check exposure for each strategy
            // if it is larger than allowed by 10% reduce it 

            return targetWeigts;
        }
    }
}





