using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using SmartQuant;
using SubStratLib;
using CheckLib;

namespace  QuantLib
{

    public static class QuantOps
    {
        public static void updateTargetWeights(List<SubStrategy> mysubStratList, Debugger mydebug)
        {
            foreach (SubStrategy mySub in mysubStratList)
            {
                int lookBack = mySub.lookBack;
                List<Bar> datap1 = mySub.Ins1Data;
                List<Bar> datap2 = mySub.Ins2Data;

                List<Bar> trimdatap1 = datap1.Skip(Math.Max(0, datap1.Count() - lookBack)).ToList();
                List<Bar> trimdatap2 = datap2.Skip(Math.Max(0, datap2.Count() - lookBack)).ToList();

                double p1price = (double)trimdatap1[trimdatap1.Count() - 1].Close;
                double p2price = (double)trimdatap2[trimdatap2.Count() - 1].Close;



                List<double> spreadList = new List<double>();
                for (int i = 0; i < lookBack; i++)
                {
                    double spr = (double)trimdatap1[i].Close - (double)trimdatap2[i].Close;
                    spreadList.Add(spr);
                }
                Console.WriteLine(spreadList.Count);
                Console.WriteLine(spreadList.Count);

                List<double> last5p1close = new List<double>();
                List<double> last5p2close = new List<double>();
                for (int i = 0; i < lookBack; i++)
                {
                    double valp1 = (double)p1price;
                    double valp2 = (double)p2price;
                    last5p1close.Add(valp1);
                    last5p2close.Add(valp2);
                }




                double mean = spreadList.Average();
                double std = CalculateStdDev(spreadList);

                
                double x_smooth = last5p1close.Average();
                double y_smooth = last5p2close.Average();
                double x_quantity = mySub.allocation * 0.5 / x_smooth;
                double y_quantity = mySub.allocation * 0.5 / y_smooth;

                double lastp1 = p1price;
                double lastp2 = p2price;
                double last_spread = spreadList[spreadList.Count - 1];
                mySub.Ins1Last = lastp1;
                mySub.Ins2Last = lastp2;


                mySub.Exposure = (lastp1 * Math.Abs(mySub.targetpositionIns1) + lastp2 * Math.Abs(mySub.targetpositionIns2));

                double zScore = (last_spread - mean) / std;
                double signal = Math.Truncate(zScore);
				if (signal>mySub.zLimit)
				{
					signal=mySub.zLimit;
				}
				else if (signal<-mySub.zLimit)
				{
					signal=-1*mySub.zLimit;
				}
                
                mySub.targetpositionIns1 = signal * -1;
                mySub.targetpositionIns2 =  signal;
                
                if (signal == mySub.zSignal)
                {
                    mySub.change = 0;
                    mySub.zSignal = (int) signal;
                }

                else
                {
                    mySub.zSignal = (int) signal;
                    mySub.change = 1;

                }

  
                //Console.WriteLine(mySub.Ins1.Symbol + " " + mySub.targetpositionIns1 + " " + mySub.zSignal);
                //Console.WriteLine(mySub.Ins2.Symbol + " " + mySub.targetpositionIns2 + " " + mySub.zSignal);



            }
        }

        public static double CalculateStdDev(List<double> values)
        {
            double ret = 0;
            if (values.Count() > 0)
            {
                //Compute the Average      
                double avg = values.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (values.Count()));
            }
            return ret;
        }
    }

    public static class CombinGenerator
    {

        class ListComparer : IEqualityComparer<List<string>>
        {
            public bool Equals(List<string> x, List<string> y)
            {
                if (x == y)
                    return true;

                if (x == null || y == null)
                    return false;

                // Order if you need

                return x.SequenceEqual(y);
            }

            public int GetHashCode(List<string> obj)
            {
                if (obj == null)
                    return 0;

                unchecked
                {
                    return obj.Select(e => e.GetHashCode()).Aggregate(17, (a, b) => 23 * a + b);
                }
            }
        }
        public static List<List<string>> generateCombinationsAndFilter(InstrumentList Instruments, Debugger mydebug)
        {
            //CombinGenerator CombinGener = new CombinGenerator();
            List<List<string>> combinList = new List<List<string>>();
            List<string> itemtracker = new List<string>();
            foreach (Instrument ins in Instruments)
            {
                foreach (Instrument id in Instruments)
                {
                    if (id != ins)
                    {
                        List<string> item = new List<string>();
                        item.Add(ins.Symbol);
                        item.Add(id.Symbol);
                        item.Sort();
                        string trackitem = ins.Symbol + id.Symbol;
                        combinList.Add(item);
                    }
                }
            }
                List<List<string>> combinListDistinct = combinList.Distinct(new ListComparer()).ToList();

                foreach (List<string> comb in combinListDistinct)
                {
                    foreach (string z in comb)
                    {
                        Console.Write(z + " ");
                    }
                    Console.WriteLine(" ");
                }
                mydebug.WriteLine("Total Instrument Combinations Generated: " + combinListDistinct.Count);
                return combinListDistinct;
                // Filter the combinations:

            }
        }
    }

   













