using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using CheckLib;

namespace DataOpsLib
{
   public static class DataOps
    {
        public static BarSeries getBarsForMe(Instrument instrument, int lookback_hours, StrategyManager StrategyManager, DataManager DataManager, Clock Clock, double thold,Debugger mydebug)
        {
            DateTime historicalEnd = Clock.DateTime;
            DateTime start = Clock.DateTime.AddDays((lookback_hours * 2) * -1).Date;
            BarSeries response = new BarSeries();
            if (StrategyManager.Mode == StrategyMode.Live)

            {

                response = DataManager.GetHistoricalBars("IB", instrument, start, historicalEnd, BarType.Time, 60 * 30);


            }
            else
            {

                response = DataManager.GetHistoricalBars(instrument, start, historicalEnd, BarType.Time, 60 * 30);

            }

            return clearSeries(response, instrument, thold,Clock,mydebug);


        }

        public static BarSeries clearSeries(BarSeries inputSeries, Instrument instrument, double thold,Clock Clock, Debugger mydebug)
        {
            double sum = 0;
            double count = 0;

            foreach (Bar mybar in inputSeries)
            {
                count++;
                sum = sum + mybar.Close;
            }

            double averageClose = sum / count;

            BarSeries responseBars = new BarSeries();

            foreach (Bar mybar in inputSeries)
            {
                int checkBarint = checkVal(mybar, Clock, thold, instrument, averageClose,mydebug);
                if (checkBarint == 1)
                {
                    responseBars.Add(mybar);
                }
            }
            return responseBars;
        }

        public static int checkVal(Bar mybar, Clock Clock, double thold, Instrument instrument, double averageClose, Debugger mydebug)
        {
            //check the zeros and illogicla values in the dict

            DateTime mydate = mybar.CloseDateTime;
            double myval = mybar.Close;


            if (mydate > Clock.DateTime)
            {
                mydebug.WriteLine(String.Format("Forward Date Detected! Instrument: {0} DateTime: {1} Value {2} ", instrument, mydate, myval));
                return 0;
            }

            else if (myval <= 0)
            {
                mydebug.WriteLine(String.Format("Neg or Zero Value Detected! Instrument: {0} DateTime: {1} Value {2} ", instrument, mydate, myval));
                return 0;
            }
            else
            {
                double last_mid = averageClose;
                double difference = Math.Abs(myval - last_mid) / myval;

                if (difference > thold)
                {
                    mydebug.WriteLine(String.Format("Illogical Value Detected! Instrument: {0} DateTime: {1} Value {2} Last Mid {3} ", instrument, mydate, myval, last_mid));
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        }
        public static Dictionary<Instrument, List<Bar>> processResampled(Dictionary<Instrument, BarSeries> dataBase,InstrumentManager InstrumentManager,InstrumentList Instruments)
        {
            // get range of first day and lasy day

            List<DateTime> datesList = new List<DateTime>();
            List<DateTime> dayList = new List<DateTime>();
            foreach (Instrument ins in Instruments)
            {
                BarSeries myBars = dataBase[ins];
                foreach (Bar mbar in myBars)
                {
                    datesList.Add(mbar.CloseDateTime);
                    dayList.Add(mbar.CloseDateTime.Date);
                }
            }
            List<DateTime> distinct = datesList.Distinct().ToList();
            distinct.Sort((a, b) => a.CompareTo(b));

            List<DateTime> distinctDay = dayList.Distinct().ToList();
            distinctDay.Sort((a, b) => a.CompareTo(b));

            List<DateTime> lastDateTime = new List<DateTime>();
            foreach (DateTime mday in distinctDay)
            {
                List<DateTime> dayListed = new List<DateTime>();
                foreach (DateTime mdate in distinct)
                {

                    if (mday == mdate.Date)
                    {
                        dayListed.Add(mdate);
                    }

                }
                lastDateTime.Add(dayListed[dayListed.Count - 1]);

            }


            List<DateTime> lastDateTimeTrimmed = lastDateTime.Skip(Math.Max(0, lastDateTime.Count() - 300)).ToList();

            List<List<Bar>> alignedList = new List<List<Bar>>();
            List<int> validList = new List<int>();

            foreach (Instrument ins in Instruments)
            {
                List<Bar> alignedItem = new List<Bar>();
                foreach (DateTime dtime in lastDateTimeTrimmed)
                {
                    int added = 0;
                    foreach (Bar mbar in dataBase[ins])
                    {
                        if (mbar.CloseDateTime == dtime)
                        {
                            added = 1;
                            alignedItem.Add(mbar);
                        }

                    }
                    if (added == 0)
                    {
                        Bar emptyBar = new Bar(dtime, dtime.AddMinutes(30), ins.Id, BarType.Time, 30 * 60, 0, 0, 0, 0, 0, 0);
                        alignedItem.Add(emptyBar);
                    }
                }
                alignedList.Add(alignedItem);

            }
            Dictionary<Instrument, List<Bar>> alignedDb = new Dictionary<Instrument, List<Bar>>();

            for (int i = 0; i < alignedList[0].Count; i++)
            {
                for (int j = 0; j < alignedList.Count; j++)
                {

                
                    Bar a1 = alignedList[j][i];
                    //Bar a2 = alignedList[1][i];
                    //Bar a3 = alignedList[2][i];
                    //Bar a4 = alignedList[3][i];
                    if (i == 0)
                    {
                        alignedDb[InstrumentManager.GetById(a1.InstrumentId)] = new List<Bar>();
                       // alignedDb[InstrumentManager.GetById(a2.InstrumentId)] = new List<Bar>();
                       // alignedDb[InstrumentManager.GetById(a3.InstrumentId)] = new List<Bar>();
                       // alignedDb[InstrumentManager.GetById(a4.InstrumentId)] = new List<Bar>();

                    }
                    if (a1.Close == 0)
                    {

                    }
                    else
                    {

                        alignedDb[InstrumentManager.GetById(a1.InstrumentId)].Add(a1);
                       // alignedDb[InstrumentManager.GetById(a2.InstrumentId)].Add(a2);
                       // alignedDb[InstrumentManager.GetById(a3.InstrumentId)].Add(a3);
                       // alignedDb[InstrumentManager.GetById(a4.InstrumentId)].Add(a4);
                    }
                }
            }
            return alignedDb;

        }



    }



}




