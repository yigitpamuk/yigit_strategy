using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using SubStratLib;
using CheckLib;

namespace ExecutionLib
{
    public static class ExecutionOps

    {
        
        public static Dictionary<Instrument, double> consolidatePositions(List<SubStrategy> subStratList, InstrumentList Instruments, Debugger mydebug)
        {
            Dictionary<Instrument, double> targetWeigts = new Dictionary<Instrument, double>();
            foreach (Instrument ins in Instruments)
            {
                targetWeigts[ins] = 0;
            }
   

            foreach (SubStrategy mySub in subStratList)
            {
                Instrument ins1 = mySub.Ins1;
                Instrument ins2 = mySub.Ins2;
                Instrument ins3 = mySub.Ins3;

                double ins1W = mySub.Ins1Target;
                double ins2W= mySub.Ins2Target;
                double ins3W= mySub.Ins3Target;

                targetWeigts[ins1] = targetWeigts[ins1] + ins1W;
                targetWeigts[ins2] = targetWeigts[ins2] + ins2W;
                targetWeigts[ins3] = targetWeigts[ins3] + ins3W;

            }
			
            // if sum is 0 no positions, else weight
            
     



            
            return targetWeigts;

        }
        public static void executionLogic(List<SubStrategy> subStratList, InstrumentList Instruments, Strategy Strategy, Portfolio Portfolio, Debugger mydebug, StrategyManager StrategyManager, InstrumentManager InstrumentManager)
        {

            Dictionary<Instrument, double> targets = consolidatePositions(subStratList, Instruments, mydebug);
            executionEngine(targets, Strategy, Portfolio, mydebug,StrategyManager , InstrumentManager );
            
                
        }


        

        public static  void executionEngine(Dictionary<Instrument, double> targetWeigts,Strategy Strategy,Portfolio Portfolio, Debugger mydebug, StrategyManager StrategyManager, InstrumentManager InstrumentManager)
        {
            // get current and targeted exposure, if it is larger than 10% reduce else add mint to the porfolio.



            foreach (Instrument ins in targetWeigts.Keys)
            {
                int target = (int)targetWeigts[ins];
                int current = 0;
                Instrument instrument_cfd = ExecutionOps.CFD(ins, StrategyManager, InstrumentManager);
                mydebug.WriteLine(instrument_cfd.ToString());

                try
                {
                    current = (int)Portfolio.GetPosition(instrument_cfd).Amount;
                }
                catch (NullReferenceException)
                {
                    mydebug.WriteLine("No Positions for " + instrument_cfd.ToString());
                }

                mydebug.WriteLine("Current Position: " + current + " " + instrument_cfd.ToString());


                mydebug.WriteLine("Instrument: " + ins + " Target: " + target + " Current: " + current);
                executor(ins, target, current, Strategy,mydebug,StrategyManager,InstrumentManager);

            }


        }

        public static void executor(Instrument ins, int targetPosition, int currentPosition,Strategy Strategy, Debugger mydebug,StrategyManager StrategyManager,InstrumentManager InstrumentManager)
        {
            Instrument cfdins = CFD(ins,StrategyManager, InstrumentManager);
           
            int lots = 0;
            if (targetPosition > 0)
            {
                if (currentPosition > 0)
                {
                    int dif = targetPosition - currentPosition;
                    if (dif > 0)
                    {
                        lots = dif;
                        Order order = Strategy.BuyOrder(cfdins, lots, "Rebalance Buy Long to More Long");
                        order.ClientID = "MA_SPEED";
                        Strategy.Send(order);

                       
                    }
                    else if (dif < 0)
                    {
                        lots = dif * -1;
                        Order order = Strategy.SellOrder(cfdins, lots, "Rebalance  Sell Long to Less  Long");
                        order.ClientID = "MA_SPEED";
                        Strategy.Send(order);

    
                    }
                    else
                    {

                    }


                }
                else if (currentPosition == 0)
                {

                    lots = targetPosition;
                    Order order = Strategy.BuyOrder(cfdins, lots, "Rebalance Buy Zero to Long");
                    order.ClientID = "MA_SPEED";
                    Strategy.Send(order);

                }
                else if (currentPosition < 0)
                {
                    lots = targetPosition - currentPosition;

                    Order order = Strategy.BuyOrder(cfdins, lots, "Rebalance  Buy Short to Long");
                    order.ClientID = "MA_SPEED";
                    Strategy.Send(order);


                }
            }

            else if (targetPosition < 0)
            {
                if (currentPosition > 0)
                {
                    lots = currentPosition - targetPosition;

                    Order order = Strategy.SellOrder(cfdins, lots, "Rebalance  Long to Short");
                    order.ClientID = "MA_SPEED";
                    Strategy.Send(order);


                    //Strategy.Sell(cfdins, lots, "Rebalance  Long to Short");
                }
                else if (currentPosition == 0)
                {
                    lots = targetPosition * -1;

                    Order order = Strategy.SellOrder(cfdins, lots, "Rebalance  Zero to Short");
                    order.ClientID = "MA_SPEED";
                    Strategy.Send(order);

                    //Strategy.Sell(cfdins, lots, "Rebalance  Zero to Short");
                }
                else if (currentPosition < 0)
                {
                    int dif = targetPosition - currentPosition;//-2,-10
                    if (dif > 0)
                    {
                        //target>current -2,-5
                        lots = dif;
                        Order order = Strategy.BuyOrder(cfdins, lots, "Rebalance  Short to Less Short");
                        order.ClientID = "MA_SPEED";
                        Strategy.Send(order);
                        //Strategy.Buy(cfdins, lots, "Rebalance Sell Short to More Short");
                    }
                    else if (dif < 0)
                    {
                        //target<current -5,-2
                        lots = dif * -1;
                        Order order = Strategy.SellOrder(cfdins, lots, "Rebalance Sell Short to MoreShort");
                        order.ClientID = "MA_SPEED";
                        Strategy.Send(order);

                        //Strategy.Sell(cfdins, lots, "Rebalance Sell Short to MoreShort");
                    }
                    else
                    {

                    }


                }
            }

            else if (targetPosition == 0)
            {
                if (currentPosition > 0)
                {
                    lots = currentPosition;
                    Order order = Strategy.SellOrder(cfdins, lots, "Rebalance Sell Long to Zero");
                    order.ClientID = "MA_SPEED";
                    Strategy.Send(order);
                    //Strategy.Sell(cfdins, lots, "Rebalance Sell Long to Zero");

                }
                else if (currentPosition == 0)
                {

                }
                else if (currentPosition < 0)
                {
                    lots = currentPosition * -1;
                    Order order = Strategy.BuyOrder(cfdins, lots, "Rebalance Buy Sell to Zero");
                    order.ClientID = "MA_SPEED";
                    Strategy.Send(order);
                   // Strategy.Buy(cfdins, lots, "Rebalance Buy Sell to Zero");

                }
            }

        }
        public static Instrument CFD(Instrument instrument, StrategyManager StrategyManager, InstrumentManager InstrumentManager)
        {

            if (StrategyManager.Mode == StrategyMode.Backtest)
            {
                return instrument;
            }
            else if (StrategyManager.Mode == StrategyMode.Live)
            {
                return InstrumentManager.GetBySymbol(instrument.Symbol + ".CFD");
            }
            else
            {
                return InstrumentManager.GetBySymbol(instrument.Symbol + ".CFD");
            }
            //return instrument;
        }



    }
}






