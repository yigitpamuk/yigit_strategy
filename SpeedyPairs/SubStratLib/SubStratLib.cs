using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using CheckLib;

namespace SubStratLib
{
    public class SubStrategy
    // This class is the backbone for multi strategy implementation, we will loop throgh list of SubStrategies
    {
        public Instrument Ins1 { get; set; }
        public Instrument Ins2 { get; set; }
        public Instrument Ins3 { get; set; }

        public List<Bar> Ins1Data { get; set; }
        public List<Bar> Ins2Data { get; set; }
        public List<Bar> Ins3Data { get; set; }

        public int Ins1Dir { get; set; }
        public int Ins2Dir { get; set; }
        public int Ins3Dir { get; set; }

        public int inpos { get; set; }
        public double thold{ get; set; }
        public int lookBack { get; set; }

        public double Ins1Last { get; set; }
        public double Ins2Last { get; set; }
        public double Ins3Last { get; set; }

        public double Ins1Target { get; set; }
        public double Ins2Target { get; set; }
        public double Ins3Target { get; set; }

        public double Ins1Entry { get; set; }
        public double Ins2Entry { get; set; }
        public double Ins3Entry { get; set; }

        public int clipSize { get; set; }
        public string strategyId { get; set; }

        public bool gear { get; set; }
        public int gearType { get; set; }
        public bool takeProfit { get; set; }
        public double curPnl { get; set; }
        public double profitTarget { get; set; }
        public double profitTargetFix { get; set; }
    }

    public static class SubStrategyOps
    {
        public static List<SubStrategy> initSubStrategies(List<List<string>> pairs , List<int> lookBacks, List<double> thresHolds, List<double> targets, InstrumentManager InstrumentManager,Debugger mydebug,int clipSize)
        {

    
            List<SubStrategy> resultList = new List<SubStrategy>();
            foreach (List<string> pair in pairs)
            {
                foreach (int lookBack in lookBacks)
                {

                    foreach (double tHold in thresHolds)
                    {
                        foreach (double target in targets)
                        {

                            SubStrategy mySubStrat = new SubStrategy();
                            mySubStrat.Ins1 = InstrumentManager.GetBySymbol(pair[0]);
                            mySubStrat.Ins2 = InstrumentManager.GetBySymbol(pair[1]);
                            mySubStrat.Ins3 = InstrumentManager.GetBySymbol(pair[2]);

                            mySubStrat.Ins1Data = new List<Bar>();
                            mySubStrat.Ins2Data = new List<Bar>();
                            mySubStrat.Ins3Data = new List<Bar>();

                            mySubStrat.Ins1Dir = 1;
                            mySubStrat.Ins2Dir = -1;
                            mySubStrat.Ins3Dir = -1;

                            mySubStrat.thold = tHold;

                            mySubStrat.inpos = 0;
                            mySubStrat.lookBack = lookBack;

                            mySubStrat.Ins1Last = 0;
                            mySubStrat.Ins2Last = 0;
                            mySubStrat.Ins3Last = 0;

                            mySubStrat.Ins1Target = 0;
                            mySubStrat.Ins2Target = 0;
                            mySubStrat.Ins3Target = 0;

                            mySubStrat.clipSize = clipSize;
                            mySubStrat.gear = true;
                            mySubStrat.gearType = 0;
                            mySubStrat.takeProfit = false;
                            mySubStrat.curPnl = 0;
                            mySubStrat.profitTarget = target;
                            mySubStrat.profitTargetFix = target;

                            mySubStrat.Ins1Entry = 0;
                            mySubStrat.Ins2Entry = 0;
                            mySubStrat.Ins3Entry = 0;

                            mySubStrat.strategyId = mySubStrat.Ins1 + " | " + mySubStrat.Ins2 + " | " + mySubStrat.Ins3 + " | " + mySubStrat.lookBack.ToString() + " | " + mySubStrat.thold.ToString() + " | "+ mySubStrat.profitTarget.ToString();
                            resultList.Add(mySubStrat);
                            mydebug.WriteLine(mySubStrat.strategyId + " Created");
                        }
                    }
                }
            }
            updateDirections(resultList);
            mydebug.WriteLine(" Instrument Parameter Combinations Created: " + resultList.Count);
            mydebug.WriteLine("--------------------------------");
            return resultList;





        }

        public static void updateDirections(List<SubStrategy> mysubStratList)
        {
            foreach (SubStrategy mySub in mysubStratList)
            {
                string p1 = mySub.Ins1.Symbol;
                if (p1 == "USDCAD")
                {
                    mySub.Ins1Dir = mySub.Ins1Dir * -1;
                }

                string p2 = mySub.Ins2.Symbol;
                if (p2 == "USDCAD")
                {
                    mySub.Ins2Dir = mySub.Ins2Dir * -1;
                }
                string p3 = mySub.Ins3.Symbol;
                if (p3 == "USDCAD")
                {
                    mySub.Ins3Dir = mySub.Ins3Dir * -1;
                }


            }
        }

        public static void updateData(List<SubStrategy> mysubStratList, Dictionary<Instrument,BarSeries> alignedDict,Debugger mydebug)
        {
            foreach (SubStrategy mySub in mysubStratList)
            {

                Instrument p1 = mySub.Ins1;
                Instrument p2 = mySub.Ins2;
                Instrument p3 = mySub.Ins3;


                mySub.Ins1Data = convertSeriestoBars(alignedDict[p1]);
                mySub.Ins2Data = convertSeriestoBars(alignedDict[p2]);
                mySub.Ins3Data = convertSeriestoBars(alignedDict[p3]);
            }

        }

        public static List<Bar> convertSeriestoBars(BarSeries inputSeries)
        {
            List<Bar> mybars = new List<Bar>();

            for (int i = 0; i < inputSeries.Count; i++)
            {
                mybars.Add(inputSeries[i]);

            }
            return mybars;
        }
        public static int checkInpos(List<SubStrategy> subStrategies)
        {
            int inposCount = 0;
            foreach (SubStrategy strategy in subStrategies)
            {
                if (strategy.inpos != 0)
                {
                    inposCount++;
                }
            }
            if (inposCount > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }




}

