using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using CheckLib;

namespace DataOpsLib
{
   public static class DataOps
    {
        public static BarSeries getBarsForMe(Instrument instrument, int lookback_hours, StrategyManager StrategyManager, DataManager DataManager, Clock Clock, double thold,Debugger mydebug)
        {
            DateTime historicalEnd = Clock.DateTime;
            DateTime start = Clock.DateTime.AddHours((lookback_hours * 8) * -1).Date;
            BarSeries response = new BarSeries();
            if (StrategyManager.Mode == StrategyMode.Live)

            {

                response = DataManager.GetHistoricalBars("IB", instrument, start, historicalEnd, BarType.Time, 60 * 60 );




            }
            else
            {

                response = DataManager.GetHistoricalBars(instrument, start, historicalEnd, BarType.Time, 60 * 60*4);
                return clearSeries(response, instrument, thold, Clock, mydebug);

            }
            BarSeries clearedSeries= clearSeries(response, instrument, thold, Clock, mydebug);
            //convert bartimes to US
            // correct the 15 minute thing
            // 
            BarSeries convertedSeries = timeAlignment(clearedSeries);

            // resample stuff
            BarSeries resampledSeries = resample(convertedSeries);
            return resampledSeries;


        }

        public static BarSeries timeAlignment(BarSeries inputSeries)
        {
            BarSeries response = new BarSeries();
            foreach (Bar myBar in inputSeries)
            {
                
                DateTime openDateTime = adjustTime(myBar.OpenDateTime);
                DateTime closeDateTime = adjustTime(myBar.CloseDateTime);
   
                Bar newBar = new Bar(openDateTime, closeDateTime, myBar.ProviderId,myBar.InstrumentId, myBar.Type, myBar.Size, myBar.Open, myBar.High,myBar.Low, myBar.Close);
                response.Add(newBar);
                
                    
            }
            return response;
        }
        public static BarSeries resample(BarSeries inputSeries)
        {
            BarSeries response = new BarSeries();
            Bar checkBar = new Bar();
            foreach (Bar myBar in inputSeries)
            {
                if (myBar.OpenDateTime.Hour%4==0)
                {
                    // find the close bar:
                    Bar openBar = myBar;
                    DateTime closeTime = openBar.OpenDateTime.AddHours(3);
                    
                    Bar closeBar = getCloseBar(inputSeries, closeTime);
                    if (closeBar==checkBar)
                    {

                    }
                    else
                    {
                        DateTime openDateTime = (openBar.OpenDateTime);
                        DateTime closeDateTime = closeBar.CloseDateTime;
                        Bar newBar = new Bar(openDateTime, closeDateTime, myBar.ProviderId, myBar.InstrumentId, myBar.Type, myBar.Size*4, myBar.Open, myBar.High, myBar.Low, closeBar.Close);
                        response.Add(newBar);
                    }

                }
            }
            return response;

        }

        public static Bar getCloseBar(BarSeries inputSeries, DateTime searchTime)
        {

            foreach (Bar myBar in inputSeries)
            {
                if (myBar.OpenDateTime == searchTime)
                {
                    return myBar;
                }

            }
            Bar response = new Bar();
            return response;
        }
        public static DateTime adjustTime(DateTime inputTime )
        {
            // if minute == 15 revert it back
            if (inputTime.Minute==15)
            {
                inputTime = inputTime.AddMinutes(-15);
            }
            TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime USTime = TimeZoneInfo.ConvertTime(inputTime.ToUniversalTime(), convertZone);

            return USTime;
        }

        public static BarSeries clearSeries(BarSeries inputSeries, Instrument instrument, double thold,Clock Clock, Debugger mydebug)
        {
            double sum = 0;
            double count = 0;

            foreach (Bar mybar in inputSeries)
            {
                count++;
                sum = sum + mybar.Close;
            }

            double averageClose = sum / count;

            BarSeries responseBars = new BarSeries();

            foreach (Bar mybar in inputSeries)
            {
                int checkBarint = checkVal(mybar, Clock, thold, instrument, averageClose,mydebug);
                if (checkBarint == 1)
                {
                    responseBars.Add(mybar);
                }
            }
            return responseBars;
        }

        public static int checkVal(Bar mybar, Clock Clock, double thold, Instrument instrument, double averageClose, Debugger mydebug)
        {
            //check the zeros and illogicla values in the dict

            DateTime mydate = mybar.CloseDateTime;
            double myval = mybar.Close;


            if (mydate > Clock.DateTime)
            {
                mydebug.WriteLine(String.Format("Forward Date Detected! Instrument: {0} DateTime: {1} Value {2} ", instrument, mydate, myval));
                return 0;
            }

            else if (myval <= 0)
            {
                mydebug.WriteLine(String.Format("Neg or Zero Value Detected! Instrument: {0} DateTime: {1} Value {2} ", instrument, mydate, myval));
                return 0;
            }
            else
            {
                double last_mid = averageClose;
                double difference = Math.Abs(myval - last_mid) / myval;

                if (difference > thold)
                {
                    mydebug.WriteLine(String.Format("Illogical Value Detected! Instrument: {0} DateTime: {1} Value {2} Last Mid {3} ", instrument, mydate, myval, last_mid));
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        }
 


    }



}






