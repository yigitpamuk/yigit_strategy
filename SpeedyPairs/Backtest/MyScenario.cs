using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;
using CheckLib;


namespace OpenQuant
{
    public partial class Scenerio : Scenario
    {   //CHANGE WHEN  LIVE
        List<string> listCurrencies = new List<string> {"EURUSD_M","GBPUSD","AUDUSD"};

        private long barSize = 60 * 60*4;

        public Scenerio(Framework framework)
            : base(framework)
        {
        }

        public override void Run()
        {

            string datesting = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();
            string filename = datesting + "_BACKTEST_LOGS.txt";
            string location = "C:\\Users\\yigit.pamuk\\oq_logs\\speedypairs\\" + filename;
            Debugger mydeBug = new Debugger(location, true);
			string name = "SPEEDYPAIRS_BACKTEST";
		
            StrategyManager.Mode = StrategyMode.Backtest;

            strategy = new MyStrategy(framework, "Backtest",mydeBug);
            BarFactory.Clear();

            //strategy.DataProvider = ProviderManager.GetDataProvider("IB");

            foreach (string curr in listCurrencies)
            {
                Instrument instrument = InstrumentManager.GetBySymbol(curr);
                strategy.AddInstrument(instrument, strategy.DataProvider);
                BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle, 0);
            }
            
			StrategyManager.Persistence = StrategyPersistence.None;
            DataSimulator.DateTime1 = new DateTime(2010, 1, 1);
            DataSimulator.DateTime2 = new DateTime(2020, 11, 1);
            Framework.EventBus.ReminderOrder = ReminderOrder.After;
            strategy.ExecutionSimulator.CommissionProvider.Type = CommissionType.Percent;
            strategy.ExecutionSimulator.CommissionProvider.MinCommission = 0;
            strategy.ExecutionSimulator.CommissionProvider.Commission = 0.00002;//slippage and comission
            strategy.ExecutionSimulator.SlippageProvider.Slippage = 0.00002;
            //ExecutionSimulator.FillOnBar = true;
            //ExecutionSimulator.FillMarketOnNext = true;
            //strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");


            StartStrategy();
        }
    }
}
































