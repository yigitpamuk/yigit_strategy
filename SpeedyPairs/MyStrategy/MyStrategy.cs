using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;
using SubStratLib;
using QuantLib;
using DataOpsLib;
using CheckLib;
using ExecutionLib;



namespace OpenQuant
{
    public class MyStrategy : Strategy
    {

        int CLIPSIZE ;
        double illogicThold ;
        int shortDATA;


        public double latestAsk;
        public double latestBid;

        public DateTime latestAskTime;
        public DateTime latestBidTime;
        public DateTime CurrentTime;
        public Dictionary<Instrument, Bar> barCheckerDict;
        public Dictionary<Instrument, TrackingObject> trackObjects;


        string StratName;
        public List<SubStrategy> subStratList;
        bool connected;
        double MAX_EXPOSURE;
        List<int> lookBacks;
        Debugger mydebug;

        int validator;
        double spreadThold;
        int minutes_to_trade;
        DateTime validationTime;

        Dictionary<Instrument,BarSeries> dataBase;
        Dictionary<Instrument, List<bool>> originals;
        public MyStrategy(Framework framework, string name, Debugger mydeBug)
            : base(framework, name)

        {
            mydebug = mydeBug;
            StratName = name;
        }

        protected override void OnStrategyStart()
        {
            mydebug.WriteLine("STRATNAME: KEEP FOR PERSISTENCE: "+ StratName);
            connected = true;
            CurrentTime = Clock.DateTime;
            //init substrategies
            mydebug.WriteLine("Start " + CurrentTime.ToString());

            // STRATEGY PARAMETERS
			GetPositions();

            /// CHANGE WHEN GOING LIVE
            string Instrument1 = "EURUSD_M";
            if (StrategyManager.Mode == StrategyMode.Live)
                Instrument1 = "EURUSD";
            string Instrument2 = "GBPUSD";
            string Instrument3 = "AUDUSD";


            List <List<string>> pairs = new List<List<string>>();
            List<string> p1 = new List<string>() { Instrument1, Instrument2, Instrument3 };
            List<string> p2 = new List<string>() { Instrument2, Instrument1, Instrument3 };
            List<string> p3 = new List<string>() { Instrument3, Instrument1, Instrument2 };

            pairs.Add(p1);
            pairs.Add(p2);
            pairs.Add(p3);


            
           
            CLIPSIZE = 500; // trade amount per substrategy.
            illogicThold = 0.9;// data is illocgical is the difference % is more than this amount from mean
            shortDATA = 100; // short data download len
            spreadThold = 0.0001; // all speads should be less than this value
            minutes_to_trade = 15; // check spread condition for this many minutes, if not satisifed, enter the trade anyways.
            lookBacks = new List<int>() { 150,180,210 }; // 240 mins
            List<double> thresHolds = new List<double>() { 0.0001}; // signal thold
            List<double> targets = new List<double>() {10,25,50 }; // profit Targets 
            MAX_EXPOSURE = CLIPSIZE*20;
			Console.WriteLine("CLIPSIZE: "+CLIPSIZE + "MAX EXPOSURE:" + MAX_EXPOSURE);
            Portfolio.Account.Deposit(MAX_EXPOSURE*0.1, CurrencyId.USD, "Initial allocation");
            subStratList = SubStrategyOps.initSubStrategies(pairs, lookBacks, thresHolds, targets, InstrumentManager, mydebug, CLIPSIZE);


            dataBase = new Dictionary<Instrument, BarSeries>();
            originals = new Dictionary<Instrument, List<bool>>();
            barCheckerDict = new Dictionary<Instrument, Bar>();

            mydebug.WriteLine("Getting Init Data");

            if (StrategyManager.Mode == StrategyMode.Live)
            {
                AddReminder(Clock.DateTime.AddSeconds(60));
            }
            
                


            foreach (Instrument instrument in Instruments)
            {
                BarSeries mydata = DataOps.getBarsForMe(instrument,(int) lookBacks.Max()*2, StrategyManager, DataManager, Clock, illogicThold,mydebug);
                dataBase[instrument]=mydata;
                
                List<bool> originalList = new List<bool>();
                foreach (Bar mbar in mydata)
                {
                    originalList.Add(true);
                    //Console.WriteLine(mbar.OpenDateTime+" "+mbar.CloseDateTime+" "+mbar.DateTime);
                }

                originals[instrument] = originalList;
 
            }


            printLast();
            foreach (Instrument ins in dataBase.Keys)
            {
                /*
                Console.WriteLine("DB" + ins);
                foreach (Bar mbar in dataBase[ins])
                {
                    Console.WriteLine(mbar.InstrumentId + " " + mbar.OpenDateTime + " " + mbar.CloseDateTime + " " + mbar.DateTime + " " + mbar.Close);
                }
                foreach (bool mbol in originals[ins])
                {
                    Console.WriteLine(mbol);
                }
                */

                int current = 0;
                Instrument instrument_cfd = ExecutionOps.CFD(ins, StrategyManager, InstrumentManager);
                mydebug.WriteLine(instrument_cfd.ToString());

                try
                {
                    current = (int)Portfolio.GetPosition(instrument_cfd).Amount;
                }
                catch (NullReferenceException)
                {
                    mydebug.WriteLine("No Positions for "+instrument_cfd.ToString());
                }
                
                mydebug.WriteLine("Current Position: " + current + " "+ instrument_cfd.ToString());
            }



            trackObjects = CheckOps.initTrackingObjects(Instruments,Clock);


        }
        protected override void OnReminder(DateTime signalTime, object data)
        {
            mydebug.WriteLine("<-------------Heartbeat " + Clock.DateTime + " ---------------------->");
            AddReminder(Clock.DateTime.AddSeconds(60));

        }
		
		public void GetPositions()
		{
			mydebug.WriteLine("------Positions-------" + DateTime.Now+"--------reconcile---------");
			foreach (Instrument ins in Instruments)
			{
				Instrument myins = ins;
    
				if (Portfolio.HasPosition(myins))
				{
					string entry = myins.Symbol + "|" + Portfolio.GetPosition(myins).Amount;
					mydebug.WriteLine(entry);

				}
			}
			mydebug.WriteLine("------Positions-------" + Clock.DateTime+"--------reconcile---------");
		}

         

        protected override void OnBid(Instrument instrument, Bid bid)
        {
            trackObjects[instrument].lastestBid = instrument.Bid.Price;
            trackObjects[instrument].UpdateTime = instrument.Bid.DateTime;

        }

        protected override void OnAsk(Instrument instrument, Ask ask)
        {
            trackObjects[instrument].lastestAsk = instrument.Ask.Price;
            trackObjects[instrument].UpdateTime = instrument.Ask.DateTime;
            
            if (Clock.DateTime - CurrentTime < TimeSpan.FromMinutes(minutes_to_trade) && validator == 1)
            {
                int spreadCheck = CheckOps.checkSpread(trackObjects, spreadThold, mydebug, CurrentTime);
                if (spreadCheck==1)
                {
                    validator = 0;
                    ExecutionOps.executionLogic(subStratList, Instruments, this, Portfolio, mydebug,StrategyManager,InstrumentManager);
                }
            else if (Clock.DateTime - CurrentTime >= TimeSpan.FromMinutes(minutes_to_trade))
                {
                    mydebug.WriteLine("Wide spread Trade");
                    validator = 0;
                    ExecutionOps.executionLogic(subStratList, Instruments, this, Portfolio, mydebug, StrategyManager, InstrumentManager);

                }
            }






        }

        public void printLast()
        {
            mydebug.WriteLine("LastBars and Counts:");
            foreach (Instrument ins in dataBase.Keys)
            {

                int c = dataBase[ins].Count;
                mydebug.WriteLine("Instrument: " + ins + " Count: " + c );
                Bar mbar = dataBase[ins][c - 1];
                mydebug.WriteLine(mbar.InstrumentId + " " + mbar.OpenDateTime + " " + mbar.CloseDateTime + " " + mbar.DateTime + " " + mbar.Close);

                
            }

        }
        protected override void OnBar(Instrument instrument, Bar bar)
        {
            

			isConnected();
            printLast();
            int marketCheck = CheckOps.checkMarket(trackObjects,mydebug, Clock,StrategyManager);
            
            barCheckerDict[instrument] = bar;
            int barCallCheck = CheckOps.barDictChecker(barCheckerDict,bar.DateTime);
            bool timing = CheckOps.checkTiming(bar,mydebug);

            if (barCallCheck==1 && marketCheck==1 && timing)
            {
                int callCheck = UpdateDateTime(bar);
                if (callCheck == 1 && connected )
                {
					if (StrategyManager.Mode == StrategyMode.Live)
					{
				
						GetPositions();
				
					}
			
                    TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);
                    // data requirements
                    mydebug.WriteLine("NYTime: " + USTime.ToString() + "-------------" + "LocalTime: " + Clock.DateTime.ToString());

                    // before doing anything check if the last timestamp is aligned..
                    DateTime lastStamp = dataBase[instrument][dataBase[instrument].Count - 1].DateTime;

                    DataUpdater(barCheckerDict);

                    foreach (Instrument ins in barCheckerDict.Keys)
                    {
                        /*
                        Console.WriteLine("DB" + ins);
                        foreach (Bar mbar in dataBase[ins])
                        {
                            Console.WriteLine(mbar.InstrumentId + " " + mbar.OpenDateTime + " " + mbar.CloseDateTime + " " + mbar.DateTime + " " + mbar.Close);
                        }
                        foreach (bool mbol in originals[ins])
                        {
                            Console.WriteLine(mbol);
                        }
                        */

                    }

                        int alignmentCheck = CheckOps.checkAlignment(dataBase, originals, instrument, mydebug);
                    int marketStatusCheck = CheckOps.checkMarketStatus(dataBase, mydebug, bar, lookBacks,StrategyManager);
                    // check data integrity check if all database is aligned..
                    if (alignmentCheck == 1 && marketStatusCheck==1)
                    {
                        
                        SubStrategyOps.updateData(subStratList, dataBase, mydebug);
    
                        QuantOps.updateTargetWeights(subStratList, mydebug,Clock,trackObjects,Instruments,Portfolio,MAX_EXPOSURE);
                      
                        validator = 0;
                        validationTime = Clock.DateTime;
                        ExecutionOps.executionLogic(subStratList, Instruments, this, Portfolio, mydebug, StrategyManager, InstrumentManager);
                    }
                }
            }
           
            Log(Portfolio.Value, "Equity");
            Portfolio.Performance.Update();
                    
        }
        protected override void OnFill(Fill fill)
        {
            Log(fill, "Fills");
       //     Console.WriteLine(fill.ToString());


        }

        protected override void OnStrategyStop()
        {

            mydebug.Dispose();
        }


        //UPDATE OPERATIONS
   


        public int UpdateDateTime(Bar bar)
        {
            if (bar.DateTime != CurrentTime)
            {
                CurrentTime = bar.DateTime;
                return 1;
            }
            else { return 0; }
        }

        private void isConnected()
        {
            if (StrategyManager.Mode == StrategyMode.Live)
                if (DataProvider.IsConnected == true)
                    connected = true;
                else
                    connected = false;
        }

        public void DataUpdater(Dictionary<Instrument, Bar> inputDict)
        {
            foreach (Instrument ins in inputDict.Keys)
            {

                updateData(ins, inputDict[ins]);
            }
        }



            public void updateData(Instrument instrument, Bar bar)
        {

            int checkInposint = SubStrategyOps.checkInpos(subStratList);


            if (checkInposint == 1)
            {
                mydebug.WriteLine(" Inpos,  add the fake bar");
                

               
                DateTime closeDateTime = DataOps.adjustTime(bar.CloseDateTime);
                DateTime openDateTime = closeDateTime.AddHours(-4);

                Bar fakeBar = new Bar(openDateTime, closeDateTime, bar.InstrumentId,bar.Type,bar.Size*4,bar.Open,bar.High,bar.Low,bar.Close);
                dataBase[instrument].Add(fakeBar);
                mydebug.WriteLine("Added  Bar: " + fakeBar.OpenDateTime.ToString() + " " + fakeBar.CloseDateTime.ToString() + " " + fakeBar.Close.ToString());
                originals[instrument].Add(false);


            }
            else if (checkInposint == 0)
            {
                // if there is a bar that is not original, delete the series, add original ones to the series and download.
                int originalCount = 0;
                // mydebug.WriteLine("original before " + original.Count);
                for (int i = 0; i < originals[instrument].Count; i++)
                {
                    if (originals[instrument][i] == false)
                    {
                        //mydebug.WriteLine("False Values Detected");
                        originalCount++;
                        //original.RemoveAt(i);
                    }

                }


                if (originalCount > 0)
                {
                    mydebug.WriteLine("After inpos data recovery");
                    BarSeries newBars = new BarSeries();
                    List<bool> originaln = new List<bool>();
                    int addCount = 0;
                    foreach (Bar mbar in dataBase[instrument])
                    {
                        addCount++;
                        if (addCount < originalCount)
                        {
                            newBars.Add(mbar);
                            originaln.Add(true);
                        }


                    }
                    int requiredBars = originalCount;
                    BarSeries historicalBarsAdd = DataOps.getBarsForMe(instrument, (int)lookBacks.Max() * 2, StrategyManager, DataManager, Clock, illogicThold, mydebug);

                    foreach (Bar mbar in historicalBarsAdd)
                    {
                        DateTime bartime = mbar.DateTime;
                        if (newBars.Contains(bartime) == false)
                        {
                            newBars.Add(mbar);
                            //mydebug.WriteLine("Added  Bar: " + mbar.OpenDateTime.ToString() + " " + mbar.CloseDateTime.ToString() + " " + mbar.Close.ToString());
                            originaln.Add(true);
                        }


                    }

                    dataBase[instrument] = newBars;
                    originals[instrument] = originaln;


                }
                else
                {
                    mydebug.WriteLine(" Not Inpos, Short data download");
                    BarSeries historicalBarsAdd = DataOps.getBarsForMe(instrument, shortDATA, StrategyManager, DataManager, Clock, illogicThold, mydebug);

                    foreach (Bar mbar in historicalBarsAdd)
                    {
                        DateTime bartime = mbar.DateTime;
                        if (dataBase[instrument].Contains(bartime) == false)
                        {
                            dataBase[instrument].Add(mbar);
                            mydebug.WriteLine("Added  Bar: " + mbar.OpenDateTime.ToString() + " " + mbar.CloseDateTime.ToString() + " " + mbar.Close.ToString());
                            originals[instrument].Add(true);
                        }


                    }


                }

            }

        }


    }


}


























