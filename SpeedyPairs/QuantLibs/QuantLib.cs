using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using SubStratLib;
using CheckLib;

namespace  QuantLib
{
    public  class ExecutionObject
    {

        public SubStrategy mySub{ get; set; }
        public int type { get; set; }
        public string entext { get; set; }
    }

	public static class QuantOps
	{
		public static List<double> reverse(List<double> reverseList)
		{
			List<double> returned = new List<double>();
			foreach (double myval in reverseList)
			{
				returned.Add(1 / myval);
			}
			return returned;
		}

		public static void calculatePnl(SubStrategy subStrat, Dictionary<Instrument, TrackingObject> trackObjects,Debugger mydebug)
		{
			double pnl = 0;
			if (subStrat.inpos==1)
			{
				double pl1 =( (trackObjects[subStrat.Ins1].lastestBid - subStrat.Ins1Entry) * subStrat.Ins1Target)/subStrat.clipSize;
				double pl2 =( (trackObjects[subStrat.Ins2].lastestAsk - subStrat.Ins2Entry) * subStrat.Ins2Target )/ subStrat.clipSize;
				double pl3 =( (trackObjects[subStrat.Ins3].lastestAsk - subStrat.Ins3Entry) * subStrat.Ins3Target )/ subStrat.clipSize;
				pnl = pl1 + pl2 + pl3;    
			}
			else if (subStrat.inpos == -1)
			{
				double pl1 =( (trackObjects[subStrat.Ins1].lastestAsk - subStrat.Ins1Entry) * subStrat.Ins1Target )/ subStrat.clipSize;
				double pl2 =( (trackObjects[subStrat.Ins2].lastestBid - subStrat.Ins2Entry) * subStrat.Ins2Target )/ subStrat.clipSize;
				double pl3 =( (trackObjects[subStrat.Ins3].lastestBid - subStrat.Ins3Entry) * subStrat.Ins3Target) / subStrat.clipSize;
				pnl = pl1 + pl2 + pl3;
			}

			subStrat.curPnl = pnl;
		}
		public static void takeProfit(SubStrategy subStrat,Debugger mydebug)
		{
			if (subStrat.curPnl>(subStrat.profitTarget/10000) && subStrat.inpos!=0)
			{
				subStrat.takeProfit = true;
			}

		}
		public static void decrementTarget(SubStrategy subStrat, Debugger mydebug)
		{
			if ( subStrat.inpos != 0)
			{
				subStrat.profitTarget = subStrat.profitTarget-0.1;
			}

		}
		public static void updateGear(SubStrategy subStrat, Debugger mydebug, double signal)
		{
			if (subStrat.gear == false)
			{
				if (subStrat.gearType==1)
				{
					if (signal>0)
					{
						subStrat.gear = true;
					}
				}
				else if (subStrat.gearType == -1)
				{
					if (signal < 0)
					{
						subStrat.gear = true;
					}
				}
			}

		}
		public static Dictionary<Instrument,double> GetPositionsDict(InstrumentList Instruments , Portfolio Portfolio )
		{
			Dictionary<Instrument, double> posDict = new Dictionary<Instrument, double>();
			foreach (Instrument ins in Instruments)
			{
				posDict[ins] = 0;
			}

			foreach (Instrument ins in Instruments)
			{
				Instrument myins = ins;

				if (Portfolio.HasPosition(myins))
				{
					posDict[myins] = Portfolio.GetPosition(myins).Amount;


				}
			}
			return posDict;
		}

        public static Dictionary<Instrument, double> getHypPositions(InstrumentList Instruments, List<SubStrategy> SubStratList)
        {
            Dictionary<Instrument, double> posDict = new Dictionary<Instrument, double>();
            foreach (Instrument ins in Instruments)
            {
                posDict[ins] = 0;
            }

            foreach (SubStrategy mySub in SubStratList)
            {
                posDict[mySub.Ins1] = posDict[mySub.Ins1] + mySub.Ins1Target;
                posDict[mySub.Ins2] = posDict[mySub.Ins2] + mySub.Ins2Target;
                posDict[mySub.Ins3] = posDict[mySub.Ins3] + mySub.Ins3Target;

            }

            return posDict;
        }


        public static double getExposureSum(Dictionary<Instrument, double> posDict,InstrumentList Instruments)
		{
			double sum = 0;
			foreach (Instrument ins in Instruments)
			{

				sum=Math.Abs(posDict[ins]) + sum;
			}



			return sum;
		}

        public static void exit(SubStrategy mySub, int type)
        {
            mySub.inpos = 0;
            mySub.Ins1Target = 0;
            mySub.Ins2Target = 0;
            mySub.Ins3Target = 0;

            mySub.gear = false;
            mySub.gearType = type;
            mySub.takeProfit = false;
            
        }

        public static void enter(SubStrategy mySub, int type, Dictionary<Instrument, TrackingObject> trackObjects)
        {
            mySub.inpos = type;
            mySub.Ins1Target = mySub.Ins1Dir * mySub.inpos * mySub.clipSize;
            mySub.Ins2Target = mySub.Ins2Dir * mySub.inpos * mySub.clipSize * 0.5;
            mySub.Ins3Target = mySub.Ins3Dir * mySub.inpos * mySub.clipSize * 0.5;
            mySub.profitTarget = mySub.profitTargetFix;
            // entry prices
            mySub.Ins1Entry = trackObjects[mySub.Ins1].lastestBid;
            mySub.Ins2Entry = trackObjects[mySub.Ins2].lastestAsk;
            mySub.Ins3Entry = trackObjects[mySub.Ins3].lastestAsk;
        }

        public static void updateTargetWeights(List<SubStrategy> mysubStratList,Debugger mydebug,Clock Clock, Dictionary<Instrument, TrackingObject> trackObjects, InstrumentList Instruments,Portfolio Portfolio ,double MAX_EXPOSURE)
		{

            Dictionary<Instrument, double> posDict  =GetPositionsDict(Instruments, Portfolio);
            List<ExecutionObject> execList = new List<ExecutionObject>();
            
           // Console.WriteLine("DAY"+Clock.DateTime);
            double current_exposure = getExposureSum(posDict, Instruments);
          //  Console.WriteLine("START EXPOSURE: " + current_exposure);
            
            // get total positions into a dict:
            // for each decision check if we have available positions

            foreach (SubStrategy mySub in mysubStratList)
			{
                

				int lookBack = mySub.lookBack;
				int inpos = mySub.inpos;
				double thresHold = mySub.thold;

				List<Bar> datap1 = mySub.Ins1Data;
				List<Bar> datap2 = mySub.Ins2Data;
				List<Bar> datap3 = mySub.Ins3Data;

				List<double> p1 = new List<double>();
				List<double> p2 = new List<double>();
				List<double> p3 = new List<double>();

				List<double> pa1 = new List<double>();
				List<double> pa2 = new List<double>();
				List<double> pa3 = new List<double>();

				datap1.Skip(Math.Max(0, datap1.Count() - lookBack)).ToList().ForEach(i => p1.Add(i.Close));
				datap2.Skip(Math.Max(0, datap2.Count() - lookBack)).ToList().ForEach(i => p2.Add(i.Close));
				datap3.Skip(Math.Max(0, datap3.Count() - lookBack)).ToList().ForEach(i => p3.Add(i.Close));


				datap1.Skip(Math.Max(0, datap1.Count() - lookBack - 1)).Take(lookBack).ToList().ForEach(i => pa1.Add(i.Close));
				datap2.Skip(Math.Max(0, datap2.Count() - lookBack-1)).Take(lookBack).ToList().ForEach(i => pa2.Add(i.Close));
				datap3.Skip(Math.Max(0, datap3.Count() - lookBack-1)).Take(lookBack).ToList().ForEach(i => pa3.Add(i.Close));


				double mom1 = p1.Average() - pa1.Average();
				double mom2 = p2.Average() - pa2.Average();
				double mom3 = p3.Average() - pa3.Average();

				double momReverse = (mom2 ) - (mom3 );
				double signal = mom1 - mom2*0.5 - mom3*0.5;

				//update pnl
				calculatePnl(mySub, trackObjects, mydebug);

				//check take profit
				takeProfit(mySub, mydebug);
				//decrement target
				decrementTarget(mySub, mydebug);
				//update gear
				updateGear(mySub, mydebug, signal);

                if (inpos!=0)
                {

                    mydebug.WriteLine("------------- Inpos Strategy Report ----------");
                    mydebug.WriteLine("ID: "+mySub.strategyId);
                    mydebug.WriteLine("Inpos: " + mySub.inpos);
                    mydebug.WriteLine("Signal: " + signal);
                    mydebug.WriteLine("Pnl: " + mySub.curPnl*10000);
                    mydebug.WriteLine("TakeProfit: " + mySub.profitTarget);
                    mydebug.WriteLine("TakeProfit Condition: " + mySub.takeProfit);
					mydebug.WriteLine("---------------------------------------------");
                }


                if (inpos==0 && signal> thresHold && mom1>0 && momReverse<0 && mySub.gear==true)
				{

					// check if we exceed limits if not go ahead:


					int type = -1;
                    ExecutionObject execObj = new ExecutionObject();
                    execObj.mySub = mySub;
                    execObj.type = type;
                    execObj.entext = "ENTRY";
                    execList.Add(execObj);
                   // enter(mySub, type, trackObjects);




                }
				else if (inpos == 0 && signal < (-1*thresHold) && mom1 < 0 && momReverse > 0 && mySub.gear == true)
				{

                    int type = 1;
                    ExecutionObject execObj = new ExecutionObject();
                    execObj.mySub = mySub;
                    execObj.type = type;
                    execObj.entext = "ENTRY";
                    execList.Add(execObj);
                   // enter(mySub, type, trackObjects);



                }
				else if (inpos == 1 && (mySub.takeProfit==true ))
				{
                    int type = 1;
                    ExecutionObject execObj = new ExecutionObject();
                    execObj.mySub = mySub;
                    execObj.type = type;
                    execObj.entext = "EXIT";
                    execList.Add(execObj);
                    //exit(mySub, type);


				}
				else if (inpos == -1 && (mySub.takeProfit == true ))
				{
                    int type = -1;
                    ExecutionObject execObj = new ExecutionObject();
                    execObj.mySub = mySub;
                    execObj.type = type;
                    execObj.entext = "EXIT";
                    execList.Add(execObj);
                   // exit(mySub, type);


                }

			}
            processExecList(execList, trackObjects, posDict, Instruments, MAX_EXPOSURE, mysubStratList);

// process execList, first exit all , sum all exited positions get new exposires




        }

        static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }


        public static void processExecList(List<ExecutionObject> execList, Dictionary<Instrument, TrackingObject> trackObjects, Dictionary<Instrument, double> posDict, InstrumentList Instruments,double TOTAL_EXPOSURE,List<SubStrategy> subStratList)
        {





            Dictionary<Instrument, double> hypPosDict = getHypPositions(Instruments, subStratList);
            double hyp_exposure = getExposureSum(hypPosDict,Instruments);
           // Console.WriteLine("Hyp Exposure: " + hyp_exposure);
            Dictionary<Instrument, double>  exitPositions = new Dictionary<Instrument, double>();
            foreach (Instrument ins in Instruments)
            {
                exitPositions[ins] = 0;
            }


            // first exit all
            foreach (ExecutionObject myExec in execList)
            {
                if (myExec.entext=="EXIT")
                {

                    exit(myExec.mySub, myExec.type);


                }
                    
            }

            //positions after exit
            Dictionary<Instrument, double> hypPosDict2 = getHypPositions(Instruments, subStratList);
            double hyp_exposure2 = getExposureSum(hypPosDict2, Instruments);

            double free_exposure = TOTAL_EXPOSURE - hyp_exposure2;
            // randomly generate a list of selection and see which one maximize exposure without exceeding the limit.

            List<ExecutionObject> entryExecs = new List<ExecutionObject>();

            foreach (ExecutionObject myEx in execList)
            {
                if (myEx.entext=="ENTRY")
                {
                    entryExecs.Add(myEx);
 
                }
            }

            if (entryExecs.Count>0)
            {

                List<ExecutionObject> entriesToExecute = new List<ExecutionObject>();
                //IEnumerable<IEnumerable<int>> mpermutations = GetPermutations(Enumerable.Range(1, entryExecs.Count), entryExecs.Count);

                //approved entries and max exposure list

               

                List<ExecutionObject> approvedEntries = new List<ExecutionObject>();
                //generate an empty pos entry dict
                Dictionary<Instrument, double> posEntryDict = new Dictionary<Instrument, double>();
                foreach (Instrument ins in Instruments)
                {
                    posEntryDict[ins] = 0;
                }

                foreach (ExecutionObject myEntry in entryExecs)
                {


                    // append stuff to posentry dict
                        
                    posEntryDict[myEntry.mySub.Ins1] = posEntryDict[myEntry.mySub.Ins1] + myEntry.mySub.clipSize  *  (double) myEntry.mySub.Ins1Dir * (double) myEntry.type;
                    posEntryDict[myEntry.mySub.Ins2] = posEntryDict[myEntry.mySub.Ins2] + myEntry.mySub.clipSize  * (double) myEntry.mySub.Ins2Dir * 0.5 * (double) myEntry.type;
                    posEntryDict[myEntry.mySub.Ins3] = posEntryDict[myEntry.mySub.Ins3] + myEntry.mySub.clipSize  * (double) myEntry.mySub.Ins3Dir * 0.5 * (double) myEntry.type;

                    double sum_exposure = 0;

                    foreach (Instrument ins in Instruments)
                    {
                        sum_exposure = sum_exposure + Math.Abs(posEntryDict[ins] + hypPosDict2[ins]);
                           
                    }

                    if (sum_exposure>TOTAL_EXPOSURE)
                    {
                        break;
                    }
                    else
                    {
                        // continue 
                        approvedEntries.Add(myEntry);

                            

                    }


                }


                if (approvedEntries.Count>0)
                {
                    foreach (ExecutionObject myExec in approvedEntries)
                    {
                        if (myExec.entext == "ENTRY")
                        {
                            //proposed entry exposure

                            //Console.WriteLine("entry");
                            enter(myExec.mySub, myExec.type, trackObjects);


                        }

                    }
                }

                // execute final decisions


                // now try all paths and find the optimal solution.

            }

            







        }

        

	}













}


