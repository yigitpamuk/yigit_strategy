using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using SubStratLib;
using CheckLib;
namespace  QuantLib
{


    public static class QuantOps
    {
        public static List<double> reverse(List<double> reverseList)
        {
            List<double> returned = new List<double>();
            foreach (double myval in reverseList)
            {
                returned.Add(1 / myval);
            }
            return returned;
        }

        public static void calculatePnl(SubStrategy subStrat, Dictionary<Instrument, TrackingObject> trackObjects,Debugger mydebug)
        {
            double pnl = 0;
            if (subStrat.inpos==1)
            {
                double pl1 =( (trackObjects[subStrat.Ins1].lastestBid - subStrat.Ins1Entry) * subStrat.Ins1Target)/subStrat.clipSize;
                double pl2 =( (trackObjects[subStrat.Ins2].lastestAsk - subStrat.Ins2Entry) * subStrat.Ins2Target )/ subStrat.clipSize;
                double pl3 =( (trackObjects[subStrat.Ins3].lastestAsk - subStrat.Ins3Entry) * subStrat.Ins3Target )/ subStrat.clipSize;
                pnl = pl1 + pl2 + pl3;    
            }
            else if (subStrat.inpos == -1)
            {
                double pl1 =( (trackObjects[subStrat.Ins1].lastestAsk - subStrat.Ins1Entry) * subStrat.Ins1Target )/ subStrat.clipSize;
                double pl2 =( (trackObjects[subStrat.Ins2].lastestBid - subStrat.Ins2Entry) * subStrat.Ins2Target )/ subStrat.clipSize;
                double pl3 =( (trackObjects[subStrat.Ins3].lastestBid - subStrat.Ins3Entry) * subStrat.Ins3Target) / subStrat.clipSize;
                pnl = pl1 + pl2 + pl3;
            }

            subStrat.curPnl = pnl;
        }
        public static void takeProfit(SubStrategy subStrat,Debugger mydebug)
        {
            if (subStrat.curPnl>(subStrat.profitTarget/10000) && subStrat.inpos!=0)
            {
                subStrat.takeProfit = true;
            }

        }
        public static void decrementTarget(SubStrategy subStrat, Debugger mydebug)
        {
            if ( subStrat.inpos != 0)
            {
                subStrat.profitTarget = subStrat.profitTarget-1;
            }

        }
        public static void updateGear(SubStrategy subStrat, Debugger mydebug, double signal)
        {
            if (subStrat.gear == false)
            {
                if (subStrat.gearType==1)
                {
                    if (signal>0)
                    {
                        subStrat.gear = true;
                    }
                }
                else if (subStrat.gearType == -1)
                {
                    if (signal < 0)
                    {
                        subStrat.gear = true;
                    }
                }
            }

        }
        public static Dictionary<Instrument,double> GetPositionsDict(InstrumentList Instruments , Portfolio Portfolio )
        {
            Dictionary<Instrument, double> posDict = new Dictionary<Instrument, double>();
            foreach (Instrument ins in Instruments)
            {
                posDict[ins] = 0;
            }

                foreach (Instrument ins in Instruments)
            {
                Instrument myins = ins;

                if (Portfolio.HasPosition(myins))
                {
                    posDict[myins] = posDict[myins]+ Math.Abs(Portfolio.GetPosition(myins).Amount);


                }
            }
            return posDict;
        }

        public static double getExposureSum(Dictionary<Instrument, double> posDict,InstrumentList Instruments)
        {
            double sum = 0;
            foreach (Instrument ins in Instruments)
            {

                sum=Math.Abs(posDict[ins]) + sum;
            }



            return sum;
        }
        public static void updateTargetWeights(List<SubStrategy> mysubStratList,Debugger mydebug,Clock Clock, Dictionary<Instrument, TrackingObject> trackObjects, InstrumentList Instruments,Portfolio Portfolio ,double MAX_EXPOSURE)
        {

            Dictionary<Instrument, double> posDict = GetPositionsDict(Instruments, Portfolio);
            /*
            Console.WriteLine("DAY"+Clock.DateTime);
            double amcurrent_exposure = getExposureSum(posDict, Instruments);
            Console.WriteLine("STARTEXPOSIRE: " + amcurrent_exposure);
            */
            // get total positions into a dict:
            // for each decision check if we have available positions

            foreach (SubStrategy mySub in mysubStratList)
            {


                int lookBack = mySub.lookBack;
                int inpos = mySub.inpos;
                double thresHold = mySub.thold;

                List<Bar> datap1 = mySub.Ins1Data;
                List<Bar> datap2 = mySub.Ins2Data;
                List<Bar> datap3 = mySub.Ins3Data;

                List<double> p1 = new List<double>();
                List<double> p2 = new List<double>();
                List<double> p3 = new List<double>();

                List<double> pa1 = new List<double>();
                List<double> pa2 = new List<double>();
                List<double> pa3 = new List<double>();

                datap1.Skip(Math.Max(0, datap1.Count() - lookBack)).ToList().ForEach(i => p1.Add(i.Close));
                datap2.Skip(Math.Max(0, datap2.Count() - lookBack)).ToList().ForEach(i => p2.Add(i.Close));
                datap3.Skip(Math.Max(0, datap3.Count() - lookBack)).ToList().ForEach(i => p3.Add(i.Close));


                datap1.Skip(Math.Max(0, datap1.Count() - lookBack - 1)).Take(lookBack).ToList().ForEach(i => pa1.Add(i.Close));
                datap2.Skip(Math.Max(0, datap2.Count() - lookBack-1)).Take(lookBack).ToList().ForEach(i => pa2.Add(i.Close));
                datap3.Skip(Math.Max(0, datap3.Count() - lookBack-1)).Take(lookBack).ToList().ForEach(i => pa3.Add(i.Close));


                double mom1 = p1.Average() - pa1.Average();
                double mom2 = p2.Average() - pa2.Average();
                double mom3 = p3.Average() - pa3.Average();

                double momReverse = (mom2 ) - (mom3 );
                double signal = mom1 - mom2*0.5 - mom3*0.5;

                //update pnl
                calculatePnl(mySub, trackObjects, mydebug);

                //check take profit
                takeProfit(mySub, mydebug);
                //decrement target
                decrementTarget(mySub, mydebug);
                //update gear
                updateGear(mySub, mydebug, signal);

                double exposure_to_add = 0;
                double current_exposure = 0;

                if (inpos==0 && signal> thresHold && mom1>0 && momReverse<0 && mySub.gear==true)
                {

                    // check if we exceed limits if not go ahead:
                    exposure_to_add = mySub.clipSize * 2;
                    current_exposure = getExposureSum(posDict, Instruments);
                    
                    if (current_exposure+ exposure_to_add>MAX_EXPOSURE*10000000000)
                    {
                        Console.WriteLine("No Add");
                    }
                    else
                    {

                        mySub.inpos = -1;
                        mySub.Ins1Target = mySub.Ins1Dir * mySub.inpos * mySub.clipSize;
                        mySub.Ins2Target = mySub.Ins2Dir * mySub.inpos * mySub.clipSize *0.5;
                        mySub.Ins3Target = mySub.Ins3Dir * mySub.inpos * mySub.clipSize * 0.5;
                        mySub.profitTarget = mySub.profitTargetFix;
                        // entry prices
                        mySub.Ins1Entry = trackObjects[mySub.Ins1].lastestBid;
                        mySub.Ins2Entry= trackObjects[mySub.Ins2].lastestAsk;
                        mySub.Ins3Entry= trackObjects[mySub.Ins3].lastestAsk;

                        //add to posDict
                        posDict[mySub.Ins1] = posDict[mySub.Ins1] + Math.Abs(mySub.Ins1Target);
                        posDict[mySub.Ins2] = posDict[mySub.Ins2] + Math.Abs(mySub.Ins2Target);
                        posDict[mySub.Ins3] = posDict[mySub.Ins3] + Math.Abs(mySub.Ins3Target);
                    }
                }
                else if (inpos == 0 && signal < (-1*thresHold) && mom1 < 0 && momReverse > 0 && mySub.gear == true)
                {
                    exposure_to_add = mySub.clipSize * 2;
                    current_exposure = getExposureSum(posDict, Instruments);
                    
                    if (current_exposure + exposure_to_add > MAX_EXPOSURE*10000000000)
                    {
                        Console.WriteLine("No Add");
                    }
                    else
                    {
                        mySub.inpos = 1;
                        mySub.Ins1Target = mySub.Ins1Dir * mySub.inpos * mySub.clipSize;
                        mySub.Ins2Target = mySub.Ins2Dir * mySub.inpos * mySub.clipSize * 0.5;
                        mySub.Ins3Target = mySub.Ins3Dir * mySub.inpos * mySub.clipSize * 0.5;
                        mySub.profitTarget = mySub.profitTargetFix;
                        // entry prices
                        mySub.Ins1Entry = trackObjects[mySub.Ins1].lastestAsk;
                        mySub.Ins2Entry = trackObjects[mySub.Ins2].lastestBid;
                        mySub.Ins3Entry = trackObjects[mySub.Ins3].lastestBid;
                        posDict[mySub.Ins1] = posDict[mySub.Ins1] + Math.Abs(mySub.Ins1Target);
                        posDict[mySub.Ins2] = posDict[mySub.Ins2] + Math.Abs(mySub.Ins2Target);
                        posDict[mySub.Ins3] = posDict[mySub.Ins3] + Math.Abs(mySub.Ins3Target);
                    }



                }
                else if (inpos == 1 && mySub.takeProfit==true)
                {
      
                    mySub.inpos = 0;
                    mySub.Ins1Target = 0;
                    mySub.Ins2Target =0;
                    mySub.Ins3Target =0;

                    mySub.gear = false;
                    mySub.gearType = 1;
                    mySub.takeProfit = false;
                    posDict[mySub.Ins1] = posDict[mySub.Ins1] - Math.Abs(mySub.Ins1Target);
                    posDict[mySub.Ins2] = posDict[mySub.Ins2] - Math.Abs(mySub.Ins2Target);
                    posDict[mySub.Ins3] = posDict[mySub.Ins3] - Math.Abs(mySub.Ins3Target);

                }
                else if (inpos == -1 && mySub.takeProfit == true)
                {
                    mySub.inpos = 0;
                    mySub.Ins1Target = 0;
                    mySub.Ins2Target = 0;
                    mySub.Ins3Target = 0;

                    mySub.gear = false;
                    mySub.gearType = -1;
                    mySub.takeProfit = false;
                    posDict[mySub.Ins1] = posDict[mySub.Ins1] - Math.Abs(mySub.Ins1Target);
                    posDict[mySub.Ins2] = posDict[mySub.Ins2] - Math.Abs(mySub.Ins2Target);
                    posDict[mySub.Ins3] = posDict[mySub.Ins3] - Math.Abs(mySub.Ins3Target);

                }

            }
            double mcurrent_exposure = getExposureSum(posDict, Instruments);
            //Console.WriteLine("SUMEXPOSURE: " + mcurrent_exposure);


        }


    }





}







