using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartQuant;
using Genesis;



namespace CheckLib
{
    public class TrackingObject
    {
        public double lastestAsk { get; set; }
        public double lastestBid { get; set; }
        public DateTime UpdateTime { get; set; }
        public Instrument instrument { get; set; }
        public double spread { get; set; }
    }

     
    public static class CheckOps
    {

        public static void calculateSpread(TrackingObject myTracker)
        {
            myTracker.spread = myTracker.lastestAsk - myTracker.lastestBid;
        }

        public static int checkSpread(Dictionary<Instrument, TrackingObject> trackDict, double spreadThold, Debugger mydebug, DateTime barTime)
        {
            int checkint = 0;
            foreach (Instrument ins in trackDict.Keys)
            {
       
                calculateSpread(trackDict[ins]);
                if (trackDict[ins].spread < spreadThold && trackDict[ins].UpdateTime> barTime)
                {
                    
                    checkint++;
                }
            }
            if (checkint== 3)
            {
                mydebug.WriteLine("Spread is ok");
                return 1;
            }
            else
            {
          
               
                mydebug.WriteLine("Spread is too  wide to trade");
                return 0;
            }
        }


        public static bool checkTiming(Bar bar,Debugger mydebug)
        {

            TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime USTime = TimeZoneInfo.ConvertTime(bar.DateTime.ToUniversalTime(), convertZone);

            if (USTime.Hour % 4 == 0)
            {
                return true;

            }
            else
            {
                mydebug.WriteLine("Not 4 hourly bar.");
                return false;
            }
        }
        public static Dictionary<Instrument, TrackingObject> initTrackingObjects(InstrumentList Instruments, Clock Clock)
        {
            Dictionary<Instrument, TrackingObject> responseDict = new Dictionary<Instrument, TrackingObject>();
            foreach (Instrument ins in Instruments)
            {
                responseDict[ins] = new TrackingObject();
                responseDict[ins].instrument = ins;
                responseDict[ins].lastestAsk = 0;
                responseDict[ins].lastestBid = 0;
                responseDict[ins].UpdateTime = Clock.DateTime;
            }
            return responseDict;
        }

        public static int checkMarket(Dictionary<Instrument, TrackingObject> trackDict, Debugger mydebug, Clock Clock, StrategyManager StrategyManager)
        {
            int checkInt = 0;
            foreach (Instrument ins in trackDict.Keys)
            {
                TrackingObject myObj = trackDict[ins];
                int checks = checkMarketTimeandPrice(myObj.instrument, myObj.lastestBid, myObj.lastestAsk, myObj.UpdateTime, Clock, mydebug, StrategyManager);
                checkInt = checkInt + checks;
            }
            if (checkInt == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        public static int checkMarketTimeandPrice(Instrument instrument, double latest_bid, double latest_ask, DateTime currentTime, Clock Clock, Debugger mydebug,StrategyManager StrategyManager)

        {
            if (StrategyManager.Mode == StrategyMode.Backtest)
            {
                return 0;
            }
            else
            {

                // this function will be called by OnBid, OnAsk and OnBar 

                //10-Year U.S. Treasury Note Futures	PreOpenSunday 16:00	Sunday 17:00-16:00	PreOpenWeekDay 16:45	Weekday 17:00-16:00
                int checker = 0;

                if (latest_bid <= 0 || latest_ask <= 0)
                {
                    checker++;
                    mydebug.WriteLine("Bid and Ask data is not healty");
                }

                TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);
                DateTime nowTime = Clock.DateTime;
                TimeSpan span = nowTime.Subtract(currentTime);
                // if the time passes this Date we should not enter any positions and consider starting out new algo

                if (span.Minutes > 5)
                {
                    mydebug.WriteLine("No Updates for 5 Minutes DateTimeNow:"+nowTime+" Last Update:"+currentTime);
                    checker++;
                }
                if ((USTime.DayOfWeek == DayOfWeek.Saturday))
                {
                    mydebug.WriteLine("Market Is Closed DoW Condition" + USTime);
                    checker++;
                }

                if ((USTime.DayOfWeek == DayOfWeek.Friday) && (USTime.Hour >= 17))
                {
                    mydebug.WriteLine("Market Is Closed Friday Condition" + USTime);
                    checker++;
                }

                if ((USTime.DayOfWeek == DayOfWeek.Sunday) && (USTime.Hour < 17 && USTime.Minute < 16))
                {
                    mydebug.WriteLine("Market Is Closed Sunday Condition" + USTime);
                    checker++;
                }

                if (USTime.Hour == 17)
                    if (USTime.Minute < 16)
                    {
                        mydebug.WriteLine("Market Is Closed Weekday Close Condition" + USTime);
                        checker++;
                    }


                if (checker > 0)
                {
                    return 1;
                }

                else
                {
                    return 0;
                }
            }

        }

        public static int alignedDictChecker(Dictionary<Instrument, List<Bar>> inputDict, Debugger mydebug, List<int> lookBacks)
        {
            int count = inputDict[inputDict.Keys.ToList()[0]].Count();
            mydebug.WriteLine("count:" + count);
            mydebug.WriteLine("MAXLOOKBACK:" + lookBacks.Max());
            foreach (Instrument ins in inputDict.Keys)
            {
                if (inputDict[ins].Count() != count)
                {
                    mydebug.WriteLine("count:" + inputDict[ins].Count() + " " + ins);

                    mydebug.WriteLine("List is not correctly aligned");
                    return 0;
                }

                else if (inputDict[ins].Count() < lookBacks.Max())
                {
                    mydebug.WriteLine("Not Enough Data");
                    return 0;
                }

            }
            return 1;
        }
        public static int barDictChecker(Dictionary<Instrument,Bar> barDict, DateTime barTime)
        {
            int countBad = 0;
            foreach (Instrument ins in barDict.Keys)
            {
                if (barDict[ins].DateTime!=barTime)
                {
                    countBad++;
                }
            }
            if (countBad>0)
            {
                return 0;
            }
            else
            {
                return 1;
            }

        }
        public static int checkAlignment(Dictionary<Instrument, BarSeries> dataBase, Dictionary<Instrument, List<bool>> boolCheck,Instrument instrument, Debugger mydebug)
        {
            int checklen = dataBase[instrument].Count;
            int result = 0;
            List<List<DateTime>> dlists = new List<List<DateTime>>();
            foreach (Instrument ins in dataBase.Keys)
            {
                if (checklen!=dataBase[ins].Count || checklen != boolCheck[ins].Count)
                {
                    mydebug.WriteLine("Database count is not properly aligned!");
                    mydebug.WriteLine("checkLen:"+checklen);
                    mydebug.WriteLine(ins +" dbcount:" + dataBase[ins].Count);
                    mydebug.WriteLine(ins +" dbbool:" + boolCheck[ins].Count);

                    result++;
                }
                List<DateTime> mydates = new List<DateTime>();
                for (int i = 0; i < dataBase[ins].Count; i++)
                {

                    mydates.Add(dataBase[ins][i].DateTime);

                }

                dlists.Add(mydates);

            }
            for (int i = 0; i < dlists[0].Count; i++)
            {
                List<DateTime> DistinctList= new List<DateTime>();
                for (int j = 0; j < dlists.Count; j++)
                {
                    DistinctList.Add(dlists[j][i]);

                }

                if (DistinctList.Distinct().Count() != 1 )
                {
                    mydebug.WriteLine("Database alignment is not right!");
                    result++;

                }
            }
            if (result==0)
            {
                return 1;
            }
            else
            {
                return 0;
            }


            }





        

        public static int checkMarketStatus(Dictionary<Instrument, BarSeries> dataBase, Debugger mydebug,Bar bar,List<int> lookBacks,StrategyManager StrategyManager)
		{

            TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime USTime = TimeZoneInfo.ConvertTime(bar.DateTime.ToUniversalTime(), convertZone);
            //check if last data point equals to bar
            //check if database size is enough
            int checker = 0;
            int minlen = 1000000000;
            foreach(Instrument ins in dataBase.Keys)
            {
                if (dataBase[ins].Count < minlen)
                {
                    minlen = dataBase[ins].Count;
                }
                DateTime lasttime = dataBase[ins][dataBase[ins].Count - 1].DateTime;
                if (lasttime != USTime)
                {
                    if (StrategyManager.Mode == StrategyMode.Live)
                    {
                        checker++;
                        mydebug.WriteLine("Last bar is not current! LastTime: " + lasttime + " BarTimeUS:  " + USTime);
                    }

                }
                    

            }

            if (minlen< (int) lookBacks.Max() * 1.5)
            {
                mydebug.WriteLine("Not Enough Data");
                return 0;
            }
            else if (checker>0)
            {
                return 0;
            }
            else
            {
                return 1;
            }

        }

	}
		
        public class Debugger

        {
            public string logLocation { get; set; }
            public bool deBug { get; set; }
            public Logger logger { get; set; }
            public Debugger(string location, bool debug)
            {

                logLocation = location;
                Logger nlogger = new Logger(location, debug);
                logger = nlogger;
                deBug = debug;
                logger.Print("---------------Logger Initiated-------------");
            }
            public void WriteLine(string str)
            {
                if (deBug == true)
                {
                    logger.Print(str);
                }
            }
            public void Dispose()
            {
                if (deBug == true)
                {
                    logger.Print("---------------Logger Disposed-------------");
                    logger.Dispose();
                }
            }

        }

    
}


