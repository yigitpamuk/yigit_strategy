using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcLib;
using UtilLib;
using QuantLib;
using MathNet.Numerics;
using SubStratLib;
using SmartQuant;

namespace ExecutionLib

{
    public static class executionOperations 
    {
    	public static void aFunc()
    	{
    		Console.WriteLine("sdad");
    	}
    }
}
    	/*

		public static void tradingLogic(Instrument instrument, Portfolio Portfolio, Dictionary<Instrument,double> askDict, 
			Dictionary<Instrument,double> bidDict,Dictionary<Instrument,int> marketAskDict,Dictionary<Instrument,int>  marketBidDict,
			List<int> LookBacks ,DateTime earliestFirstNotice , StrategyManager StrategyManager , 
			List<Instrument> instrumentCollection, int stateCheck, int tradingPeriod,List<SubStrategy> subStratList,double lotExposure)
		{
			//int timeCheck = CheckOperations.bidAskCheck(timeAskDict,timeBidDict,CurrentTimeBar);
			
			int marketCheckInt = CheckOperations.processMarketCheck(instrument,askDict,bidDict,marketAskDict,marketBidDict,LookBacks,
				earliestFirstNotice,StrategyManager,instrumentCollection);

			// check if the state is updated , check if we are under 10 minutes , check if the bid and ask is updated after onbar, 
			if ((stateCheck==1) && (DateTime.Now.Minute<tradingPeriod) && marketCheckInt==1)
			{
				// calculate current resid for each bid and ask change for substrategy list
				RegressionCalculator.updateResids(subStratList,midDict,tickConversion,true);
				// generate signal for each substrategy
				RegressionCalculator.calculateWeights(subStratList);
				
				// calculate total target lots long short
				Dictionary<Instrument,double> target = generateTargetLots(subStratList,lotExposure,instrumentCollection);
				executionEngine(target);
				printOperations.positionPrinter(Portfolio,target);

			}
    	}

    	public static Dictionary<Instrument,double> generateTargetLots (List<SubStrategy> subStratList, double lotsExposure, List<Instrument> instrumentCollection  )
        {
        	Dictionary<Instrument,double> responseDict = new Dictionary<Instrument,double> ();
        	foreach (Instrument ins in instrumentCollection)
        	{
        		responseDict[ins]=0;
        	}

        	foreach (SubStrategy SubStrategy in subStratList)
        	{
        		Dictionary<Instrument,double> mytargetDict = SubStrategy.targetDict;
        		foreach (Instrument ins in mytargetDict.Keys)
        		{
        			double target = mytargetDict[ins];
        			responseDict[ins]+=target;
        		}
        	}

        	foreach (Instrument ins in instrumentCollection)
        	{
        		double unMultipled = responseDict[ins];
        		
        		double roundedval =Math.Round(unMultipled*lotsExposure); 
        		responseDict[ins]= roundedval;
        		//Console.WriteLine("Instrument: " + ins + " Target Lot: " + roundedval );
        	}
        	return responseDict;

        }


		public static double positionChecker(Portfolio Portfolio, Instrument ins)
		{
			double response = 0 ;
			foreach(Position pos in Portfolio.Positions)
			{
				if (pos.Instrument==ins)
				{
					response = pos.Amount;
				
				}
			}	
			return response;
		}

		public static int spreadChecker(Dictionary<Instrument,double> bidDict,Dictionary<Instrument,double> askDict,
			Dictionary<int,double> logicalSpreadDict,List<Instrument> instrumentCollection)
		{
			//checks if all spreads are in the logical interval before executing
			int spreadCheck = 0;
			foreach (Instrument instrument in instrumentCollection)
			{	
				int InsId = instrument.Id;
				double logicalSpread = logicalSpreadDict[InsId];
				double currentBid = bidDict[instrument];
				double currentAsk = askDict[instrument];
				double currentSpread = currentAsk - currentBid;
				if (currentSpread>logicalSpread)
				{
					Console.WriteLine("Spread Check Failed: " +  instrument);
					spreadCheck++;
				} 
			}

			if (spreadCheck>0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		
		public static void executionEngine(Dictionary<Instrument,double> targetDict,Dictionary<Instrument,double> logicalSpreadDict ,List<Instrument> instrumentCollection  )
		{
			// IMPLEMENT CHECK SPREAD
			int spreadsCheckint = spreadChecker(bidDict,askDict,logicalSpreadDict,instrumentCollection);

			if (spreadsCheckint>0)
			{
				foreach(Instrument instrument in targetDict.Keys)
				{
					int targetLot = (int) targetDict[instrument];
					int currentPos = (int) positionChecker(instrument);
					
					if (targetLot>0)
					{
						if (currentPos>0)
						{
							int difference = targetLot-currentPos;
							if (difference>0)
							{
								Buy(instrument,difference,"Long Pos to More Long Pos");
							}
							else if (difference==0)
							{
								// do nothing
							}
							else if (difference<0)
							{
								Sell(instrument,difference*-1,"Long Pos to Less Long Pos");
							}


						}
						else if (currentPos==0)
						{
							int lotstoBuy = targetLot;
							Buy(instrument,lotstoBuy,"Neutral to Long Pos");
						}
						else if (currentPos<0)
						{
							int lotstoBuy = -currentPos + targetLot;
							Buy(instrument,lotstoBuy,"Short to Long Pos");
						}
						
					} 
					else if (targetLot<0)
					{
						if (currentPos>0)
						{
							int lotstoSell = currentPos-targetLot;
							Sell(instrument,lotstoSell,"Short Entry From Long ");
						}
						else if (currentPos==0) 
						{
							int lotstoSell = targetLot*-1;
							Sell(instrument,lotstoSell,"Short Entry From Neutral ");
						}
						else if (currentPos<0)
						{
							int difference = targetLot-currentPos;
							if (difference>0)
							{
								Buy(instrument,difference,"Short Pos to Less Short Pos");
							}
							else if (difference==0)
							{
								// do nothing
							}
							else if (difference<0)
							{
								Sell(instrument,difference*-1,"Short Pos to More Short Pos");
							}

							
						}

					}
					else if (targetLot==0)
					{
						if (currentPos>0)
						{
							int lotstoSell = currentPos;
							Sell(instrument,lotstoSell,"Exit Long Target 0");
						}
						else if (currentPos==0) 
						{
							// do nothing
						}
						else if (currentPos<0)
						{
							int lotstoBuy = currentPos*-1;
							Buy(instrument,lotstoBuy,"Exit Short Target 0");
							
						}
					}
					

				}
			}
			else
			{
				Console.WriteLine("Spread Condition is not satisfied for trading!");
			}
		}
        
   
    }

}
 */
