using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilLib;
using SmartQuant;
using MathNet.Numerics;
using SubStratLib;

namespace QuantLib
{
    public static class DataProcess 
    {

		public static Dictionary<Instrument,double []> getTheoDollar  ( List<List<double>> alignedlist , Dictionary<int,double> tickConversion,List<Instrument> Instruments,bool verbose = false)
		{
			int columnCount=Instruments.Count;
			// first convert the aligned list in to jagged array
			double[][] priceVector = alignedlist.Select(a => a.ToArray()).ToArray();
			double[,] theoDollarDiffVector = new double[columnCount,priceVector.Length-1];
			double[,] theoDollarVector = new double[columnCount,priceVector.Length-2];
			
			// loop through each columns and create a diff jagged array
			Dictionary<Instrument,double []> arrayDict = new Dictionary<Instrument,double []>();	
			for (int col=0; col<columnCount ; col++){				
				double oldVal=0;
				int instrumentID=Instruments[col].Id;
				double conversionFactor=tickConversion[instrumentID];

				for (int row=0; row<priceVector.Length; row++){
					
					if (row==0){
						oldVal = priceVector[row][col]; 

					}
					else
					{
						double latestVal = priceVector[row][col];
						double dollarDiff=((latestVal-oldVal)/(0.03125))*conversionFactor;
						theoDollarDiffVector[col,row-1]=dollarDiff;
						oldVal=latestVal;
					}		
				}
			}
			


			//generate a cumsum list from tick conversion values
			int cols = theoDollarDiffVector.GetLength(0);
			int rows = theoDollarDiffVector.GetLength(1);
			for (int col=0; col<cols ; col++){				
				double oldVal=0;
				for (int row=0; row<rows; row++){
		
					if (row==0){
						oldVal = theoDollarDiffVector[col,row]; 
					}
					else
					{
						double latestVal = theoDollarDiffVector[col,row];
						theoDollarVector[col,row-1]=oldVal+latestVal;
						oldVal=latestVal+oldVal;
					}		
				}
			}

			int colsa = theoDollarVector.GetLength(0);
			int rowsa = theoDollarVector.GetLength(1);
			//verbose

			if (verbose == true){


			for (int i=0; i<rowsa ; i++)
  			{
    			for (int j=0; j<colsa ; j++)
    			{
      				Console.Write(string.Format(theoDollarVector[j, i].ToString() + " "));
			    }
    			Console.WriteLine();
  			}

			}
			// create a dictionary that contains each individual arrays 
			for (int j=0; j<colsa ; j++)
			{
				double[] myArray = new double[rowsa]; 
				Instrument ins = Instruments[j];
				for (int i=0; i<rowsa ; i++) 
				{
					myArray[i]=theoDollarVector[j,i];				
			 	}
			 	arrayDict[ins]=myArray;
			}


			myConsole.WriteLine(" Theoritical Dollar Delta Array Dict Populated");
			//get classifier and feature dict from this theo dollar list

			//return this dict
			return arrayDict;

		}


	}


	public static class RegressionCalculator
	{

		
		public static   double[][] UnpackFeatureArray(double[,] featureArray, int NFeatures, int timeSteps){
			/*
			We need to convert the multi-dim featureArray 
			to and array of arrays in order to match with the
			multiple regression function.
			*/
			double[][] unpackedFeatureArray = new double[timeSteps][];
			for (int r=0;r<timeSteps;r++){
				double[] temp = new double[NFeatures];
				for (int c=0;c<NFeatures;c++){
					temp[c] = featureArray[c,r];
				}
				unpackedFeatureArray[r] = temp;
			}
			return unpackedFeatureArray;
		}	
	
		public static  int checkInpos(SubStrategy subStrategy){

			if (subStrategy.inpos==1){
				subStrategy.incrementSinceEntry=subStrategy.incrementSinceEntry+1;
				return 0; // inpos don't recals
			}
			else
			{
				return 1; // recalc 
			}

		}
	
		public static int checkRecalc(SubStrategy subStrategy){

			myConsole.WriteLine("Increment Since Update:" +subStrategy.incrementSinceUpdate);
			if (subStrategy.incrementSinceUpdate>subStrategy.recalcInterval){
				
				return 1; // recalc
			}
			else
			{
				subStrategy.incrementSinceUpdate=subStrategy.incrementSinceUpdate+1;
				return 0; // dont recalc 
			}

		}
		public static int checkRecalcNeed(SubStrategy subStrategy)
		{

			int inposCheck = checkInpos(subStrategy);
			int reCalcCheck = checkRecalc(subStrategy);

			if ((inposCheck+reCalcCheck)==2)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
			
		public static  int QuantUpdate(SubStrategy subStrategy, Dictionary<int,double> costDict,bool verbose=false){
			/*
			This does the multiple regression calcs, and puts calculated comission and stdev of residuals into the object.
			*/

			
			/*
			myConsole.WriteLine("inposCheck: " +inposCheck);
			myConsole.WriteLine("reCalcCheck: " +reCalcCheck);
			myConsole.WriteLine("Update: " +subStrategy.incrementSinceUpdate);
			myConsole.WriteLine("Entry: " +subStrategy.incrementSinceEntry);
			*/

			subStrategy.incrementSinceUpdate=1;
			myConsole.WriteLine("Recalc Called: " + subStrategy.strategyId);
			// we will only do this regression when we are not inpos and the increment is larger than the thold.
			// also as a utility if we are inpos this function will increment the counter for inpos


			double[,] featureArray = subStrategy.featArray;
			double[] classifierArray = subStrategy.classArray;
			int NFeatures =  featureArray.GetLength(0);
			bool intercept = true;
			try {
				
				double[][] ft = UnpackFeatureArray(featureArray, NFeatures, classifierArray.Length);
				double[] p = MathNet.Numerics.Fit.MultiDim(ft,classifierArray,intercept);
				

				subStrategy.coeffArray=p;
				subStrategy.validRedression = true;
				// now we calculate the residuals and stdev of residuals

				double[] myResid = new double[classifierArray.Length];
				myResid =   CalculateHistoricalResiduals(featureArray,classifierArray,p);
				double myStd = 0;
				myStd = CalculateStdDev(myResid);
				subStrategy.stdevofResid=myStd;

				double myCosts = 0;
				myCosts = CalculateComissionValues(subStrategy,costDict);
				subStrategy.comissionCosts=myCosts; 


				if (verbose==true){
				int colsa = p.GetLength(0);
				for (int i=0; i<colsa ; i++) 
				{
					myConsole.WriteLine("Coeff: " + i + " " +p[i]);				
			 	}
				myConsole.WriteLine("Resid Stdev: " +  myStd);
				myConsole.WriteLine("Cost per Lot: " +  myCosts);
				}
				
				return 1;
			}
			catch {
				myConsole.WriteLine("Numerical Error");
				subStrategy.validRedression = false;
				return 0;
			}
				

		}

		public static double CalculateComissionValues (SubStrategy subStrategy, Dictionary<int,double> costDict)
		{
			List<int> featids = subStrategy.featureIds;
			double totalCommCostperLot;
			totalCommCostperLot = costDict[subStrategy.classifierId];

			for (int i=0;i<featids.Count;i++)
			{
				
				double Coeff = subStrategy.coeffArray[i+1];
			
				double Cost = costDict[featids[i]];


				totalCommCostperLot += Math.Abs(Coeff)*Cost;				
			}
			return totalCommCostperLot; 
		}

		public static   double[] CalculateHistoricalResiduals(double[,] featureArray, double[] classifierArray, double[] regression_coefs)
		{
			double[] histResid = new double[featureArray.Length/(regression_coefs.Length-1)];
			for(int i=0;i<histResid.Length;i++)
			{
				double temp = 0;
				for(int j=0;j<regression_coefs.Length-1;j++)	
				{
					//myConsole.WriteLine(String.Format("{0} {1} {2}",i,j,featureArray[i,j]*regression_coefs[i]));
					temp+=featureArray[j,i]*regression_coefs[j+1];
				}
				histResid[i] = temp - classifierArray[i] + regression_coefs[0]; 
				//myConsole.WriteLine(String.Format("{0} ",histResid[i]));
				
			}
			
			return histResid;
		}

		public static double CalculateStdDev(double[] values)
		{   
		  double ret = 0;
		  if (values.Count() > 0) 
		  {      
		     //Compute the Average      
		     double avg = values.Average();
		     //Perform the Sum of (value-avg)_2_2      
		     double sum = values.Sum(d => Math.Pow(d - avg, 2));
		     //Put it all together      
		     ret = Math.Sqrt((sum) / (values.Count()));   
		  }   
		  return ret;
		}
		public static double getLastTheo(double [] myArray, Instrument myIns,SubStrategy subStrat,Dictionary<Instrument,double> midDict,Dictionary<int,double> tickConversion)
		{
			double lastYPrice = subStrat.lastValDict[myIns];
			double currentYPrice = midDict[myIns];
			int myInsID = myIns.Id;
			double conversionFactor=tickConversion[myInsID];
			double dollarDiff = ((currentYPrice-lastYPrice)/(0.03125))*conversionFactor;
			int arrayCount = myArray.GetLength(0);
			double lastTheo = myArray[arrayCount-1];

			double newTheo = lastTheo + dollarDiff;
			myConsole.WriteLine("----------");
			myConsole.WriteLine("Instrument" + myIns + " last p: "  + lastYPrice);
			myConsole.WriteLine("Instrument" + myIns + " current p: "  + currentYPrice);
			myConsole.WriteLine("Instrument" + myIns + " Last Theo: "  + lastTheo);
			myConsole.WriteLine("Instrument" + myIns + " dollarDiff: "  + dollarDiff);
			myConsole.WriteLine("Instrument" + myIns + " new theo: "  + newTheo);
			return newTheo;
		}



		public static void updateResids(List<SubStrategy> subStratList,Dictionary<Instrument,double> midDict,Dictionary<int,double> tickConversion,bool verbose,Clock Clock)
		{

			foreach (SubStrategy subStrat in subStratList)
			{
				Instrument classifier = subStrat.classifierIns;
				List<Instrument> featList = subStrat.featureInstruments;

				List<double[]> arrayList = new List<double[]>();
				List<Instrument> insList = new List<Instrument>();
				arrayList.Add(subStrat.classArray);
				insList.Add(classifier);
				for (int i=0;i<featList.Count;i++)
				{
					arrayList.Add(DictShapers.convert1DArray(subStrat.featArray ,i ));
					insList.Add(featList[i]);
				}
				
				double [] coeffArray =subStrat.coeffArray; 
				double myResid = 0;
				double subtract = 0;
				double add = 0;
				for (int i=0;i<insList.Count;i++)
				{
					if (i==0){	 
						subtract = getLastTheo(arrayList[i],insList[i],subStrat,midDict,tickConversion);			
					}
					else
					{
						add += getLastTheo(arrayList[i],insList[i],subStrat,midDict,tickConversion)*coeffArray[i];


					}
					
				}
				myResid = add - subtract + coeffArray[0];

				subStrat.lastResidual = myResid;
				

				if (verbose==true)
				{
					myConsole.WriteLine("------------------------------------------------------------------");
					myConsole.WriteLine(Clock.DateTime + " Res:" + myResid);	
					myConsole.WriteLine(Clock.DateTime + " Add:" + add);	
					myConsole.WriteLine(Clock.DateTime + " Subtract:" + subtract);
					myConsole.WriteLine(Clock.DateTime + " Coeff:" + coeffArray[1]);
					myConsole.WriteLine(Clock.DateTime + " Int:" + coeffArray[0]);

					myConsole.WriteLine(Clock.DateTime + " EntryResid:" + subStrat.entryResid);
					myConsole.WriteLine("STDRes: " + Clock.DateTime +" "+ subStrat.lastResidual);
					foreach (Instrument ins in midDict.Keys)
					{
						myConsole.WriteLine("Mid Price "+ ins + " : " + midDict[ins] );
					}	



				}
				


				//residual = x-y+intercept
			}
		}

		public static void updateWeights(int side, SubStrategy subStrat)
		{

			Dictionary<Instrument,double> targetDict = subStrat.targetDict;
			double[] coeffArray =  subStrat.coeffArray; // first one is intercept , others are sorted in the order of featlist
			List<Instrument> featureIns = subStrat.featureInstruments;
			//buy the main sell the features
			Instrument classifier = subStrat.classifierIns;
			int rows = coeffArray.GetLength(0);
			subStrat.side=side;
			subStrat.inpos=1;
			double latestResid = subStrat.lastResidual;
			subStrat.entryResid=latestResid;
			for(int i=0 ; i<rows ; i++)
			{
				if (i==0)
				{
					targetDict[classifier]=-1*side;		
				}
				else
				{
					targetDict[featureIns[i-1]]=coeffArray[i]*side;	
				}
			}		
		}


		public static void zeroWeights(SubStrategy subStrat)
		{
			Dictionary<Instrument,double> targetDict = subStrat.targetDict;
			Instrument classifier = subStrat.classifierIns;
			List<Instrument> featureIns = subStrat.featureInstruments;
			targetDict[classifier]=0;
			subStrat.inpos=0;
			subStrat.side=0;
			subStrat.incrementSinceEntry=0;
			foreach(Instrument ins in featureIns)
			{
				targetDict[ins]=0;
			}

		}

		public static void calculateWeights(List<SubStrategy> subStratList, Dictionary<Instrument,double> askDict, 
			Dictionary<Instrument,double> bidDict, Dictionary<int,double> logicalSpreadDict, List<Instrument> instrumentCollection)
		{
			foreach (SubStrategy subStrat in subStratList)
			{
				int inpos = subStrat.inpos;
				double lastResid = subStrat.lastResidual;
				double thold = subStrat.bandWidth*subStrat.stdevofResid;
				double comCosts = subStrat.comissionCosts;
				bool validity = subStrat.validRedression;
				int side = 0; 
				double revThold = thold*-1;
				double revcomCosts = comCosts*-1;
				int incrementSinceEntry = subStrat.incrementSinceEntry;
				int exitLimit=subStrat.exitLimit;
				int stratSide = subStrat.side;
				double entryResid=subStrat.entryResid;
				double cThold = subStrat.comissionThold;
				double shortExitThold = entryResid-(comCosts*cThold);
				double longExitThold = entryResid+(comCosts*cThold);
				int spreadsCheckint = spreadOps.spreadChecker(bidDict,askDict,logicalSpreadDict,instrumentCollection);

				if (inpos==1 && spreadsCheckint==1)
				{
					if (stratSide==1 && incrementSinceEntry>exitLimit)
					{
						myConsole.WriteLine("Position Update : SubstatId: " + subStrat.strategyId + " Action: Long Exit Time");
						zeroWeights(subStrat);
					}
					else if (stratSide==-1 && incrementSinceEntry>exitLimit)
					{
						myConsole.WriteLine("Position Update : SubstatId: " + subStrat.strategyId + " Action: Short Exit Time");
						zeroWeights(subStrat);
					}
					else if (stratSide==-1 && lastResid<shortExitThold)
					//else if (stratSide==-1 && lastResid<-5)
					{

						myConsole.WriteLine("Position Update : SubstatId: " + subStrat.strategyId + " Action: Short Exit Price");	
						zeroWeights(subStrat);
					}
					else if (stratSide==1 && lastResid>longExitThold)
					//else if (stratSide==1 && lastResid>5)
					{
						myConsole.WriteLine("Position Update : SubstatId: " + subStrat.strategyId + " Action: Long Exit Price");

						zeroWeights(subStrat);
					}


				}
				else if (inpos==0  && spreadsCheckint==1)
				{

					//if (validity==true && lastResid>5 && lastResid>0)
					if (validity==true && lastResid>thold && lastResid>comCosts)
					{
						side = -1;
						myConsole.WriteLine("Position Update : SubstatId: " + subStrat.strategyId + " Action: Short Entry");
						myConsole.WriteLine("Com Cost: " + comCosts);
						updateWeights(side,subStrat);

					}
					//else if (validity==true && lastResid<-5 && lastResid<0)
					else if (validity==true && lastResid<revThold && lastResid<revcomCosts)
					{
						side = 1;
						myConsole.WriteLine("Position Update : SubstatId: " + subStrat.strategyId + " Action: Long Entry");

						updateWeights(side,subStrat);
						
					}

				}
			}

		}



	}
}

    

