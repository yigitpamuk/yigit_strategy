using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;


namespace OpenQuant
{
    public partial class RealTime : Scenario
    {	
		List<string> listCurrencies = new List<string> {"ZB","ZF","ZN"};

		
		
		private long barSize = 60;
        public RealTime(Framework framework)
            : base(framework)
        {
		}
		
        public override void Run()
        {	
			/*
			INSTRUMENT CODES
			ZB 30
			ZF 31
			ZN 38
			ZT 39
			*/
			
			// Strategy generates  combinations and excludes the ones that are not in the filterList
			// TO DO REVERT THE LOGIC
			// first one is the classfier, remaining is features
			List<List<int>> filterList = new List<List<int>>(); 
		
			List<int> elem1 = new List<int> {31,30,38};
			filterList.Add(elem1);
			//List<int> elem2 = new List<int> {31,30,38,39};
			//filterList.Add(elem2);
			//List<int> elem3 = new List<int> {31,30};
			//filterList.Add(elem3);			


			StrategyManager.Mode = StrategyMode.Live;
		//	StrategyManager.Persistence = StrategyPersistence.Full;
			strategy = new MyStrategy(framework, "RealTime",filterList );
			BarFactory.Clear();
			
			strategy.DataProvider = ProviderManager.GetDataProvider("IB");

			foreach (string curr in listCurrencies)
			{
				Instrument instrument = InstrumentManager.GetBySymbol(curr);
				strategy.AddInstrument(instrument, strategy.DataProvider);
				BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle,0);		
			}
			

			
			strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");
			framework.EventManager.Filter = new MyEventFilter(framework);
			StartStrategy();
        }

    }
}





























