using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics;
using SmartQuant;
using CalcLib;
using UtilLib;
using QuantLib;
using SubStratLib;


namespace ExecutionOpsLib

{
	public static class executionOps
	{

	public static void positionPrinter(Portfolio Portfolio, Dictionary<Instrument,double> targetDict, List<Order> orderList)
    	{
    		myConsole.WriteLine("---Position Recap---");
			foreach(Instrument instrument in targetDict.Keys)
			{
				double currentPosition = positionChecker(Portfolio,instrument);
				double pendingOrders = pendingOrderChecker(Portfolio, instrument,orderList);
				myConsole.WriteLine("Instrument: " + instrument.Symbol + " | Target Lots: " + targetDict[instrument] +  " | Current Positions: " +  currentPosition + " |Pending Orders: " +pendingOrders );
			}

    	}
    

		public static void tradingLogic(Instrument instrument, Portfolio Portfolio, Dictionary<Instrument,double> askDict, 
			Dictionary<Instrument,double> bidDict,Dictionary<Instrument,int> marketAskDict,Dictionary<Instrument,int>  marketBidDict,
			List<int> LookBacks ,DateTime earliestFirstNotice , StrategyManager StrategyManager , 
			List<Instrument> instrumentCollection, int stateCheck, int tradingPeriod,List<SubStrategy> subStratList,double lotExposure,
			Dictionary<Instrument,double> midDict, Dictionary<int,double> tickConversion,Dictionary<int,double> logicalSpreadDict,
			Strategy Strategy, List<Order> orderList , bool runExec,Clock Clock )
		{
			//int timeCheck = CheckOperations.bidAskCheck(timeAskDict,timeBidDict,CurrentTimeBar);
			
			int marketCheckInt = CheckOperations.processMarketCheck(instrument,askDict,bidDict,marketAskDict,marketBidDict,LookBacks,
				earliestFirstNotice,StrategyManager,instrumentCollection,Clock);

			// check if the state is updated , check if we are under 10 minutes , check if the bid and ask is updated after onbar, 
			if ((stateCheck==1) && (Clock.DateTime.Minute<tradingPeriod) && marketCheckInt==1)
			{
				

				// calculate current resid for each bid and ask change for substrategy list
				RegressionCalculator.updateResids(subStratList,midDict,tickConversion,true,Clock);
				// generate signal for each substrategy
				RegressionCalculator.calculateWeights(subStratList,askDict,bidDict,logicalSpreadDict,instrumentCollection);
				
				// calculate total target lots long short
				Dictionary<Instrument,double> target = generateTargetLots(subStratList,lotExposure,instrumentCollection);
				
				if (runExec==true)
				{
				executionEngine(target,logicalSpreadDict,instrumentCollection,askDict,bidDict,Portfolio,Strategy,orderList);
				}
				positionPrinter(Portfolio,target,orderList);

			}
		}

		public static Dictionary<Instrument,double> generateTargetLots (List<SubStrategy> subStratList, double lotsExposure, List<Instrument> instrumentCollection  )
	    {
	    	Dictionary<Instrument,double> responseDict = new Dictionary<Instrument,double> ();
	    	foreach (Instrument ins in instrumentCollection)
	    	{
	    		responseDict[ins]=0;
	    	}

	    	foreach (SubStrategy SubStrategy in subStratList)
	    	{
	    		Dictionary<Instrument,double> mytargetDict = SubStrategy.targetDict;
	    		foreach (Instrument ins in mytargetDict.Keys)
	    		{
	    			double target = mytargetDict[ins];
	    			responseDict[ins]+=target;
	    		}
	    	}

	    	foreach (Instrument ins in instrumentCollection)
	    	{
	    		double unMultipled = responseDict[ins];
	    		
	    		double roundedval =Math.Round(unMultipled*lotsExposure); 
	    		responseDict[ins]= roundedval;
	    		//myConsole.WriteLine("Instrument: " + ins + " Target Lot: " + roundedval );
	    	}
	    	return responseDict;

	    }


		public static double positionChecker(Portfolio Portfolio, Instrument ins)
		{
			double response = 0 ;
			foreach(Position pos in Portfolio.Positions)
			{
				if (pos.Instrument==ins)
				{
					response = pos.Amount;
					
				
				}
			}	
			return response;
		}


		public static double pendingOrderChecker(Portfolio Portfolio, Instrument ins, List<Order> Orders)
		{

			double quantity = 0;
			foreach (Order order in Orders)
			{

  				if (order.Instrument==ins)

  				{
  					if (order.IsNew || order.IsNotSent || order.IsPendingNew )
  					{

  						double myQuantity = order.Qty;
  						if (order.Side.ToString()== "Buy")
  						{
  							quantity=myQuantity;
  							/*
  							myConsole.WriteLine("New: " +order.IsNew);
	  						myConsole.WriteLine("NotSent: " + order.IsNotSent);
	  						myConsole.WriteLine("Partial " +order.IsPartiallyFilled);
	  						myConsole.WriteLine("PendingNew " + order.IsPendingNew);
	  						myConsole.WriteLine("IsPendingReplace "+order.IsPendingReplace);
	  						myConsole.WriteLine("Pendingcancel " + order.IsPendingCancel);
	  						myConsole.WriteLine(quantity);
	  						*/
  						}
  						else if (order.Side.ToString()== "Sell")
  						{
  							quantity=myQuantity*-1;
  							/*
  							myConsole.WriteLine("New: " +order.IsNew);
	  						myConsole.WriteLine("NotSent: " + order.IsNotSent);
	  						myConsole.WriteLine("Partial " +order.IsPartiallyFilled);
	  						myConsole.WriteLine("PendingNew " + order.IsPendingNew);
	  						myConsole.WriteLine("IsPendingReplace "+order.IsPendingReplace);
	  						myConsole.WriteLine("Pendingcancel " + order.IsPendingCancel);
	  						myConsole.WriteLine(quantity);
	  						*/

  						}

  					}
  				}
			}
			return quantity;
		}


		
		public static void executionEngine(Dictionary<Instrument,double> targetDict,Dictionary<int,double> logicalSpreadDict ,List<Instrument> instrumentCollection,
			Dictionary<Instrument,double> askDict, Dictionary<Instrument,double> bidDict,Portfolio Portfolio,Strategy Strategy, List<Order> orderList )
		{
			// IMPLEMENT CHECK SPREAD
			int spreadsCheckint = spreadOps.spreadChecker(bidDict,askDict,logicalSpreadDict,instrumentCollection);

			if (spreadsCheckint>0)
			{
				foreach(Instrument instrument in targetDict.Keys)
				{
					int targetLot = (int) targetDict[instrument];
					int closedQuantity = (int) positionChecker(Portfolio,instrument);
					int openQuantity = (int) pendingOrderChecker(Portfolio,instrument,orderList);
					int currentPos = closedQuantity+openQuantity;
					
					if (targetLot>0)
					{
						if (currentPos>0)
						{
							int difference = targetLot-currentPos;
							if (difference>0)
							{
								Strategy.Buy(instrument,difference,"Long Pos to More Long Pos");
							}
							else if (difference==0)
							{
								// do nothing
							}
							else if (difference<0)
							{
								Strategy.Sell(instrument,difference*-1,"Long Pos to Less Long Pos");
							}


						}
						else if (currentPos==0)
						{
							int lotstoBuy = targetLot;
							Strategy.Buy(instrument,lotstoBuy,"Neutral to Long Pos");
						}
						else if (currentPos<0)
						{
							int lotstoBuy = -currentPos + targetLot;
							Strategy.Buy(instrument,lotstoBuy,"Short to Long Pos");
						}
						
					} 
					else if (targetLot<0)
					{
						if (currentPos>0)
						{
							int lotstoSell = currentPos-targetLot;
							Strategy.Sell(instrument,lotstoSell,"Short Entry From Long ");
						}
						else if (currentPos==0) 
						{
							int lotstoSell = targetLot*-1;
							Strategy.Sell(instrument,lotstoSell,"Short Entry From Neutral ");
						}
						else if (currentPos<0)
						{
							int difference = targetLot-currentPos;
							if (difference>0)
							{
								Strategy.Buy(instrument,difference,"Short Pos to Less Short Pos");
							}
							else if (difference==0)
							{
								// do nothing
							}
							else if (difference<0)
							{
								Strategy.Sell(instrument,difference*-1,"Short Pos to More Short Pos");
							}

							
						}

					}
					else if (targetLot==0)
					{
						if (currentPos>0)
						{
							int lotstoSell = currentPos;
							Strategy.Sell(instrument,lotstoSell,"Exit Long Target 0");
						}
						else if (currentPos==0) 
						{
							// do nothing
						}
						else if (currentPos<0)
						{
							int lotstoBuy = currentPos*-1;
							Strategy.Buy(instrument,lotstoBuy,"Exit Short Target 0");
							
						}
					}
					
					

				}
			}
			else
			{
				myConsole.WriteLine("Spread Condition is not satisfied for trading!");
			}
		}
	}
}







