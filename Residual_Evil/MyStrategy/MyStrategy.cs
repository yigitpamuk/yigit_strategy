using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SmartQuant;
using CalcLib;
using UtilLib;
using QuantLib;
using MathNet.Numerics;
using SubStratLib;
using ExecutionOpsLib;

namespace OpenQuant
{
    public class MyStrategy : Strategy
    {
		// STRATEGY PARAMETER SETS		
		public List<int> LookBacks = new List<int>(){200};
		public List<int> Exits = new List<int>(){200};
		public List<double> Bands = new List<double>(){3};
		public List<int> RecalculateIntervals = new List<int>(){100};
		public List<double> ComissionTholds = new List<double>(){1.5};

		
		[Parameter]
		public double  illogicThold= 0.2;		//remove the datapoint if it is more than this amount of current price
		
		[Parameter]
		public double  lotExposure= 1000;		//Amount of Lots to trade per substrategy for classifier, the features will be scaled according to this value
		
		[Parameter]
		public double  comissionPerLot= 1.6;		//USD
		
		[Parameter]
		public int  tradingPeriod= 70; // after the bar is called we have this many minutes to put our trades, the executor will run this many minutes after the bar		

		[Parameter]
		public DateTime  earliestFirstNotice = new DateTime(2019, 06, 03, 0, 0, 0);	//earliest First notice date for our contract universe.  

		public Dictionary<Instrument,double> bidDict;
		public Dictionary<Instrument,double> askDict;
		public Dictionary<Instrument,double> midDict;
		

		public Dictionary<Instrument,int> marketAskDict;
		public Dictionary<Instrument,int> marketBidDict;
		public Dictionary<Instrument,int> marketMidDict;
		

		public Dictionary<Instrument,DateTime> timeAskDict;
		public Dictionary<Instrument,DateTime> timeBidDict;

		public Dictionary<int,Instrument> idDict;
		public Dictionary<int,double> tickConversion;
		public Dictionary<int,double> costDict;
		public Dictionary<int,double> logicalSpreadDict;
		public Dictionary<Instrument,int> callCheck;

		public List<List<int>> combinListnew;
		public List<SubStrategy> subStratList;
		public List<Instrument> instrumentCollection;


		List<List<int>> filterListStrat = new List<List<int>>();
		public List<Dictionary<DateTime,Dictionary<Instrument,double>>> priceDictList;

		public DateTime LastUpdate;
		public DateTime CurrentTime;
		public DateTime CurrentTimeBar;
		public int stateCheck;
		public int bidCounter;
		public int askCounter;



		public MyStrategy(Framework framework, string name, List<List<int>> filterList)
            : base(framework, name)
        {
			filterListStrat=filterList;
		}
		
		protected override void OnProviderDisconnected(Provider provider)
		{
		
			if (provider.Id == 4)
			{
				myConsole.WriteLine("IB has disconnected.");

				while (!provider.IsConnected)
				{
					myConsole.WriteLine("provider.Connect()");
					provider.Connect();
					while(provider.IsConnecting)
					{
						myConsole.WriteLine("Connecting ...");
					}
					
				}
				myConsole.WriteLine("provider Connected.");
				
			}
		}

		protected override void OnStrategyStart()
		{
			myConsole.WriteLine("Strategy Initiated.");
			stateCheck=0;
			bidCounter=0;
			askCounter=0;

			CurrentTimeBar = Clock.DateTime;
			CurrentTime=Clock.DateTime;
			instrumentCollection = new List<Instrument>();
			foreach (Instrument instrument in Instruments)
			{
				instrumentCollection.Add(instrument);
			}
			// initbidask


			List<object> environmentVariables = InitOps.initBidAskIdIns( instrumentCollection, bidDict, askDict , midDict, idDict , marketAskDict, 
				marketBidDict,timeAskDict  ,timeBidDict  ,callCheck,Clock );

			bidDict=(Dictionary<Instrument,double>) environmentVariables[0];
			askDict=(Dictionary<Instrument,double>) environmentVariables[1];
			midDict=(Dictionary<Instrument,double>) environmentVariables[2];
			idDict= (Dictionary<int,Instrument>) environmentVariables[3];
			marketBidDict=(Dictionary<Instrument,int>) environmentVariables[4];
			marketAskDict=(Dictionary<Instrument,int>) environmentVariables[5];
			callCheck=(Dictionary<Instrument,int>) environmentVariables[6];
			timeAskDict=(Dictionary<Instrument,DateTime>) environmentVariables[7];
			timeBidDict=(Dictionary<Instrument,DateTime>) environmentVariables[8];

			// a list of lists with the classifier in the beginning
			combinListnew=  CombinGenerator.generateCombinationsAndFilter(filterListStrat,instrumentCollection);

			// add tick conversion factors to tickdictionary and cost Dictionary
			if (StrategyManager.Mode == StrategyMode.Live)
			{
				List<Dictionary<int,double>> costAndConversions =  TimeOperations.setTickConversionsLive(comissionPerLot,tickConversion,costDict,logicalSpreadDict);
	    		costDict = costAndConversions[0];
				tickConversion = costAndConversions[1];
				logicalSpreadDict = costAndConversions[2];

	    	}
	    	else if (StrategyManager.Mode == StrategyMode.Backtest)
	    	{
				List<Dictionary<int,double>> costAndConversions =  TimeOperations.setTickConversionsBacktest(comissionPerLot,tickConversion,costDict,logicalSpreadDict);
		    	costDict = costAndConversions[0];
				tickConversion = costAndConversions[1];
				logicalSpreadDict = costAndConversions[2];
	    	}

			//generate parameter combinations for all lookbacks
			subStratList=SubStrategyOps.initSubStrategies(combinListnew, idDict ,LookBacks, Bands, ComissionTholds, RecalculateIntervals, Exits,Clock);
			
			// init reminder
			//TimeSpan StartTime = new TimeSpan(0,0,0);
			//AddReminder(Clock.DateTime.Date.Add(StartTime));


		}




		protected override void OnAsk(Instrument instrument, Ask ask)
		{	
			// first first 5 changes just to populate the dicts, we run with volume data and OnAsk functions will not be called:

			

		//	if ( instrument.Ask.Price != askDict[instrument] ){
				
				if (instrument.Ask.DateTime>timeAskDict[instrument] && instrument.Ask.Price>0   )
				{
					timeAskDict[instrument]=instrument.Ask.DateTime;
					askDict[instrument]=instrument.Ask.Price;
					midDict[instrument]=(instrument.Ask.Price+bidDict[instrument])/2;
					//myConsole.WriteLine("instrument:" +instrument+" P:" +midDict[instrument] + " D" + Clock.DateTime );

				}
				List<Dictionary<Instrument,int>> marketCheckList = CheckOperations.marketCheck(instrument,1,askDict,bidDict,marketAskDict, 
				 	marketBidDict, LookBacks,earliestFirstNotice,StrategyManager,Clock);
				 marketAskDict = marketCheckList[0];
				 marketBidDict = marketCheckList[1];
				
				

			
		}
				

		protected override void OnBid(Instrument instrument, Bid bid)
        {	
         


        //	if ( instrument.Bid.Price !=bidDict[instrument] ){
				if (instrument.Bid.DateTime>timeBidDict[instrument] && instrument.Bid.Price>0 )
				{
					bidDict[instrument]=instrument.Bid.Price;
					timeBidDict[instrument]=instrument.Bid.DateTime;
					midDict[instrument]=(instrument.Bid.Price+askDict[instrument])/2;
					//myConsole.WriteLine("instrument:" +instrument+" P:" +midDict[instrument] + " D" + Clock.DateTime );
				}
				List<Dictionary<Instrument,int>> marketCheckList = CheckOperations.marketCheck(instrument,2,askDict,bidDict,marketAskDict, 
				 	marketBidDict, LookBacks,earliestFirstNotice,StrategyManager,Clock);
				 marketAskDict = marketCheckList[0];
				 marketBidDict = marketCheckList[1];

	
        }


		protected override void OnBar (Instrument	instrument	, Bar bar)
		{	
			


			// check if the Onbar called for all instruments
			//int callCheckInt = CheckOperations.processCallCheck(instrument,callCheck,instrumentCollection);
			int callCheck = UpdateDateTime(bar);
			// check if the market is Open and prices are valid
			//Instrument instrument = instrumentCollection[0];
			int marketCheckInt = CheckOperations.processMarketCheck(instrument,askDict,bidDict,marketAskDict,marketBidDict,LookBacks,
				earliestFirstNotice,StrategyManager,instrumentCollection,Clock);
			
			
			if ((marketCheckInt==1) &&  (callCheck==1) )
			{

				int barDataCheck = DataOps.uniqCheck(instrument,bar,Clock,DataManager);// it means that the last downloaded bar is not the same information with the current one.
				if (barDataCheck==1)
				{
					stateCheck=0;
					stateCheck=StateManager.updateBarState(instrumentCollection,LookBacks,StrategyManager, DataManager ,tickConversion,
						subStratList, illogicThold,askDict, bidDict,costDict, Clock );
					
					cancelOpenOrders();
					List<Order> orderList = generateOrderList();
					executionOps.tradingLogic( instrument,  Portfolio, askDict, bidDict, marketAskDict, marketBidDict,
						LookBacks , earliestFirstNotice , StrategyManager , instrumentCollection,  stateCheck,
						tradingPeriod,subStratList, lotExposure,midDict,tickConversion,logicalSpreadDict, this, orderList,true,Clock);	
				}
				
			}

			Portfolio.Performance.Update();
			//AddReminder(Clock.DateTime.AddHours(1));
					
		}

		protected override void OnFill (Fill fill)
			{
				Log(fill, "Fills");
				myConsole.WriteLine("FillLog:" + fill);
				Instrument instrument = fill.Instrument;
				List<Order> orderList = generateOrderList();
				/*
				executionOps.tradingLogic( instrument,  Portfolio, askDict, bidDict, marketAskDict, marketBidDict,
					LookBacks , earliestFirstNotice , StrategyManager , instrumentCollection,  stateCheck,
					tradingPeriod,subStratList, lotExposure,midDict,tickConversion,logicalSpreadDict, this, orderList,false,Clock);	
				*/
			}
		
		protected override void OnOrderCancelled  (Order order)
		// to make sure all the pending orders are cancelled.
		{
			bool hasPendingOrders = false; 
			foreach (Order myorder in OrderManager.Orders) 
			{
				if (myorder.IsDone==false || myorder.IsPendingCancel==false ) 
				{
					if (myorder.IsCancelled==false)
					{
						hasPendingOrders = true;
					} 
				}
			}
			
			if (hasPendingOrders)
			{
				cancelOpenOrders();
			}

		}

		public  void cancelOpenOrders()
		{
			
			foreach (Order myorder in OrderManager.Orders)
			{
         		if (!myorder.IsDone)
         		{
            		Cancel(myorder);
         		}
			}
		}

		public  List<Order> generateOrderList()
		{	
			List<Order> responseList = new List<Order>(); 
			foreach (Order myorder in OrderManager.Orders)
			{
				responseList.Add(myorder);
			}
			return responseList;
		}
        public int UpdateDateTime(Bar bar)
        {
			if (bar.DateTime != CurrentTime){
				CurrentTime = bar.DateTime;
				return 1;
			}else{ return 0; }
		}

	}
    
		
}

















































