using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SubStratLib;

using SmartQuant;
using QuantLib;
using UtilLib;
using MathNet.Numerics;


namespace CalcLib
{
	public static class CombinGenerator
	{
		public static bool NextCombination(IList<int> num, int n, int k)
		{
			bool finished;
 
			var changed = finished = false;
 
			if (k <= 0) return false;
 
			for (var i = k - 1; !finished && !changed; i--)
			{
				if (num[i] < n - 1 - (k - 1) + i)
				{
					num[i]++;
 
					if (i < k - 1)
						for (var j = i + 1; j < k; j++)
							num[j] = num[j - 1] + 1;
					changed = true;
				}
				finished = i == 0;
			}
 
			return changed;
		}
 
		public static  IEnumerable Combinations<T>(IEnumerable<T> elements, int k)
		{
			var elem = elements.ToArray();
			var size = elem.Length;
 
			if (k > size) yield break;
 
			var numbers = new int[k];
 
			for (var i = 0; i < k; i++)
				numbers[i] = i;
 
			do
			{
				yield return numbers.Select(n => elem[n]);
			} while (NextCombination(numbers, size, k));
		}
 

	

	public static List<List<int>> generateCombinationsAndFilter(List<List<int>> filterListStrat, List<Instrument> Instruments)
		{
			//CombinGenerator CombinGener = new CombinGenerator();
			List<List<int>> combinList = new List<List<int>>();
					
			foreach (Instrument insa in Instruments){
				
				List<int> iterList = new List<int>();
				
				foreach (Instrument insb in Instruments){
					if (insa!=insb){
			
						iterList.Add(insb.Id);
					}
				}
				
			for ( int ii=1; ii<=3 ; ii++)
			{    
				foreach (IEnumerable<int> i in Combinations(iterList, ii)){
					List<int> subcomb_list = new List<int>();
					subcomb_list.Add(insa.Id);
					foreach (int insid in i ){
						subcomb_list.Add(insid);
							
					}
					combinList.Add(subcomb_list);
				}
			}
		
			}


			foreach (List<int> comb in combinList)
			{
				foreach (int z in comb)
				{
					Console.Write(z + " ");
				}
				myConsole.WriteLine(" ");
			}
				
			myConsole.WriteLine("Total Instrument Combinations Generated: " + combinList.Count);
			List<List<int>> combinListnew = new List<List<int>>();
			// Filter the combinations:
			foreach (List<int> comb in combinList){

				foreach (List<int> filter in filterListStrat){
					if (filter.SequenceEqual(comb)){
						combinListnew.Add(comb);
					}
				}
				}
				
			myConsole.WriteLine("Total Instrument Combinations Used: " + combinListnew.Count);
			myConsole.WriteLine("-------------------------------------- ");
			myConsole.WriteLine("Instrument Combination List: ");
			foreach (List<int> comb in combinListnew){
				foreach (int i in comb ){
					Console.Write(i + " ");
				}
				myConsole.WriteLine(" ");
	        }
	        myConsole.WriteLine("-------------------------------------- ");
	        return combinListnew;
	    }
    }
    
	public static class InitOps
	{
	    public static List<object> initBidAskIdIns(List<Instrument> instrumentCollection,  Dictionary<Instrument,double> bidDict,
	     Dictionary<Instrument,double> askDict , Dictionary<Instrument,double> midDict,Dictionary<int,Instrument> idDict , Dictionary<Instrument,int> marketAskDict,
	     Dictionary<Instrument,int> marketBidDict, Dictionary<Instrument,DateTime> timeAskDict,Dictionary<Instrument,DateTime> timeBidDict  ,
	     Dictionary<Instrument,int>  callCheck,Clock Clock )
	    {
			bidDict = new Dictionary<Instrument,double>();
			askDict = new Dictionary<Instrument,double>();
			midDict = new  Dictionary<Instrument,double>();

			
			idDict = new Dictionary<int,Instrument>();
			marketAskDict = new  Dictionary<Instrument,int>();
			marketBidDict = new  Dictionary<Instrument,int>();

			timeAskDict = new  Dictionary<Instrument,DateTime>();
			timeBidDict = new  Dictionary<Instrument,DateTime>();

			callCheck = new  Dictionary<Instrument,int>();

			

			foreach (Instrument instrument in instrumentCollection)
			{
				bidDict[instrument]=0;
				askDict[instrument]=0;
				midDict[instrument]=0;
				idDict[instrument.Id]=instrument;
				marketBidDict[instrument]=0;
				marketAskDict[instrument]=0;
				callCheck[instrument]=0;
				
				timeAskDict[instrument]=Clock.DateTime;
				timeBidDict[instrument]=Clock.DateTime;
		
			}
			List<object> responseList = new List<object> ();
			responseList.Add(bidDict);
			responseList.Add(askDict);
			responseList.Add(midDict);
			responseList.Add(idDict);
			responseList.Add(marketBidDict);
			responseList.Add(marketAskDict);
			responseList.Add(callCheck);
			responseList.Add(timeAskDict);
			responseList.Add(timeBidDict);

			return responseList; 


	    }
	}

	public static class StateManager
	{


		public static void updateLastValueDict(List<List<double>> alignedlist,List<Instrument> instrumentCollection,List<SubStrategy> subStratList)
		{

			foreach (SubStrategy SubStrategy in subStratList)
			{
				int iii=0;
				foreach (Instrument instrument in instrumentCollection)	
				{					 
					SubStrategy.lastValDict[instrument]=alignedlist[alignedlist.Count-2][iii];
					iii++;
				}

			}

		}


		public static void updateState(List<List<double>> alignedlist,Dictionary<int,double> tickConversion,
			List<Instrument> instrumentCollection,List<SubStrategy> subStratList,Dictionary<int,double> costDict)
		{
			//(List<List<double>> alignedlist,Dictionary<int,double> tickConversion,List<instrument> instrumentCollection,List<Strategy> subStratList,Dictionary<int,double> costDict)
			Dictionary<Instrument,double []> vectorDict = new Dictionary<Instrument,double []>();

			// put the last used prices in to the SubStrat object
			

			vectorDict=DataProcess.getTheoDollar(alignedlist , tickConversion,instrumentCollection, false);

			// for each SubStrategy , iterate, filter the arrays and generate coefficients


			// for each combination set prepare tick adjusted series.
			foreach (SubStrategy  subStrategy in subStratList){
				//generate feature array and classifier array for instruments
				Instrument classifier = subStrategy.classifierIns;
				List<Instrument> featuresList = subStrategy.featureInstruments;
				int lookBack =  subStrategy.lookBack;
				List<double[]> featuresArrayList = new List<double[]>(); 
				foreach (Instrument ins in featuresList){

					featuresArrayList.Add(vectorDict[ins]);
				}
				



				// update coefficients
				int checkUpdateNeed = RegressionCalculator.checkRecalcNeed(subStrategy);//ceck if a regression update is needed
				// if the regression is updated than insert the last vals:
				
				if ( checkUpdateNeed==1)
				{
					double[,] featArray = new double[featuresList.Count,featuresArrayList[0].Length];

					double[] classArray = new double[featuresArrayList[0].Length];
					
					featArray = DictShapers.To2DArray(featuresArrayList,false);
					classArray = vectorDict[classifier];
					

					//check if we have enough data and add the trimmed feature vector in to the object 
					//trim so that the last bar is not included and the lookback is included and lookback is satisfied
					DictShapers.Trim2DArray(featArray,lookBack,subStrategy,false);
					DictShapers.Trim1DArray(classArray,lookBack,subStrategy,false);
				
					RegressionCalculator.QuantUpdate(subStrategy,costDict,true);
					updateLastValueDict(alignedlist,instrumentCollection,subStratList);//add last prices to the object
				}


				}
			}


		public static int updateBarState(List<Instrument> instrumentCollection,List<int> LookBacks,
			StrategyManager StrategyManager,DataManager DataManager ,Dictionary<int,double> tickConversion,List<SubStrategy> subStratList,
			double illogicThold, Dictionary<Instrument,double> askDict, Dictionary<Instrument,double> bidDict,Dictionary<int,double> costDict,Clock Clock)
		{	
			int badData=0;
			myConsole.WriteLine("------------------------------NEW BAR CALLED----------------------------------------");
			myConsole.WriteLine("------------------------------DATETIME: "+Clock.DateTime+"----------------------------------------");
	

			List<Dictionary<DateTime,Dictionary<Instrument,double>>> priceDictList = new  List<Dictionary<DateTime,Dictionary<Instrument,double>>>();
			foreach (Instrument Ins in instrumentCollection){
				BarSeries historicalBars = DataOps.getBarsForMe(Ins,LookBacks.Max(),StrategyManager,DataManager,Clock); 
				

				
				if (historicalBars.Count == 0){
					badData++;
					myConsole.WriteLine("No bars to update state!, Check Connection!");
				}
				else
				{
					Dictionary<DateTime,Dictionary<Instrument,double>> dataDict =  DictShapers.ConvertBarsToDict(Ins,historicalBars);
					priceDictList.Add(DictShapers.checkDatapoints(dataDict,illogicThold,askDict,bidDict,Clock));
				}

			}

			if (badData==0)
			{
								//get aligned list aligned accounding to instrument list order
				List<List<double>> alignedlist = DictShapers.alignedListGenerator(priceDictList,instrumentCollection,false);
				myConsole.WriteLine("Aligned List Generated");
				myConsole.WriteLine("-------------------------------------- ");
				StateManager.updateState(alignedlist,tickConversion,instrumentCollection,subStratList,costDict);
			//	stateCheck=1;
				myConsole.WriteLine("State Updated");
				return 1;
			}
			else
			{
				myConsole.WriteLine("State Update Failed!");
				return 0;

			}
		}


	}


}





