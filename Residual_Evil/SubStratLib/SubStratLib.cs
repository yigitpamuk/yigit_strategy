using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;

//using System.Web.Script.Serialization;

namespace SubStratLib
{

	public  class SubStrategy
	// This class is the backbone for multi strategy implementation, we will loop throgh list of SubStrategies
		{
			public int classifierId {get;set;}
			public Instrument classifierIns {get;set;}
				
		    		
			public List<int> featureIds {get;set;}
			public List<Instrument> featureInstruments {get;set;}
			
			public int lookBack {get;set;}
			public double bandWidth {get;set;}
			public double comissionThold{get;set;}
			public int recalcInterval {get;set;}
			public int exitLimit {get;set;}
			public DateTime LastUpdated {get;set;}  		
			public int inpos {get;set;}	
			public int incrementSinceUpdate {get;set;}

			//below cannot be filled before we actually process the data

			public double lastResidual {get;set;}

			public double comissionCosts {get;set;} 
			public double stdevofResid {get;set;}
			public double entryResid {get;set;}
			public int side {get;set;}

			public bool lookBackCritFeat {get;set;} 
			public bool lookBackCritClass {get;set;} 
			public bool validRedression {get;set;}
			public double[,] featArray {get;set;}
			public double[] classArray {get;set;}
			public double[] coeffArray {get;set;}

			
			public string strategyId {get;set;} 
			public int incrementSinceEntry {get;set;}
			public Dictionary<Instrument,double> lastValDict {get;set;}
			public Dictionary<Instrument,double> targetDict {get;set;}

		}

	public static  class SubStrategyOps
	{
		public static List<SubStrategy> initSubStrategies(List<List<int>> combinlist,  Dictionary<int,Instrument> idDict ,
			// creates a list of SubStrats for the beginning
			List<int> lookBacks, List<double> bandWidths, List<double> comissionTholds, List<int> recalcIntervals, List<int> exitLimits, Clock Clock )
			{
				List<SubStrategy> resultList = new List<SubStrategy>(); 
				foreach (List<int> combinline in  combinlist){


					List<int> featids = new List<int>();
					List<Instrument> featInstruments = new List<Instrument>();
					
					Dictionary<Instrument,double> targetDict = new Dictionary<Instrument,double>() ;
					Dictionary<Instrument,double> mylastValDict = new Dictionary<Instrument,double> ();
					targetDict[idDict[combinline[0]]]=0;
					for (int i=1; i<combinline.Count; i++ ){
						featids.Add(combinline[i]);
						featInstruments.Add(idDict[combinline[i]]); 

						mylastValDict[idDict[combinline[i]]]=0;
						targetDict[idDict[combinline[i]]]=0;

					}



					foreach (int lb in lookBacks ){
						foreach (double bw in bandWidths ){
							foreach (double cthold in comissionTholds ){
								foreach (int rcalc in recalcIntervals ){
									foreach (int exit in exitLimits ){
										SubStrategy mySubStrat =  new SubStrategy();
										mySubStrat.classifierId=combinline[0];//OK
										mySubStrat.classifierIns=idDict[mySubStrat.classifierId];//OK
										mySubStrat.featureIds=featids;//OK
										mySubStrat.featureInstruments=featInstruments;//OK				
										mySubStrat.lookBack=lb;//OK
										mySubStrat.bandWidth=bw;//OK
										mySubStrat.comissionThold=cthold;//OK
										mySubStrat.recalcInterval=rcalc;//OK
										mySubStrat.exitLimit=exit;//OK
										mySubStrat.featArray=new double [featInstruments.Count,lb-1] ;//OK
										mySubStrat.classArray=new double [lb-1] ;//OK
										mySubStrat.lookBackCritFeat=false;//OK
										mySubStrat.lookBackCritClass=false;//OK
										mySubStrat.comissionCosts=0;// OK
										mySubStrat.stdevofResid=0;// OK
										mySubStrat.coeffArray= new double [featInstruments.Count+1]; // OK
										mySubStrat.validRedression = false; // OK
										mySubStrat.incrementSinceUpdate= 100000;// OK
										mySubStrat.lastValDict=mylastValDict;// contains the last data point price that is used for each regression.
										mySubStrat.lastResidual=0;// On Bid and On Ask
										mySubStrat.side=0; // Order Logic
										mySubStrat.targetDict=targetDict;
										mySubStrat.entryResid=0;
										mySubStrat.incrementSinceEntry= 0;// Execution Class and OnBarOps
										mySubStrat.inpos= 0; // Execution Class
										//DYNAMIC VARIABLES
										

										mySubStrat.LastUpdated= Clock.DateTime; // Not sure if this is usable yet
										
										
										

										
										string myClassString = " C:" +  idDict[mySubStrat.classifierId].Symbol;
										string myFeatString = "";
										int ii=0;
										foreach(Instrument ins in featInstruments )
										{
											ii++;
											myFeatString+=" F"+ii+": "+ins.Symbol; // if i want to separate them by pipe symbol
										}

										string myFinalString = myClassString +" | "+myFeatString+" | Lback: "+lb+" | BWidth: "+bw+" | CThold: "+cthold+" | Recalc: "+rcalc+" | Exit: "+exit;
										Console.WriteLine(myFinalString);

										mySubStrat.strategyId=myFinalString;//OK
										resultList.Add(mySubStrat);
									}
								}
							}
						}
					}


					}
			int mycheck = lookBacks.Count*bandWidths.Count*comissionTholds.Count*recalcIntervals.Count*exitLimits.Count* combinlist.Count;	
			Console.WriteLine(" Instrument Parameter Combinations Used: " + resultList.Count + " | CheckSum: " + mycheck);
			Console.WriteLine("--------------------------------");
			/*
			foreach (SubStrategy item in resultList){
				JavaScriptSerializer js = new JavaScriptSerializer();
  				string json = js.Serialize(item);
				myConsole.WriteLine(json );				}
			*/
			return resultList;
				
			}
		

		}
}
