using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartQuant;


namespace OpenQuant
{
	public partial class Scenerio : Scenario
	{	
		List<string> listCurrencies = new List<string> {"ZFC1","ZNC1"};

		
		
		private long barSize = 60*60;
		public Scenerio(Framework framework)
			: base(framework)
		{
		}
		
		public override void Run()
		{	
			/*
			INSTRUMENT CODES FOR BACKTEST
			ZB 26
			ZF 27
			ZN 28
			ZT 29
			*/
			
			// Strategy generates  combinations and excludes the ones that are not in the filterList
			// TO DO REVERT THE LOGIC
			// first one is the classfier, remaining is features
			List<List<int>> filterList = new List<List<int>>(); 
		
			List<int> elem1 = new List<int> {27,28};
			filterList.Add(elem1);
			//List<int> elem2 = new List<int> {31,30,38,39};
			//filterList.Add(elem2);
			//List<int> elem3 = new List<int> {31,30};
			//filterList.Add(elem3);			


			StrategyManager.Mode = StrategyMode.Backtest;

			//	StrategyManager.Persistence = StrategyPersistence.Full;
			strategy = new MyStrategy(framework, "Backtest",filterList );
			BarFactory.Clear();
			strategy.ExecutionSimulator.CommissionProvider.Commission = 1.6;
			//strategy.DataProvider = ProviderManager.GetDataProvider("IB");

			foreach (string curr in listCurrencies)
			{
				Instrument instrument = InstrumentManager.GetBySymbol(curr);
				strategy.AddInstrument(instrument, strategy.DataProvider);
				BarFactory.Add(instrument, BarType.Time, barSize, BarInput.Middle,0);		
			}
			
			DataSimulator.DateTime1 = new DateTime(2014, 1, 1);
			DataSimulator.DateTime2 = new DateTime(2020, 11, 1);
			Framework.EventBus.ReminderOrder = ReminderOrder.After;
			//strategy.ExecutionProvider = ProviderManager.GetExecutionProvider("IB");

			StartStrategy();
		}

	}
}





































