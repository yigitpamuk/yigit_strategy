using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubStratLib;
using SmartQuant;
using System.Web.Script.Serialization;
//using ExecutionOpsLib;


namespace UtilLib
{
    public static class DictShapers 
    {
    	public static double[] convert1DArray (double [,] myArray , int index )
		{
			int rows = myArray.GetLength(1);
		
			double[] returnArray = new double[rows];

	    	for (int i = 0; i < rows; i++) 
	    	{
	    		returnArray[i]=myArray[index,i];
	    	}
	    	return returnArray; 
	    }

    	public static double[,]  To2DArray(List<double[]> arrayList, bool verbose = false)
		{
			//Combined 2 seperate arrays into 1 array
	    	
			int rows =arrayList[0].Length; 
			int cols =arrayList.Count;

			double[,] combinedArray = new double[cols,rows];
	    	for (int i = 0; i < rows; i++) 
	    	{
	    		for (int j = 0; j < cols; j++)
	    		{

	    			combinedArray[j,i]=arrayList[j][i];
	    		} 

	    	}

	    	if (verbose == true){


			for (int i=0; i<rows ; i++)
  			{
    			for (int j=0; j<cols ; j++)
    			{
      				Console.Write(string.Format(arrayList[j][i].ToString() + " "));
			    }
    			Console.WriteLine();
  			}
 			myConsole.WriteLine("Input Columns " + cols);
			myConsole.WriteLine("Rows " + rows);  		
  		}

	    	if (verbose == true){


			for (int i=0; i<rows ; i++)
  			{
    			for (int j=0; j<cols ; j++)
    			{
      				Console.Write(string.Format(combinedArray[j, i].ToString() + " "));
			    }
    			Console.WriteLine();
  			}
 			myConsole.WriteLine(" Output Columns " + cols);
			myConsole.WriteLine("Rows " + rows);  		
  		}
			
	    	return combinedArray;
	    }

    	public static void  Trim2DArray(double[,] myArray,int lookBack, SubStrategy subStrategy, bool verbose = false)
		{
	    	// Trim 2D array, returns 2d array ins ize of lookback without the last observation

			int cols = myArray.GetLength(0);
			int rows = myArray.GetLength(1);
			// if the lookback is 200 => the array is 1000, get the values between 799-999
			
			// not enough bars condition check
			// bars needed 200 + 1
			// bars available rows
			if (rows>=lookBack+1){
				double[,] combinedArray = new double[cols,lookBack];

				int newIndex =  lookBack;
		    	for (int i = rows-2; i > rows-2-lookBack; i--) 
		    	{

		    		newIndex--; 
		    		for (int j =0; j < cols; j++)
		    		{
		    			
		    			combinedArray[j,newIndex]=myArray[j,i];
		    		} 

		    	}
				if (verbose == true){


				for (int i=0; i<rows ; i++)
	  			{
	    			for (int j=0; j<cols ; j++)
	    			{
	      				Console.Write(string.Format(myArray[j, i].ToString() + " "));
				    }
	    			Console.WriteLine();
	  			}
	 			myConsole.WriteLine("Input Columns " + cols);
				myConsole.WriteLine("Input Rows " + rows);  		
	  		}

				if (verbose == true){

				int colsa = combinedArray.GetLength(0);
				int rowsa = combinedArray.GetLength(1);

				for (int i=0; i<rowsa ; i++)
	  			{
	    			for (int j=0; j<colsa ; j++)
	    			{
	      				Console.Write(string.Format(combinedArray[j, i].ToString() + " "));
				    }
	    			Console.WriteLine();
	  			}
	 			myConsole.WriteLine("Output Columns " + colsa);
				myConsole.WriteLine("Output Rows " + rowsa);  		
	  		}
	  		subStrategy.lookBackCritFeat=true;
	  		subStrategy.featArray=combinedArray;

			}
			else
			{
				myConsole.WriteLine("Not Enoguh Data For Lookback , Classifier Data");

			}

	    	
	    }

    	public static void  Trim1DArray(double[] myArray,int lookBack, SubStrategy subStrategy, bool verbose = false)
		{
	    	// Trim 1D array, returns 1d array ins ize of lookback without the last observation
			int rows = myArray.GetLength(0);
			
			if (rows>=lookBack+1){
				double[] combinedArray = new double[lookBack];

				int newIndex =  lookBack;
		    	for (int i = rows-2; i > rows-2-lookBack; i--) 
		    	{

		    		newIndex--; 
		    		combinedArray[newIndex]=myArray[i];
		    		 

		    	}


				if (verbose == true){


				for (int i=0; i<rows ; i++)
	  			{

	      			Console.Write(string.Format(myArray[i].ToString() + " "));
	    			Console.WriteLine();
	  			}

				myConsole.WriteLine("Input Rows " + rows);  		
	  		}

				if (verbose == true){

				int rowsa = combinedArray.GetLength(0);

				for (int i=0; i<rowsa ; i++)
	  			{

	      			Console.Write(string.Format(combinedArray[i].ToString() + " "));
	    			Console.WriteLine();
	  			}

				myConsole.WriteLine("Output Rows " + rowsa);  		
	  		}
	  		subStrategy.lookBackCritClass=true;
	  		subStrategy.classArray=combinedArray;

			}
		else
			{
				myConsole.WriteLine("Not Enoguh Data For Lookback , Classifier Data");

			}

	    	
	    }

		public static Dictionary<DateTime,Dictionary<Instrument,double>> ConvertBarsToDict(Instrument instrument, BarSeries myseries)
		{			
			// Convert Bar Objects to Dictionary
			Dictionary<DateTime,Dictionary<Instrument,double>> histDict = new Dictionary<DateTime,Dictionary<Instrument,double>>();
			Dictionary<Instrument, BarSeries> historicalBarDict = new Dictionary<Instrument,BarSeries>();
			List<DateTime> dateList = new List<DateTime>();
		
			historicalBarDict.Add(instrument,myseries);
		
			myConsole.WriteLine(String.Format("Got historical bars for : {0} {1}",instrument, myseries.Count));
			foreach(var histBar in historicalBarDict){
				foreach(Bar bar in histBar.Value){
					bool isInDateList = false;
					
				//CHECK DUPLICATES
					foreach(DateTime date in dateList){
						if(date==bar.CloseDateTime){
							isInDateList=true;
						}
					}
					if(isInDateList==false){
						dateList.Add(bar.CloseDateTime);
						histDict.Add(bar.CloseDateTime,new Dictionary<Instrument, double>());
					}
					
				}
			}
			
			foreach(var histBar in historicalBarDict)
			{
				foreach(Bar bar in histBar.Value){
					histDict[bar.CloseDateTime].Add(histBar.Key,bar.Close);
				}
				
			}
			
			return histDict;
		
		}    


		public static Dictionary<DateTime,Dictionary<Instrument,double>> checkDatapoints (Dictionary<DateTime,Dictionary<Instrument,double>> input_dict ,
		 double illogicalThold, Dictionary<Instrument,double> askDict, Dictionary<Instrument,double> bidDict,Clock Clock)
		{	
			//clean the ib data
			List<DateTime> DelList = new List<DateTime>();
			foreach (DateTime keyd in input_dict.Keys ){
				foreach (Instrument key in input_dict[keyd].Keys ){
					double myval = input_dict[keyd][key];
					int check_int = checkVal(myval,illogicalThold,key,keyd,askDict,bidDict,Clock);
					if (check_int==0){
						DelList.Add(keyd);
					}		
				}
			}
			

			if (DelList.Count>0){
				foreach (DateTime deldate in DelList){
					input_dict.Remove(deldate);
				}
			}

			return  input_dict;
			
		}

		public static int checkVal (double myval,double thold, Instrument instrument,DateTime mydate, 
			Dictionary<Instrument,double> askDict, Dictionary<Instrument,double> bidDict,Clock Clock){
			//check the zeros and illogicla values in the dict
			
			if (mydate> Clock.DateTime){
				myConsole.WriteLine(String.Format("Forward Date Detected! Instrument: {0} DateTime: {1} Value {2} ",instrument,mydate,myval));
				return 0;
			}
			
			else if (myval<=0){
				myConsole.WriteLine(String.Format("Neg or Zero Value Detected! Instrument: {0} DateTime: {1} Value {2} ",instrument,mydate,myval));
				return 0;
			}
			else
			{
				double last_mid = (askDict[instrument]+bidDict[instrument])/2;
				double difference = Math.Abs(myval - last_mid)/myval;
				
				if (difference>thold)
				{
					myConsole.WriteLine(String.Format("Illogical Value Detected! Instrument: {0} DateTime: {1} Value {2} Last Mid {3} ",instrument,mydate,myval,last_mid));
					return 0; 
				}
				else
				{	
					return 1;
				}
			}				
		}
		public static List<List<double>>  alignedListGenerator (List<Dictionary<DateTime,Dictionary<Instrument,double>>> input_dict, List<Instrument> Instruments , bool verbose=false)
		{
			HashSet<DateTime> h1 = new HashSet<DateTime>();
			//Generate uniq keys

			foreach (var dicts  in input_dict){
				{
				h1.UnionWith(dicts.Keys);
			}
			
				}
			
			
			//now for each datetime, we create a classifier array and feature array and label list
			// this array list will go to numerification and regression.
			
			List<List<double>> glob_list = new List<List<double>>();
			foreach (DateTime k in h1)
			{
				//if all available, than append it to dictionary
				int counter=0;
				foreach (var dicts  in input_dict){
					if (dicts.ContainsKey(k)){
						counter++;
					}
				}

				if (counter==Instruments.Count){
					List<double> sub_list = new List<double>();
					foreach (Instrument ins in Instruments){
						foreach (var dicts  in input_dict){
							//check if key is exists, if not than skip this date for all instruments.
							if (dicts[k].ContainsKey(ins)){
								sub_list.Add(dicts[k][ins]);
							}
						}
						
						
					}
					
					glob_list.Add(sub_list);
					
				}
				
			}
			if (verbose == true){
			foreach (List<double> row in glob_list)
  			{
    			foreach (double element in row)
    			{
      			Console.Write(element.ToString() + " ");
			    }
    		Console.WriteLine();
  			}
			}



			return glob_list;

			
		}
	}
public static class DataOps
	{
	public static BarSeries getBarsForMe(Instrument instrument, int lookback_hours,StrategyManager StrategyManager,DataManager DataManager,Clock Clock )
		{
			DateTime historicalEnd = Clock.DateTime;
			DateTime start = Clock.DateTime.AddHours((lookback_hours*2)*-1).Date;

			if (StrategyManager.Mode == StrategyMode.Live)
			
			{
				
				return 	DataManager.GetHistoricalBars("IB",instrument, start, historicalEnd, BarType.Time, 60*60);
			}
			else 
			{

				return DataManager.GetHistoricalBars(instrument,start,historicalEnd,BarType.Time, 60*60);
				//return 	DataManager.GetHistoricalBars("IB",instrument, start, historicalEnd, BarType.Time, 60*60);


		
			}
		}



		public static int uniqCheck(Instrument instrument, Bar bar,Clock Clock,DataManager DataManager)
		{

			
			DateTime historicalEnd = Clock.DateTime;
			DateTime start = Clock.DateTime.AddHours((100*2)*-1).Date;
			BarSeries x = DataManager.GetHistoricalBars(instrument,start,historicalEnd,BarType.Time, 60*60);
			Bar lastBar =x[x.Count()-1];

			if (lastBar.DateTime==bar.DateTime)
			{
				return 1;
			}	
			else
			{
				return 0;
			}
		}
	}

	public static class TimeOperations
	{

		public static  int checkMarketTimeandPrice(Instrument instrument, double latest_bid, double latest_ask, DateTime currentTime,
		 int maxLookBack, DateTime firstNotice, Clock Clock)

		{
			// this function will be called by OnBid, OnAsk and OnBar 

			//10-Year U.S. Treasury Note Futures	PreOpenSunday 16:00	Sunday 17:00-16:00	PreOpenWeekDay 16:45	Weekday 17:00-16:00
			int checker=0;

			if (latest_bid<=0 || latest_ask<=0)
			{
				checker++;
				myConsole.WriteLine("Bid and Ask data is not healty");
			}
			
			TimeZoneInfo convertZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
			DateTime USTime = TimeZoneInfo.ConvertTime(Clock.DateTime.ToUniversalTime(), convertZone);
			DateTime nowTime = Clock.DateTime;
			TimeSpan span = nowTime.Subtract(currentTime);
			DateTime finalNotice = firstNotice.AddHours(maxLookBack*-1).Date;
			// if the time passes this Date we should not enter any positions and consider starting out new algo

			if (span.Minutes > 5){
				myConsole.WriteLine("No Updates for 5 Minutes");
				checker++;	
			}
			if ((USTime.DayOfWeek==DayOfWeek.Saturday))
			{
				myConsole.WriteLine("Market Is Closed DoW Condition");
				checker++;	
			}
			
			if ((USTime.DayOfWeek==DayOfWeek.Friday) && (USTime.Hour>=16 ))
			{
				myConsole.WriteLine("Market Is Closed Friday Condition");
				checker++;	
			}			
			
			if ((USTime.DayOfWeek==DayOfWeek.Sunday) && (USTime.Hour <17))
			{
				myConsole.WriteLine("Market Is Closed Sunday Condition");
				checker++;	
			}
			
			if (USTime.Hour >= 16 && USTime.Hour <17)
			{
				myConsole.WriteLine("Market Is Closed Weekday Close Condition");
				checker++;	
			}
			
			if (Clock.DateTime >= finalNotice)
			{
				myConsole.WriteLine("The Contract is Close to expire cannot enter new positions try to close the remaining positions and start a new algo ");
				checker++;	
			}

			if (checker>0){
				return 0;
			}

			else
			{
				return 1;
			}
		}
		public static List<Dictionary<int,double>> setTickConversionsLive(double comissionPerLot,Dictionary<int,double> tickConversion,
			Dictionary<int,double> costDict,Dictionary<int,double> logicalSpreadDict )
	    {
	    	/*
			INSTRUMENT CODES
			ZB 30
			ZF 31
			ZN 38
			ZT 39
			*/
			tickConversion = new Dictionary<int,double>();
			costDict = new Dictionary<int,double>() ;
			logicalSpreadDict = new Dictionary<int,double>() ;
	    	
	    	double tVal = 31.25; // (1/32)*31.25
			double comS = comissionPerLot*2;
			double increment = 0.03125;

	    	tickConversion[30]=tVal;
	    	tickConversion[31]=tVal;
	    	tickConversion[38]=tVal;
	    	tickConversion[39]=tVal*2;
	    	
	    	costDict[30]=comS + tVal;
	    	costDict[31]=comS + tVal/4;
	    	costDict[38]=comS + tVal/2;
	    	costDict[39]=comS + tVal/4;


	    	logicalSpreadDict[30]=(increment)*1.1*2;
	    	logicalSpreadDict[31]=(increment/4)*1.1*2;
	    	logicalSpreadDict[38]=(increment/2)*1.1*2;
	    	logicalSpreadDict[39]=(increment/8)*1.1*2;

	    	List<Dictionary<int,double>> resList = new List<Dictionary<int,double>>(); 		
	    	resList.Add(costDict);
	    	resList.Add(tickConversion);
	    	resList.Add(logicalSpreadDict);
	    	return resList;


	    }
	    public static List<Dictionary<int,double>> setTickConversionsBacktest(double comissionPerLot,Dictionary<int,double> tickConversion,
			Dictionary<int,double> costDict,Dictionary<int,double> logicalSpreadDict )
	    {
	    	/*
			INSTRUMENT CODES
			ZB 26
			ZF 27
			ZN 28
			ZT 29
			*/
			tickConversion = new Dictionary<int,double>();
			costDict = new Dictionary<int,double>() ;
			logicalSpreadDict = new Dictionary<int,double>() ;
	    	
	    	double tVal = 31.25; // (1/32)*31.25
			double comS = comissionPerLot*2;
			double increment = 0.03125;

	    	tickConversion[26]=tVal;
	    	tickConversion[27]=tVal;
	    	tickConversion[28]=tVal;
	    	tickConversion[29]=tVal*2;
	    	
	    	costDict[26]=comS + tVal;
	    	costDict[27]=comS + tVal/4;
	    	costDict[28]=comS + tVal/2;
	    	costDict[29]=comS + tVal/4;


	    	logicalSpreadDict[26]=(increment)*1.1*2;
	    	logicalSpreadDict[27]=(increment/4)*1.1*2;
	    	logicalSpreadDict[28]=(increment/2)*1.1*2;
	    	logicalSpreadDict[29]=(increment/4)*1.1*2;

	    	List<Dictionary<int,double>> resList = new List<Dictionary<int,double>>(); 		
	    	resList.Add(costDict);
	    	resList.Add(tickConversion);
	    	resList.Add(logicalSpreadDict);
	    	return resList;


	    }

		public static List<int> marketCheckLogic(int Caller, int checkerInt )
		{
				List<int> marketCheck = new List<int>(); 
				int marketAskInt=0;
				int marketBidInt=0;

				if (Caller==2 && checkerInt==0){
					
					marketBidInt=0;
				}
				else if (Caller==2 && checkerInt==1){
					marketBidInt=1;
				}
				else if (Caller==1 && checkerInt==0){
					marketAskInt=0;
				}
				else if (Caller==1 && checkerInt==1){
					marketAskInt=1;
				}
				else if (Caller==3 && checkerInt==0){
					marketAskInt=0;
					marketBidInt=0;
				}
				else if (Caller==3 && checkerInt==1){
					marketAskInt=1;
					marketBidInt=1;
				}
				marketCheck.Add(marketBidInt);
				marketCheck.Add(marketAskInt);
				return marketCheck;
		}

	}

	public static class CheckOperations
	{
		public   static List<Dictionary<Instrument,int>>  marketCheck(Instrument instrument, int Caller,Dictionary<Instrument,double> askDict,Dictionary<Instrument,double> bidDict,
			Dictionary<Instrument,int> marketAskDict, Dictionary<Instrument,int> marketBidDict,  List<int> LookBacks,DateTime  earliestFirstNotice,
			StrategyManager StrategyManager, Clock Clock )
		{
			List<Dictionary<Instrument,int>> responseList = new List<Dictionary<Instrument,int>> ();
			// checks the time and market 
			if (StrategyManager.Mode == StrategyMode.Live)
			{ 
			// CHECK THE BID AND ASK AND IF THEY ARE HEALTY REALTIME //TO Do ADD SCHEDULE FROM IB
			// ADD END TIME TO MAKE SURE STRATEGY BEFORE THE FIRST NOTICE TIME TO DO
				DateTime CurrentTime=Clock.DateTime;
				double latest_bid=askDict[instrument];
				double latest_ask=bidDict[instrument];
				int checkerInt = TimeOperations.checkMarketTimeandPrice(instrument,latest_bid,latest_ask,CurrentTime,LookBacks.Max(),earliestFirstNotice,Clock);
				List<int> marketCheck = TimeOperations.marketCheckLogic(Caller,checkerInt );
				marketBidDict[instrument]=marketCheck[0];
				marketAskDict[instrument]=marketCheck[1];
			}
			else
			{
				marketAskDict[instrument]=1;
				marketBidDict[instrument]=1;
			}	
			responseList.Add(marketAskDict);
			responseList.Add(marketBidDict);
			return responseList;
		}   
		
		public static int processMarketCheck(Instrument instrument,Dictionary<Instrument,double> askDict,Dictionary<Instrument,double> bidDict,
			Dictionary<Instrument,int> marketAskDict, Dictionary<Instrument,int> marketBidDict,  List<int> LookBacks,DateTime  earliestFirstNotice,
			StrategyManager StrategyManager, List<Instrument> InstrumentList,Clock Clock)
		{	
			// processes the market check called by onbar
			List<Dictionary<Instrument,int>> marketCheckList = CheckOperations.marketCheck(instrument,3,askDict,bidDict,marketAskDict, 
				 	marketBidDict, LookBacks,earliestFirstNotice,StrategyManager,Clock);
			marketAskDict = marketCheckList[0];
			marketBidDict = marketCheckList[1];
			int result = CheckOperations.marketChecker(marketBidDict, marketAskDict,InstrumentList);
			return result;
		}

		public static  int processCallCheck(Instrument instrument,Dictionary<Instrument,int> callCheck,List<Instrument> InstrumentList)
		{
			callCheck[instrument]=1;
			int checkSum = 0;
			foreach(var item in callCheck.Keys)
			{
				if (callCheck[item]==1){
					checkSum++;
				}
			  
			}

			
			if (checkSum==InstrumentList.Count()){
				resetCallCheck(instrument,callCheck,InstrumentList);
				return 1;
			}
			else
			{
				return 0;
			}

		} 
		
		public static void resetCallCheck(Instrument instrument,Dictionary<Instrument,int> callCheck,List<Instrument> InstrumentList)
		{
			//myConsole.WriteLine("Resetcalled" + DateTime.Now);
			foreach(Instrument ins in InstrumentList)
			{
				callCheck[ins]=0;
			}
			 
		} 

		public static int marketChecker( Dictionary<Instrument,int> marketBidDict, Dictionary<Instrument,int> marketAskDict,List<Instrument> InstrumentList)
		{

			int checkSumA = 0;
			int checkSumB = 0;


			foreach(var item in marketBidDict.Keys)
			{
				if (marketBidDict[item]==1){
					checkSumA++;
				}  
			}

			foreach(var item in marketAskDict.Keys)
				{
					if (marketAskDict[item]==1){
						checkSumB++;
					}			  
				}

			if ((checkSumB==InstrumentList.Count()) && (checkSumA==InstrumentList.Count()) ){
				return 1;
			}

			else{
				return 0;
			}			 
		} 

		public static int bidAskCheck(Dictionary<Instrument,DateTime> timeAskCheck,Dictionary<Instrument,DateTime> timeBidCheck,DateTime CurrentTimeBar)
		{	
			int checkSumA = 0;
			int checkSumB = 0;

			foreach(var item in timeAskCheck.Keys)
			{
				if (timeAskCheck[item]>CurrentTimeBar){
					checkSumA++;
				}
			}  

			foreach(var item in timeBidCheck.Keys)
			{
				if (timeBidCheck[item]>CurrentTimeBar){
					checkSumB++;
				}
			}  

			if ((checkSumA + checkSumB)==(timeBidCheck.Count+timeBidCheck.Count))
			{
				return 1;
			}
			else
			{
				return 0;
			}

		}
	}

	public static class myConsole
	{
	
		public static void WriteLine(string str)
		{
			bool debug=false;
			if (debug==true)
			{
				Console.WriteLine(str);
			}
		}

	}

	public static class spreadOps
	{
		public static int spreadChecker(Dictionary<Instrument,double> bidDict,Dictionary<Instrument,double> askDict,
			Dictionary<int,double> logicalSpreadDict,List<Instrument> instrumentCollection)
		{
			//checks if all spreads are in the logical interval before executing
			int spreadCheck = 0;
			foreach (Instrument instrument in instrumentCollection)
			{	
				int InsId = instrument.Id;
				double logicalSpread = logicalSpreadDict[InsId];
				double currentBid = bidDict[instrument];
				double currentAsk = askDict[instrument];
				double currentSpread = currentAsk - currentBid;
				if (currentSpread>logicalSpread)
				{
					myConsole.WriteLine("Spread Check Failed: " +  instrument);
					spreadCheck++;
				} 
			}

			if (spreadCheck>0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}

	}
    

}








