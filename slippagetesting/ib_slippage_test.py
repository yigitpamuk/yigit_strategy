from ib_insync import *
import numpy as np


ib = IB()
ib.connect('127.0.0.1', 7496, clientId=50)


cdata=Forex('EURUSD')
contract = CFD(currency="USD",symbol="EUR")
ib.qualifyContracts(contract)


ib.reqMktData(cdata, '', False, False)



ticker = ib.ticker(cdata)

i=0


global pos
pos=1
global c
c=0

import datetime
import pickle
import time


orderlist=[]
tradelist=[]

last=time.time()
def onPendingTickers(tickers):
    global pos
    global c
    global last
    for t in tickers:
        print('ORDERS',c)
        buyorder = MarketOrder('BUY', 100000)
        sellorder = MarketOrder('SELL', 100000)
        dif=time.time()-last
        if pos==1:
            if dif>10:
                print('Long')
                pos=-1
                c+=1
                ib.placeOrder(contract,buyorder)
                orderlist.append(['BUY',t.ask,datetime.datetime.now()])
                last=time.time()
        elif pos==-1:
            if dif>10:
                print('Short')
                pos=1
                c+=1
                ib.placeOrder(contract,sellorder)
                orderlist.append(['SELL',t.bid,datetime.datetime.now()])
                last=time.time()
        if c>300:
            ib.pendingTickersEvent -= onPendingTickers
            pickle.dump( orderlist, open( "orders_large_tws.p", "wb" ) )
            print("Pickle Dumped")
ib.pendingTickersEvent += onPendingTickers

ib.sleep(100000)

