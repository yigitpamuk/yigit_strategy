int lotsOpen() {
	string CurrentAsset = Asset; // Asset is changed in the for loop  
	int val = 0;  
	for(last_trades)    
	if(strstr(Asset,CurrentAsset) && TradeIsOpen)      
	val += TradeLots;  
	return val;
}
int lotsOpensign() {
	string CurrentAsset = Asset; // Asset is changed in the for loop  
	int val = 0;  
	for(open_trades)    
	if(strstr(Asset,CurrentAsset) && TradeIsOpen && TradeIsShort)      
	val -= TradeLots;  
	for(open_trades)  
	if(strstr(Asset,CurrentAsset) && TradeIsOpen && TradeIsLong)      
	val += TradeLots;
	return val;
}


function pair_strat(string p1_,string p2_,int lb_,int capital_,int revert_full,int status,int p1_q,int p2_q){
	int capital=capital_;
	int lb=lb_;
	int x_y_x=p1_q;
	int x_y_y=p2_q;
	int x_y_status=status;
	string asset_x = p1_; 
	string asset_y = p2_; 	
	
	asset(asset_x);
	vars x = series(priceClose());
	
	asset(asset_y);
	vars y = series(priceClose());
	
	vars spread=series(x[0]/y[0]);
	vars stdev=series(StdDev(spread,lb));
	vars mean=series(SMA(spread,lb));
	vars up_limit=series(mean[0]+stdev[0]);
	vars down_limit=series(mean[0]-stdev[0]);
	vars x_smooth=series(SMA(x,5));
	vars y_smooth=series(SMA(y,5));
	int x_quantity=round(capital/x_smooth[0]);
	int y_quantity=round(capital/y_smooth[0]);

	
	float spread_cur=spread[0];
	float up_cur=up_limit[0];
	float dd_cur=down_limit[0];
	float mean_cur=mean[0];
	
	int close=0;
	float close_long=mean_cur;
	float close_short =mean_cur;
	if (revert_full==1){
		close_long=up_cur;
		close_short=dd_cur;
	}
	
	
	if (x_y_status==0){
		if (spread_cur>up_cur){//s
			x_y_x=x_quantity*-1;
			x_y_y=y_quantity;
			x_y_status=-1;

		}
		else if (spread_cur<dd_cur){//asd
			x_y_x=x_quantity;
			x_y_y=y_quantity*-1;
			x_y_status=1;


		}
		
	}
	
	
	if (x_y_status==1){//das
		if (spread_cur>close_long){//das

			x_y_status=0;
			x_y_x=0;
			x_y_y=0;
			close=1;

		}
	}
	
	if (x_y_status==-1){//das
		if (spread_cur<close_short){//dsaa
			x_y_status=0;
			x_y_x=0;
			x_y_y=0;
			close = 1;

		}
	}
	int * result = (int*)malloc(sizeof(int)*3);
	result[0]=x_y_status;
	result[1]=x_y_x;
	result[2]=x_y_y;
	return result;
}



function vote_trade(string p1_,string p2_,int status_1_,int status_2_,int status_3_,int status_4_,int status_5_,int vote_p,int pq11,int pq12,int pq13,int pq14,int pq15,int pq21,int pq22,int pq23,int pq24,int pq25){

	
	int vote_perm=vote_p;
	
	string p1=p1_;
	string p2=p2_;

	
	int status_1=status_1_;
	int status_2=status_2_;
	int status_3=status_3_;
	int status_4=status_4_;
	int status_5=status_5_;
	
	int act_1=0;
	int act_2=0;
	int act_3=0;
	int act_4=0;
	int act_5=0;
	
	if (p1_=="EWA" && p2_=="EWC"){
		act_1=1;
		act_2=1;
		act_3=1;
		act_4=1;
		act_5=0;

		
	}
	if (p1_=="EZA" && p2_=="EEM"){
		act_1=1;
		act_2=1;
		act_3=1;
		act_4=1;
		act_5=0;
	}
	
	if (p1_=="EZA" && p2_=="EWA"){
		act_1=1;
		act_2=1;
		act_3=1;
		act_4=1;
		act_5=0;
	}
	
	if (p1_=="EZA" && p2_=="EWC"){
		act_1=1;
		act_2=1;
		act_3=1;
		act_4=1;
		act_5=0;
	}
	

	


	int temp_x_1=0;
	int temp_x_2=0;
	int temp_x_3=0;
	int temp_x_4=0;
	int temp_x_5=0;
	
	int temp_y_1=0;
	int temp_y_2=0;
	int temp_y_3=0;
	int temp_y_4=0;
	int temp_y_5=0;
	int vote=0;
	if (act_1==1){

		int lb=21*1;
		int capital=Capital/4.0;
		int revert_full=0;
		int * result = pair_strat(p1,p2,lb,capital,revert_full,status_1,pq11,pq21);
		vote+=result[0];
	   //printf("\n vote1 %d",result[0]);
		status_1=result[0];
		temp_x_1=result[1];
		temp_y_1=result[2];
		free(result);
	}

	if (act_2==1){
		int lb=21*3;
		int capital=Capital/4.0;
		int revert_full=0;
		int * result = pair_strat(p1,p2,lb,capital,revert_full,status_2,pq12,pq22);
		vote+=result[0];
	   //printf("\n vote2 %d",result[0]);
		status_2=result[0];
		temp_x_2=result[1];
		temp_y_2=result[2];
		free(result);
	}

	if (act_3==1){
		int lb=21*6;
		int capital=Capital/4.0;
		int revert_full=0;
		int * result = pair_strat(p1,p2,lb,capital,revert_full,status_3,pq13,pq23);
		vote+=result[0];
	   //printf("\n vote3 %d",result[0]);
		status_3=result[0];
		temp_x_3=result[1];
		temp_y_3=result[2];
		free(result);
	}

	if (act_4==1){
		int lb=21*12;
		int capital=Capital/4.0;
		int revert_full=0;
		int * result = pair_strat(p1,p2,lb,capital,revert_full,status_4,pq14,pq24);
		vote+=result[0];
	   //printf("\n vote4 %d",result[0]);
		status_4=result[0];
		temp_x_4=result[1];
		temp_y_4=result[2];
		free(result);
	}


	

	
	int POS_FX=temp_x_1+temp_x_2+temp_x_3+temp_x_4+temp_x_5;
	int POS_FY=temp_y_1+temp_y_2+temp_y_3+temp_y_4+temp_y_5;
	
	// //printf("Vote %d",vote);
	int * response = (int*)malloc(sizeof(int)*18);
	response[0]=status_1;
	response[1]=status_2;
	response[2]=status_3;
	response[3]=status_4;
	response[4]=status_5;
	response[5]=POS_FX ;
	response[6]=POS_FY;
	response[7]=vote_perm;
	response[8]=temp_x_1;
	response[9]=temp_x_2;
	response[10]=temp_x_3;
	response[11]=temp_x_4;
	response[12]=temp_x_5;
	response[13]=temp_y_1;
	response[14]=temp_y_2;
	response[15]=temp_y_3;
	response[16]=temp_y_4;
	response[17]=temp_y_5;
	return response;
}



int status_1_1=0;
int status_1_2=0;
int status_1_3=0; 
int status_1_4=0; 
int status_1_5=0; 
int P1_1=0;
int P1_2=0;
int vote_1=0;

int status_2_1=0;
int status_2_2=0;
int status_2_3=0; 
int status_2_4=0; 
int status_2_5=0; 

int vote_2=0;

int status_3_1=0;
int status_3_2=0;
int status_3_3=0; 
int status_3_4=0; 
int status_3_5=0; 

int vote_3=0;

int status_4_1=0;
int status_4_2=0;
int status_4_3=0; 
int status_4_4=0; 
int status_4_5=0; 

int vote_4=0;


int status_5_1=0;
int status_5_2=0;
int status_5_3=0; 
int status_5_4=0; 
int status_5_5=0; 

int vote_5=0;


int P1_11=0;
int P1_21=0;
int P1_31=0;
int P1_41=0;
int P1_51=0;
int P1_12=0;
int P1_22=0;
int P1_32=0;
int P1_42=0;
int P1_52=0;

int P2_11=0;
int P2_21=0;
int P2_31=0;
int P2_41=0;
int P2_51=0;
int P2_12=0;
int P2_22=0;
int P2_32=0;
int P2_42=0;
int P2_52=0;

int P3_11=0;
int P3_21=0;
int P3_31=0;
int P3_41=0;
int P3_51=0;
int P3_12=0;
int P3_22=0;
int P3_32=0;
int P3_42=0;
int P3_52=0;

int P4_11=0;
int P4_21=0;
int P4_31=0;
int P4_41=0;
int P4_51=0;
int P4_12=0;
int P4_22=0;
int P4_32=0;
int P4_42=0;
int P4_52=0;

int P5_11=0;
int P5_21=0;
int P5_31=0;
int P5_41=0;
int P5_51=0;
int P5_12=0;
int P5_22=0;
int P5_32=0;
int P5_42=0;
int P5_52=0;

function run() {
	set(LOGFILE|PARAMETERS);
	BarPeriod = 1440;
	BarZone=ET;
	BarOffset=940;
	Capital=10000;
	StartDate=2010;
	TradesPerBar = 10;
	PlotWidth = 650;
	LookBack=260;
	assetList("AssetsIB.csv");

	StartDate = 2006; 
	EndDate = 2019; 
	set(NFA);
	int P1_1=0;
	int P1_2=0;
	int P2_1=0;
	int P2_2=0;
	int P3_1=0;
	int P3_2=0;
	int P4_1=0;
	int P4_2=0;
	int P5_1=0;
	int P5_2=0;
	
	
	string p1="EWA";
	string p2="EWC";
	int * response = vote_trade(p1,p2,status_1_1,status_1_2,status_1_3,status_1_4,status_1_5,vote_1,P1_11,P1_21,P1_31,P1_41,P1_51,P1_12,P1_22,P1_32,P1_42,P1_52);
	status_1_1=response[0];
	status_1_2=response[1];
	status_1_3=response[2];
	status_1_4=response[3];
	status_1_5=response[4];
	P1_1=response[5] ;
	P1_2=response[6];
	P1_11=response[8];
	P1_21=response[9];
	P1_31=response[10];
	P1_41=response[11];
	P1_51=response[12];
	P1_12=response[13];
	P1_22=response[14];
	P1_32=response[15];
	P1_42=response[16];
	P1_52=response[17];
	vote_1=response[7];
	free(response);
	
	string p1="EZA";
	string p2="EEM";
	int * response = vote_trade(p1,p2,status_2_1,status_2_2,status_2_3,status_2_4,status_2_5,vote_2,P2_11,P2_21,P2_31,P2_41,P2_51,P2_12,P2_22,P2_32,P2_42,P2_52);
	status_2_1=response[0];
	status_2_2=response[1];
	status_2_3=response[2];
	status_2_4=response[3];
	status_2_5=response[4];
	P2_1=response[5] ;
	P2_2=response[6];
	P2_11=response[8];
	P2_21=response[9];
	P2_31=response[10];
	P2_41=response[11];
	P2_51=response[12];
	P2_12=response[13];
	P2_22=response[14];
	P2_32=response[15];
	P2_42=response[16];
	P2_52=response[17];
	vote_2=response[7];
	free(response);
	
	string p1="EZA";
	string p2="EWA";
	int * response = vote_trade(p1,p2,status_3_1,status_3_2,status_3_3,status_3_4,status_3_5,vote_3,P3_11,P3_21,P3_31,P3_41,P3_51,P3_12,P3_22,P3_32,P3_42,P3_52);
	status_3_1=response[0];
	status_3_2=response[1];
	status_3_3=response[2];
	status_3_4=response[3];
	status_3_5=response[4];
	P3_1=response[5] ;
	P3_2=response[6];
	P3_11=response[8];
	P3_21=response[9];
	P3_31=response[10];
	P3_41=response[11];
	P3_51=response[12];
	P3_12=response[13];
	P3_22=response[14];
	P3_32=response[15];
	P3_42=response[16];
	P3_52=response[17];
	vote_3=response[7];
	free(response);
	
	string p1="EZA";
	string p2="EWC";
	int * response = vote_trade(p1,p2,status_4_1,status_4_2,status_4_3,status_4_4,status_4_5,vote_4,P4_11,P4_21,P4_31,P4_41,P4_51,P4_12,P4_22,P4_32,P4_42,P4_52);
	status_4_1=response[0];
	status_4_2=response[1];
	status_4_3=response[2];
	status_4_4=response[3];
	status_4_5=response[4];
	P4_1=response[5] ;
	P4_2=response[6];
	P4_11=response[8];
	P4_21=response[9];
	P4_31=response[10];
	P4_41=response[11];
	P4_51=response[12];
	P4_12=response[13];
	P4_22=response[14];
	P4_32=response[15];
	P4_42=response[16];
	P4_52=response[17];
	vote_4=response[7];
	free(response);
	
	

	
	

	
	

	int INS1 =P3_2+P1_1; // EWA
	int INS2 =P4_2+P1_2; // EWC 
	int INS3 =P2_1+P3_1+P4_1; // EZA
	int INS4 =P2_2; // EEM

	printf("\n ----------------------------------------------------");
	printf("\n NET TARGET POSITIONS");
	printf("\n EWA %d EWC %d EZA %d EEM %d",INS1,INS2,INS3,INS4);
	printf("\n *****");
	printf("\n EWA components: P1: %d P3: %d",P1_1,P3_2);
	printf("\n P1 EWA components:");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P1_11,P1_21,P1_31,P1_41,P1_51);
	printf("\n P3 EWA components: ");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P3_12,P3_22,P3_32,P3_42,P3_52);
	printf("\n *****");
	printf("\n EWC components: P1: %d P4: %d",P1_2,P4_2);
	printf("\n P1 EWC components:");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P1_12,P1_22,P1_32,P1_42,P1_52);
	printf("\n P4 EWC components: ");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P4_12,P4_22,P4_32,P4_42,P4_52);
	
	printf("\n *****");
	printf("\n EZA components: P2: %d P3: %d P4 %d",P2_1,P3_1,P4_1);
	printf("\n P2 EZA components:");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P2_11,P2_21,P2_31,P2_41,P2_51);
	printf("\n P3 EZA components: ");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P3_11,P3_21,P3_31,P3_41,P3_51);
	printf("\n P4 EZA components: ");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P4_11,P4_21,P4_31,P4_41,P4_51);
	printf("\n *****");
	printf("\n EEM components: P2: %d ",P2_2);
	printf("\n P2 EEM components: ");
	printf("\n LB1m: %d  LB3m: %d LB6m: %d LB12m: %d",P2_12,P2_22,P2_32,P2_42,P2_52);
	printf("\n *****");

	double net_exposure=0;
	// TRADE LOOP
	//int time = hour()*100+minute();
	//printf("\n TIME %d ",time);
	//if (time==1400){
	while(loop("EWA","EWC","EZA","EEM")){ 
	asset(Loop1);
	//Margin = 0.5 * OptimalF * Capital;
	//printf("Margin %f",Margin);
	int targetLots_x=0;
	if (Loop1=="EWA"){
		targetLots_x=INS1;
	}
	if (Loop1=="EWC"){
		targetLots_x=INS2;
	}
	
	if (Loop1=="EZA"){
		targetLots_x=INS3;
	}
	
	if (Loop1=="EEM"){
		targetLots_x=INS4;
	}

	

		
	printf("\n ||||----------------|||");
	printf("\n %s",Loop1);
	printf("\n ORDER LOOP START");
	int lotsOpen_x;
	if (targetLots_x > 0) { //want to be long asset_x
		printf("\n LONG TARGET");
		lotsOpen_x = lotsOpen();
		printf("\n Before Close  Target %d Open %d ",targetLots_x,lotsOpen_x);
		exitShort();
		printf("\n SHORTS EXITED");
		lotsOpen_x = lotsOpen();	
		printf("\n After Close Target %d Open %d ",targetLots_x,lotsOpen_x);
		if (lotsOpen_x < targetLots_x) { //\need to buy more x
			printf("\n BUY MORE");
			Lots = targetLots_x - lotsOpen_x;
			printf("\n Lots to Enter %d",targetLots_x - lotsOpen_x);
			enterLong();
			printf("\n LONGS ENTERED");
		}
		else if (lotsOpen_x > targetLots_x){
			printf("\n WE HAVE TO EXIT SOME ");
			exitLong(0,0,(lotsOpen_x-targetLots_x));
			printf("\n LONGS EXITED");
		}
	}
	else if (targetLots_x < 0) { //want to be short asset_x
		printf("\n SHORT TARGET");
		printf("\n Before Close  Target %d Open %d ",targetLots_x,lotsOpen_x);
		exitLong();
		printf("\n LONGS EXITED");
		printf("\n After Close Target %d Open %d ",targetLots_x,lotsOpen_x);
		lotsOpen_x = lotsOpen();
		if (lotsOpen_x < abs(targetLots_x)) { //\need to sell more x
			printf("\n SELL MORE");
			Lots = abs(targetLots_x) - lotsOpen_x;
			printf("\n Lots to Sell %d",abs(targetLots_x) - lotsOpen_x);
			enterShort();
			printf("\n SHORTS ENTERED");
		}
		else if (lotsOpen_x > abs(targetLots_x)) {
			printf("\n WE HAVE TO EXIT SOME ");
			exitShort(0,0,(lotsOpen_x - abs(targetLots_x)));
			printf("\n SHORTS EXITED");
		}
	}
	else if (targetLots_x == 0) { //want to be short asset_x
		exitLong();
		exitShort();
		printf("\n EXIT ALL");
	}
	vars zz = series(priceClose());
	double current_price=zz[0];
	double opens=lotsOpensign()/1.;
	double exposure=zz[0]*opens;

	
	
	
	net_exposure+=exposure;
	
	//}

	}
	
	printf("\n Net Exposure: %.6f",net_exposure);
	printf("\n ----------------------------------------------------");
	
	
}



