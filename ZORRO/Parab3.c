/*#define ASSET2 "USD/CHF"//20162018dukascopy"*/
#define ASSET1 "EUR/USDr"//20162018dukascopy"
#define ASSET2 "GBP/USDr"//20162018dukascopy"
/*#define ASSET1 "USD/JPY"//20162018dukascopy"*/
/*#define ASSET1 "XAU/USD"//20162018dukascopy"*/
/*#define ASSET2 "XAG/USD"//20162018dukascopy"*/
var counter = 0;
var resid_entry = 0;
string fname;
var isopen = 0;
var open_pnl = 0;
//------------------------------------------------------------------------------------------
//m[0] returns the hedge ratio.
var linreg(var n){
    var i;
    var x,y;
    var sumx,sumy,sumxsq,sumxy,denom,a0,a1;
    var a0,a1;
    for(i=0;i<n;i++)
    {
        asset(ASSET1);
        x = 1.0/priceClose(i);
        asset(ASSET2);
        y = priceClose(i);
        /*printf("\n%6.2f,%6.2f",x,y);*/
        sumx += x;
        sumxsq += x * x;
        sumy += y;
        sumxy += x * y;
    }

    denom=n*sumxsq-(sumx*sumx);
    a0 = (sumy * sumxsq - sumx * sumxy) / denom;
    a1 = (n * sumxy - sumx * sumy) / denom;
    var m[2];
    m[0] = a1;
    m[1] = a0;
        // printf("\n%6.2f,%6.2f",m[0],m[1]);
    return m[0];
    }
//-----------------------------------------------------------------------------------------------
//m[1] returns the intercept for the spread. The python backtest included the intercept in the stationary spread.
var linreg_m1(var n){
    var i;
    var x,y;
    var sumx,sumy,sumxsq,sumxy,denom,a0,a1;
    var a0,a1;
    for(i=0;i<n;i++)
    {
        asset(ASSET1);
        x = 1.0/priceClose(i);
        asset(ASSET2);
        y = priceClose(i);
        /*printf("*******> x and y: \n%6.2f,%6.2f",x,y);*/
        sumx += x;
        sumxsq += x * x;
        sumy += y;
        sumxy += x * y;
    }

    denom=n*sumxsq-(sumx*sumx);
    a0 = (sumy * sumxsq - sumx * sumxy) / denom;
    a1 = (n * sumxy - sumx * sumy) / denom;
    var m[2];
    m[0] = a1;
    m[1] = a0;
    return m[1];
    }

//------------------------------------------------------------------------------------------
float make_resid(var m, var m1){//, var m1){
        var x,y,residreturn;
        asset(ASSET1);
                  x = 1.0/priceClose(0);
        asset(ASSET2);
                  y = priceClose(0);

                  residreturn = m*x - y + m1;
                  return residreturn;
}

//------------------------------------------------------------------------------------------
var lotsOpen(){
    string CurrentAsset = Asset;
    var val = 0;
    for (open_trades)
        if(strstr(Asset,CurrentAsset) && TradeIsOpen)
            val += TradeLots;
    return val;
}


//------------------------------------------------------------------------------------------
var myProfit(){
    var profit=0.;
    for (open_trades){
            profit += TradeProfit;
    }
    if (profit>0) {
        return 1;
    }
    else {
        return -1;
    }
}

//------------------------------------------------------------------------------------------
var print_date(){
    printf("\n%02d-%02d-%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),(int)second());
}

var allOpenLots(){
    var openLots = 0;
    asset(ASSET1);
    openLots+=lotsOpen();
    asset(ASSET2);
    openLots+=lotsOpen();
    return openLots;
}

var my_std(var *n, int N){
    var i;
    var k;
    var mean_n;
    var std;
    for(i=0;i<N;i++){
        /*printf("%.7f ",n[i]);*/
        k += n[i];
    }
    mean_n = k/N;

    var ss;
    for(i=0;i<N;i++){
        ss += (n[i] - mean_n)*(n[i]-mean_n);
    }

    std = sqrt(ss/(N-1));

    return std;
}


var my_mean(var *n, int N){
    var i;
    var k;
    var mean_n;
    for(i=0;i<N;i++){
        k += n[i];
    }
    mean_n = k/N;
    return mean_n;
}

//------------------------------------------------------------------------------------------

function strat3()
{
    //Initialising platform paramters.
    Weekend = 3;
    StartDate = 20160104;
    EndDate = 20181206;
    LookBack = 501;
    BarPeriod = 60;
    Commission = 0;
    Slippage = 0;
    Spread = 0.00006;
    TradesPerBar = 2;

    set(TESTNOW+LOGFILE);
    set(PLOTNOW);

    ////Strategy Parameters:
    var HRlookback = 200;
    var BBLKBK = 200;
    var BWLength = 1.5;

    ////== Strategy Variables

    //Residual calculation
    var m = linreg(HRlookback)*1.0;
    var m1 = linreg_m1(HRlookback)*1.0;
    var Residual = make_resid(m,m1);
    asset(ASSET1);
    var x = 1.0/priceClose(0);
    asset(ASSET2);
    var y = priceClose(0);
    vars AdjResid2 = series(m*x-y+m1);
    var Equation = m*x - y + m1;
    var mx = m * x;
    var b;
	var llots = 1000.0;

    //Bollinger bands
    vars BB_Std = series(my_std(AdjResid2,BBLKBK));
    vars BB_mean = series(my_mean(AdjResid2,BBLKBK));
    vars BB_up = series(BB_mean[0] + BWLength*BB_Std[0]);
    vars BB_down = series(BB_mean[0] - BWLength*BB_Std[0]);

    //Moving average delta calculation. (Risk off)
    vars MA_Delta = series(abs(BB_mean[0]-BB_mean[1])*100);
    var MA_thresh = 0.005;

    //Visualising variables.
    plot("Residual",AdjResid2[0],NEW,RED);
    plot("Bollinger1",BB_up[0],LINE,BLUE);
    plot("Bollinger2",BB_down[0],LINE,BLUE);
    plot("BBmean",BB_mean[0],LINE,BLACK);
    plot("MA_DELTA",abs((BB_mean[0]-BB_mean[1])*100),NEW,BLUE);
    plot("BB_Std",BB_Std[0],NEW,RED);
    plot("USDCHF",x,NEW,GREEN);
    plot("EURUSD",y,LINE,BLUE);
    plot("HedgeRatio",m,NEW,BLUE);
    PlotWidth = 1000;
    PlotHeight1 = 500;
    PlotHeight2 = 300;

    string debug = strf("\n ---> parab3: %d-%d-%d %d:%d d:%.5f | m:%.5f | u:%.5f | r:%.5f | m0:%.5f | m1:%.5f | sd:%.8f | l:%d s:%d | p1:%.5f p2:%.5f",year(),month(),day(),hour(),minute(),BB_down[0],BB_mean[0],BB_up[0],AdjResid2[0],m,m1,BB_Std[0],TradeIsOpen*TradeIsLong,TradeIsShort*TradeIsOpen,x,y);

    string fname = strf("Log/results_paper.dat");
    if (counter==0){file_delete(fname);}
    file_append(fname,debug,0);
    counter += 1;

//==========================Trade logic is formulated here:  ================================//

    if (AdjResid2[0] > BB_up[0] && isopen==0 && (0 < hour() <= 23) && MA_Delta[0]<MA_thresh){
    		 asset(ASSET1);
    		 Lots = llots*abs(x/y);
    		 enterShort();
    		 asset(ASSET2);
    		 Lots = llots;
    		 enterShort();
    		 printf("\nEnter Long");
    		 isopen=-1;
             open_pnl = ProfitTotal;
    }
    else if (((AdjResid2[0] < BB_mean[0]) && (isopen==-1))){
    	asset(ASSET1);
    	exitShort();

    	asset(ASSET2);
    	exitShort();
    	printf("\nExit Long %.6f");
    	isopen=0;
    }
    if (AdjResid2[0] < BB_down[0] && isopen==0 && (0 < hour() <=23) && MA_Delta[0]<MA_thresh){
    		 asset(ASSET2);
    		 Lots = llots;
    		 enterLong();
    		 asset(ASSET1);
    		 Lots = llots*abs(x/y);
    		 enterLong();
    		 printf("\nEnter Short");
    		 isopen=1;
             open_pnl = ProfitTotal;
    }
    else if (((AdjResid2[0] > BB_mean[0]) && (isopen==1))){
    		 asset(ASSET2);
    		 exitLong();
    		 asset(ASSET1);
    		 exitLong();
    		 printf("\nExit Short");
    		 isopen=0;
    }
}
function run() {
    strat3();
}

