int LOTS=1;



var my_mean(var *n, int N){
    var i;
    var k;
    var mean_n;
    for(i=0;i<N;i++){
        k += n[i];
    }
    mean_n = k/N;
    return mean_n;
}

var my_std(var *n, int N){
    var i;
    var k;
    var mean_n;
    var std;
    for(i=0;i<N;i++){
        /*printf("%.7f ",n[i]);*/
        k += n[i];
    }
    mean_n = k/N;

    var ss;
    for(i=0;i<N;i++){
        ss += (n[i] - mean_n)*(n[i]-mean_n);
    }

    std = sqrt(ss/(N-1));

    return std;
}
var correlation(var *arrX, var *arrY, var length)
  {
   var i;
   double meanX,meanY,stdX,stdY,numerator,ccoeff;
   meanX = 0;
   meanY = 0;
   stdX= 0;
   stdY= 0;
   numerator=0;

   meanX = my_mean(arrX,length);
   meanY = my_mean(arrY,length);

   for(i=0; i<length; i++)
     {
      numerator+=(arrX[i]-meanX)*(arrY[i]-meanY);
     }

   stdX = my_std(arrX,length);
   stdY = my_std(arrY,length);
   ccoeff=numerator/(stdX*stdY*(length-1));

   return(ccoeff);
   }
float dickey_fuller(var *arr, double sig, int length)
//http://pl.scribd.com/doc/80877200/How-to-do-a-Dickey-Fuller-Test-using-Excel
// n=25     50     100    250    500    >500
// {-2.62, -2.60, -2.58, -2.57, -2.57, -2.57};    //10% significance

// {-3.33, -3.22, -3.17, -3.12, -3.12, -3.12};    //5% significance

// {-3.75, -3.58, -3.51, -3.43, -3.43, -3.43};    //2% significance
  {
   int i;
   double cVal;
   bool null_status;
   int n=length;
   double tValue;
   double corrCoeff;
   /*var copyArr[length];*/
   var difference[1000];
   /*ArrayResize(difference,n-1);*/
   double cVals[4];
//---
   for(i=0; i<n-1; i++)
     {
      difference[i]=arr[i+1]-arr[i];
     }
//---
   var *copyArr = arr;
   corrCoeff=correlation(copyArr,difference,length);
   tValue=corrCoeff*sqrt((n-2)/(1-pow(corrCoeff,2)));

   if(sig==10)
      {
       cVals[0] = -2.62;
       cVals[1] = -2.6;
       cVals[2] = -2.58;
       cVals[3] = -2.57;
      }
   else
      {
       if(sig==5)
         {
          cVals[0] = -3.33;
          cVals[1] = -3.22;
          cVals[2] = -3.17;
          cVals[3] = -3.12;
         }
      else
         {
         if(sig==2)
            {
             cVals[0] = -3.75;
             cVals[1] = -3.58;
             cVals[2] = -3.51;
             cVals[3] = -3.43;
            }
         }
       }

   if(n<25)
     {
      cVal=cVals[0];
        }else{
      if(n>=25 && n<50)
        {
         cVal=cVals[1];
           }else{
         if(n>=50 && n<100)
           {
            cVal=cVals[2];
              }else{
            cVal=cVals[3];
           }
        }
     }

   null_status=tValue<cVal;
   return(null_status);       //Returns true if it is not stationary, false implies cointegration
  }


int lotsOpen() {
	string CurrentAsset = Asset;
	int val = 0;
	for(last_trades)
	if(strstr(Asset,CurrentAsset) && TradeIsOpen)
	val += TradeLots;
	return val;
}

function algo_generator(int BBLKBK, double BWLength)
{
    //Initialising platform paramters.


    ////Strategy Parameters:

    ////== Strategy Variables

    //Residual calculation


    vars y = series(priceClose());


    //Bollinger bands
    vars BB_Std = series(my_std(y,BBLKBK));
    vars BB_mean = series(my_mean(y,BBLKBK));
	 vars small = series(priceClose(),5);
    vars BB_up = series(BB_mean[0] + BWLength*BB_Std[0]);
    vars BB_down = series(BB_mean[0] - BWLength*BB_Std[0]);
    vars BB_up_limit = series(BB_mean[0] + BWLength*BB_Std[0]*2);
    vars BB_down_limit = series(BB_mean[0] - BWLength*BB_Std[0]*2);
    //Moving average delta calculation. (Risk off)
    vars MA_Delta = series(abs(BB_mean[0]-BB_mean[1])*100);
    var MA_thresh = 0.005;
	 
	 //hurst exponent
	 
	 double hurst_ratio = Hurst(y,BBLKBK);
	 double hurst_thresh= 0.4;
	 
	 //adf
	 
	 int adf = dickey_fuller(y,5,BBLKBK);

	 
	 
    //Visualising variables.
	 /*
    plot("Residual",y[0],NEW,RED);
    plot("Bollinger1",BB_up[0],LINE,BLUE);
    plot("Bollinger2",BB_down[0],LINE,BLUE);
    plot("BBmean",BB_mean[0],LINE,BLACK);
    plot("MA_DELTA",abs((BB_mean[0]-BB_mean[1])*100),NEW,BLUE);
    plot("BB_Std",BB_Std[0],NEW,RED);
    plot("USDCHF",x,NEW,GREEN);
    plot("EURUSD",y,LINE,BLUE);
    plot("HedgeRatio",m,NEW,BLUE);
    PlotWidth = 1000;
    PlotHeight1 = 500;
    PlotHeight2 = 300;
*/

//==========================Trade logic is formulated here:  ================================//

    if (y[0] > BB_up[0] && NumOpenLong==0 && NumOpenShort==0 && adf==0 && hurst_ratio<hurst_thresh && MA_Delta[0]<MA_thresh){

		Lots=LOTS;
    	enterShort();
    	//printf("\nEnter Short");
    		 
    }
    else if ((y[0] < BB_mean[0]) && NumOpenShort>0){

    	exitShort();
    	//printf("\nExit Short %.6f");
    	
    }
	 

	  
    if (y[0] < BB_down[0]  && NumOpenLong==0 && NumOpenShort==0 && adf==0   && hurst_ratio<hurst_thresh && MA_Delta[0]<MA_thresh){

		Lots=LOTS;
		enterLong();
		//printf("\nEnter Long");
    	
    }
    else if ((y[0] > BB_mean[0]) && NumOpenLong>0){
    		 
		exitLong();

		//printf("\nExit Long");
    		 
    }

	 
}




function run() {
	set(PARAMETERS+LOGFILE);
    Weekend = 3;
    LookBack = 2*5*12*2;
    BarPeriod = 1440;
	 BarOffset=0;	
	 assetList("AssetsOANDA");

	 set(PRELOAD);
    set(TESTNOW+LOGFILE);
    set(PLOTNOW);
	

	PlotWidth = 750;
	
	assetList("AssetsOANDA");

	while(asset(loop("NZD/CAD","AUD/NZD","GBP/AUD","NZD/USD","AUD/CAD","CAD/CHF","NZD/CHF","GBP/CHF","USD/CHF","NZD/JPY","CHF/JPY","USD/JPY","GBP/JPY","CAD/JPY")))
	while(algo(loop("1WMA","2WMA","4WMA","8WMA","12WMA","1WMB","2WMB","4WMB","8WMB","12WMB","1WMC","2WMC","4WMC","8WMC","12WMC","1WMD","2WMD","4WMD","8WMD","12WMD")))
	{


	if(Algo == "12WMA"){
	algo_generator(2*5*12,0.5);
	}
	else if(Algo == "8WMA"){
	algo_generator(2*5*8,0.5);
	}
	else if(Algo == "4WMA"){
	algo_generator(2*5*4,0.5);
	}
	else if (Algo == "3WMA"){
	algo_generator(2*5*3,0.5);
	}
	else if (Algo == "2WMA"){
	algo_generator(2*5*2,0.5);
	}
	else if (Algo == "1WMA"){
	algo_generator(2*5*1,0.5);
	}
	
	else if(Algo == "12WMB"){
	algo_generator(2*5*12,1);
	}
	else if(Algo == "8WMB"){
	algo_generator(2*5*8,1);
	}
	else if(Algo == "4WMB"){
	algo_generator(2*5*4,1);
	}
	else if (Algo == "3WMB"){
	algo_generator(2*5*3,1);
	}
	else if (Algo == "2WMB"){
	algo_generator(2*5*2,1);
	}
	else if (Algo == "1WMB"){
	algo_generator(2*5*1,1);
	}	

	else if(Algo == "12WMC"){
	algo_generator(2*5*12,1.5);
	}
	else if(Algo == "8WMC"){
	algo_generator(2*5*8,1.5);
	}
	else if(Algo == "4WMC"){
	algo_generator(2*5*4,1.5);
	}
	else if (Algo == "3WMC"){
	algo_generator(2*5*3,1.5);
	}
	else if (Algo == "2WMC"){
	algo_generator(2*5*2,1.5);
	}
	else if (Algo == "1WMC"){
	algo_generator(2*5*1,1.5);
	}	
	
	else if(Algo == "12WMD"){
	algo_generator(2*5*12,2);
	}
	else if(Algo == "8WMD"){
	algo_generator(2*5*8,2);
	}
	else if(Algo == "4WMD"){
	algo_generator(2*5*4,2);
	}
	else if (Algo == "3WMD"){
	algo_generator(2*5*3,2);
	}
	else if (Algo == "2WMD"){
	algo_generator(2*5*2,2);
	}
	else if (Algo == "1WMD"){
	algo_generator(2*5*1,2);
	}	
			
	
	}
}