

int LIMIT = 3;


int AUDNZDs=0;
int AUDCADs=0;
int GBPUSDs=0;
int NZDUSDs=0;
int USDCHFs=0;



int lotsOpen() {
	string CurrentAsset = Asset;
	int val = 0;
	for(last_trades)
	if(strstr(Asset,CurrentAsset) && TradeIsOpen)
	val += TradeLots;
	return val;
}

function algo_generator(int lookbacks){
	
	vars Close = series(priceClose());		
	int halfLife = lookbacks;
	vars zScore = series(((Close[0] - SMA(Close, halfLife))/StdDev(Close, halfLife))); 
	
	int openLots;
	int tarz=zScore[0];
	if (zScore[0]>LIMIT){
	tarz=LIMIT;
	}
	else if (zScore[0]<-1*LIMIT){
	tarz=-1*LIMIT;
	}
	
	var LOTS = 1;
	tarz=tarz*LOTS;
	
	
	

	printf("\nALGO LB: %d ASSET: %s TARGET POSITION %d",lookbacks,Asset,tarz*-1);
	printf("\n first price: %f",Close[0]);
	printf("\n zScore: %f",zScore[0]);
	printf("\n last price: %f",Close[halfLife]);
	printf("\n last price-1: %f",Close[halfLife-1]);
	printf("\n last price-2: %f",Close[halfLife-2]);
	printf("\n last price-2: %f",Close[halfLife-3]);
	
	
	if(Asset == "AUD/NZD"){
	AUDNZDs=AUDNZDs+tarz*-1;
	}	
	
	if(Asset == "AUD/CAD"){
	AUDCADs=AUDCADs+tarz*-1;
	}	
	
	if(Asset == "GBP/USD"){
	GBPUSDs=GBPUSDs+tarz*-1;
	}

	if(Asset == "NZD/USD"){
	NZDUSDs=NZDUSDs+tarz*-1;
	}	

	if(Asset == "USD/CHF"){
	USDCHFs=USDCHFs+tarz*-1;
	}		

	
}


function run() {
	set(PARAMETERS+LOGFILE);
	BarPeriod = 1440;
	StartDate=2000;
	Weekend = 3;
	NumCores = -2;		
	BarOffset=0;

	AUDNZDs=0;
	AUDCADs=0;
	GBPUSDs=0;
	NZDUSDs=0;
	USDCHFs=0;
	assetList("AssetsOANDA");
	set(PRELOAD);

	
	printf("\n**************************************************************************************************************************************");
	printf("\nNEW DAY START");
	printf("\n##### ALGO SPECIFIC TARGET POSITIONS #####");
	
	PlotWidth = 750;
	
	LookBack = 21*12-5;

	while(asset(loop("AUD/NZD","AUD/CAD","GBP/USD","NZD/USD","USD/CHF")))
		
	
	

	while(loop("12M","6M"))
	{
		

	
	if(Loop2 == "6M"){
	algo_generator(21*6);
	}		
	
	if(Loop2== "12M"){
	algo_generator(21*12-5);
	}		
	
		
				
	}
	

	printf("\n##### GLOBAL TARGET POSITIONS #####");
	printf("\nTARGET AUDNZDr pos: %d",AUDNZDs);
	printf("\nTARGET AUDCADr pos: %d",AUDCADs);
	printf("\nTARGET GBPUSDr pos: %d",GBPUSDs);
	printf("\nTARGET NZDUSDr pos: %d",NZDUSDs);
	printf("\nTARGET USDCHFr pos: %d",USDCHFs);

	while(loop("AUD/NZD","AUD/CAD","GBP/USD","NZD/USD","USD/CHF"))
	{
	int openLots;
	int target=0;
	asset(Loop1);
	
	if (Loop1=="AUD/NZD"){
	target=AUDNZDs;
	}
	if (Loop1=="AUD/CAD"){
	target=AUDCADs;
	}
	if (Loop1=="GBP/USD"){
	target=GBPUSDs;
	}
	if (Loop1=="NZD/USD"){
	target=NZDUSDs;
	}
	if (Loop1=="USD/CHF"){
	target=USDCHFs;
	}
	target=target*-1;
	
	
	if (target > 0) { 
		exitLong();
		openLots = lotsOpen();
		if (openLots <target) {
			Lots = target - openLots;
			enterShort();
		}
		else if (openLots > target) {
			exitShort(0,0,(openLots - target));
		}
	}
	else if (target < 0) {
		exitShort();
		openLots = lotsOpen();
		if (openLots < abs(target)) {
			Lots = abs(target) - openLots;
			enterLong();
		}
		else if (openLots > abs(target)) {
			exitLong(0,0,(openLots - abs(target)));
		}
	}
	
	else if (target==0){
	exitLong();
	exitShort();
	}
	
	}


}


