#include <profile.c>
#include <r.h>


#define NUM_INSTRUMENTS 4
#define NUM_COMPONENTS 1
#define GROSS_DOLLAR_EXPOSURE 10000
#define REBALANCING_THRESHOLD 0 // CHANGE BACK TO 1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define LOOKBACK 252 // days
#define LOG_FILENAME "target_lots.txt"
#define R_FILENAME "statarb.r"


static int Counter = 0;

static double TargetVolumes[NUM_INSTRUMENTS]; // R uses decimals even for integer order volumes	
int InstrumentIndex;
for(InstrumentIndex = 0; InstrumentIndex < NUM_INSTRUMENTS; InstrumentIndex++) {
	TargetVolumes[InstrumentIndex] = 0;
}


function writeTargetLotsToFile(int *lots) {
	file_write(LOG_FILENAME, strf("anz = %i\ncba = %i\nnab = %i\nwbc = %i\n", lots[0], lots[1], lots[2], lots[3]), 0);
}


function main()
{
	assetList("AssetsIB");
	BarPeriod = 1440;
	BarZone = AEST; // comment this out when backtesting
	// BarOffset = 17 * 60; // in minutes
	BarOffset = 0;
	LookBack = LOOKBACK;	
	// StartDate = 20050510;
	StartDate = 20180101;
	
	set(LOGFILE|PARAMETERS);

	Rstart(R_FILENAME, 2); // debuglevel = 2
	Rset("REBALANCING_THRESHOLD", REBALANCING_THRESHOLD);
}


function run()
{
	while(loop("anz", "cba", "nab", "wbc")) {
		asset(Loop1);
		Spread = RollLong = RollShort = Slippage = 0;
		Commission = -0.08;
		vars prices = series(priceClose(), LOOKBACK);
		Rset(Asset, prices, LOOKBACK);
		// if(Asset == "anz") {
			// printf("\n");
			// printf("Price of %s: %f", Asset, priceClose());
			// printf("\n");
			// printf("\n");
		// }	
	}
		
	Rset("previous_target_volumes", TargetVolumes, NUM_INSTRUMENTS); // needed for when no rebalancing is required

	Counter += 1;
	if(Counter > LOOKBACK) {
		Rx("print(anz)", 3); // print rout to the Zorro window. only prints a maximum of 54 data points. 3 is the mode. this prints floats!
		Rx(strf("rout <- get_target_volumes(anz, cba, nab, wbc, gross_dollar_exposure = %i, n_components = %i)", GROSS_DOLLAR_EXPOSURE, NUM_COMPONENTS));
		Rv("rout", TargetVolumes, NUM_INSTRUMENTS); // read it back into `TargetVolumes`. Any decimal will automatically be rounded to the nearest integer!!!
		// Rx("print(rout)", 3); // print rout to the Zorro window. only prints a maximum of 54 data points. 3 is the mode. this prints floats!

		// convert float array to integer array
		int IntegerTargetVolumes[NUM_INSTRUMENTS];
		int i;
		for(i = 0; i < NUM_INSTRUMENTS; ++i) {
		    IntegerTargetVolumes[i] = (int)TargetVolumes[i];
		}

		writeTargetLotsToFile(IntegerTargetVolumes);

		printf("\n");
		printf("Signal generated on %s (UTC date) at local time %i", strdate(YMD, 0), ltod(AEST, 0)); // YMD is for YYYYMMDD format
		printf("\n");
		printf("Target volume of anz: %i", IntegerTargetVolumes[0]);
		printf("\n");
		printf("Target volume of cba: %i", IntegerTargetVolumes[1]);
		printf("\n");
		printf("Target volume of nab: %i", IntegerTargetVolumes[2]);
		printf("\n");
		printf("Target volume of wbc: %i", IntegerTargetVolumes[3]);
		printf("\n");
	}
}