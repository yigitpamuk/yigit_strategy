

int lotsOpen() {
	string CurrentAsset = Asset;
	int val = 0;
	for(last_trades)
	if(strstr(Asset,CurrentAsset) && TradeIsOpen)
	val += TradeLots;
	return val;
}

function algo_generator(int lookbacks){
	
	vars Close = series(priceClose());	
	int halfLife = lookbacks;
	vars zScore = series(((Close[0] - SMA(Close, halfLife))/StdDev(Close, halfLife))); 

	int openLots;
	



	if (zScore[0] > 0) { 
		exitLong();
		openLots = lotsOpen();
		if (openLots < zScore[0]) {
			Lots = zScore[0] - openLots;
			enterShort();
		}
		else if (openLots > zScore[0]) {
			exitShort(0,0,(openLots - zScore[0]));
		}
	}
	else if (zScore[0] < 0) {
		exitShort();
		openLots = lotsOpen();
		if (openLots < abs(zScore[0])) {
			Lots = abs(zScore[0]) - openLots;
			enterLong();
		}
		else if (openLots > abs(zScore[0])) {
			exitLong(0,0,(openLots - abs(zScore[0])));
		}
	}
	
	


	
}







function run() {
	set(PARAMETERS+LOGFILE);
	BarPeriod = 1440;
	StartDate = 2000;
	EndDate = 2020;
	Weekend = 3;
	NumCores = -2;		
	

	PlotWidth = 750;
	
	LookBack = 21*24;
	assetList("AssetsOANDA");

	while(asset(loop("AUD/NZD","AUD/CAD","GBP/USD","NZD/USD","USD/CHF")))
	while(algo(loop("12M","18M","24M")))
	{


	if(Algo == "3M"){
	algo_generator(21*3);
	}		
	if(Algo == "6M"){
	algo_generator(21*6);
	}		
	if(Algo == "9M"){
	algo_generator(21*9);
	}		
	if(Algo == "12M"){
	algo_generator(21*12);
	}		
	if(Algo == "15M"){
	algo_generator(21*15);
	}		
	if(Algo == "18M"){
	algo_generator(21*18);
	}		
	if(Algo == "21M"){
	algo_generator(21*21);
	}		
	if(Algo == "24M"){
	algo_generator(21*24);
	}			
	}


	

}