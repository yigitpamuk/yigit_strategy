#include <profile.c>
#include <r.h>

#define NUM_INSTRUMENTS 4
#define TARGET_LOTS_FILENAME "target_lots.txt"
#define MARKET_OPENING_TIME 1000
#define MARKET_CLOSING_TIME 1559 // does not apply to half-days such as Christmas Eve!
// 	THIS SCRIPT NEEDS TO BE STOPPED AFTER TRADING HOURS ON HALF TRADING HOURS SUCH AS CHRISTMAS EVE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


// gets current lots for current asset
int getCurrentLots() {
	string CurrentAsset = Asset; // Asset is changed in the for loop  
	int val = 0;
	for(last_trades)    
		if(strstr(Asset, CurrentAsset) && TradeIsOpen)      
			val += TradeLots * ((int) TradeIsLong - (int) TradeIsShort);
	asset(CurrentAsset);
	return val;
}

// gets target lots for all assets
int getTargetLotsFromFile() {
	string content = file_content(TARGET_LOTS_FILENAME);
	return strvar(content, Asset);
}

// when trading is allowed
// market(AEST, 0) didn't work as intended
bool duringTradingHours() {
	int CurrentTime = ltod(AEST, 0);
	// printf("Current time: %i", CurrentTime);
	// printf("\n");
	return !suspended() && (MARKET_OPENING_TIME <= CurrentTime) && (CurrentTime <= MARKET_CLOSING_TIME);
}

// changes the open volume of the current asset from CurrentVolume to TargetVolume
function rebalance(int CurrentVolume, int TargetVolume) {
	if(duringTradingHours()) { // when trading is allowed
		printf("Trading is allowed at current local time: %i", ltod(AEST, 0));
		printf("\n");
		if(TargetVolume > 0) {
			if(CurrentVolume < 0) {
				exitShort();
				CurrentVolume = 0;
			}
			int RequiredVolume = TargetVolume - CurrentVolume;
			if(RequiredVolume > 0) {
				enterLong(RequiredVolume);
			} else if(RequiredVolume < 0) {
				exitLong(0, 0, -RequiredVolume);
			}
		} else if(TargetVolume < 0) {
			if(CurrentVolume > 0) {
				exitLong();
				CurrentVolume = 0;
			}
			int RequiredVolume = TargetVolume - CurrentVolume;
			if(RequiredVolume > 0) {
				exitShort(0, 0, RequiredVolume);
			} else if(RequiredVolume < 0) {
				enterShort(-RequiredVolume);
			}
		} else { // TargetVolume = 0
			exitLong();
			exitShort();
		}
	}
	else {
		printf("Trading is NOT allowed at current local time: %i", ltod(AEST, 0));
		printf("\n");
	}
}



function main()
{
	assetList("AssetsIB");
	BarPeriod = 1; // in minutes
	StartDate = 20190101;
	StartMarket = 1000;
	EndMarket = 1559;
	set(LOGFILE|PARAMETERS);
	set(NFA);
	LookBack = 0;
}


// 1. gets current positions for each instrument from Zorro
// 2. reads target volumes of each instrument from a file
// 3. closes the difference (if any) between the target position and the current position of each instrument
function run()
{
	int InstrumentIndex;
	for(InstrumentIndex = 0; InstrumentIndex < NUM_INSTRUMENTS; InstrumentIndex++) {
		if(InstrumentIndex == 0) {
			asset("anz");
		} else if(InstrumentIndex == 1) {
			asset("cba");
		} else if(InstrumentIndex == 2) {
			asset("nab");
		} else if(InstrumentIndex == 3) {
			asset("wbc");
		}
		Spread = RollLong = RollShort = Slippage = 0;
		// Commission = -0.08;
		Commission = 0;
		
		int CurrentVolume = getCurrentLots();
		int TargetVolume = getTargetLotsFromFile();
		
		printf("\n");
		printf("Current volume of %s: %i", Asset, CurrentVolume);
		printf("\n");
		printf("Target volume of %s:  %i", Asset, TargetVolume);
		printf("\n");

		rebalance(CurrentVolume, TargetVolume);
	}
}