
function tradeCounterTrend()
{
	TimeFrame = 4;
	vars Price = series(price());
	vars Filtered = series(BandPass(Price,optimize(30,20,40),0.5));
	vars Signal = series(FisherN(Filtered,500));
	var Threshold = optimize(1,0.5,1.5,0.1);
	
	Stop = optimize(4,2,10) * ATR(100);
	Trail = 4*ATR(100);
	
	if(crossUnder(Signal,-Threshold)) 
		enterLong(); 
	else if(crossOver(Signal,Threshold)) 
		enterShort();
}

function tradeTrend()
{
	TimeFrame = 1;
	vars Price = series(price());
	vars Trend = series(LowPass(Price,optimize(500,300,700)));

	Stop = optimize(4,2,10) * ATR(100);
	Trail = 0;

	vars MMI_Raw = series(MMI(Price,300));
	vars MMI_Smooth = series(LowPass(MMI_Raw,300));
	
	if(falling(MMI_Smooth)) {
		if(valley(Trend))
			enterLong();
		else if(peak(Trend))
			enterShort();
	}
}

function run()
{
	set(PARAMETERS+FACTORS+LOGFILE); 
	NumCores = -2;		
	BarPeriod = 60;	
	LookBack = 2000;	
	StartDate = 2010;
	EndDate = 2017; 	
	NumWFOCycles = 14; 

	assetList("AssetsOANDA");
	if(ReTrain) {
		UpdateDays = 21*6;	
		SelectWFO = -1;	
		reset(FACTORS);	
	}
	
	
	while(asset(loop("AUD/CAD","AUD/NZD","AUD/USD","EUR/AUD","EUR/CHF","EUR/GBP","EUR/JPY","EUR/USD","GBP/AUD","GBP/CHF","GBP/JPY","GBP/NZD","GBP/USD","USD/CAD","USD/CHF","USD/JPY")))
	while(algo(loop("TRND","CNTR")))
	{


		if(Algo == "TRND"){ 
			if (Loop1=="EUR/USD" || Loop1=="AUD/NZD" || Loop1=="CHF/JPY" || Loop1=="EUR/AUD" || Loop1 == "EUR/JPY" || Loop1 == "GBP/AUD" || Loop1=="GBP/JPY" || Loop1=="USD/CAD" || Loop1 == "USD/JPY" ){
			tradeTrend();
			}
		}
		else if(Algo == "CNTR"){ 
			if (Loop1=="AUD/CAD" || Loop1=="AUD/USD" ||Loop1=="EUR/CHF" ||Loop1=="EUR/GBP" ||Loop1=="EUR/USD" ||  Loop1=="GBP/CHF" ||Loop1=="GBP/NZD" ||Loop1=="GBP/USD" ||Loop1=="USD/CHF" || Loop1=="USD/JPY" ){
			tradeCounterTrend();
			}				
			
		}
	}
	

	
	
	PlotWidth = 600;
	PlotHeight1 = 300;
	set(TESTNOW+LOGFILE);
}