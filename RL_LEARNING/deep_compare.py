import matplotlib.pyplot as plt
from numpy import *
import traceback
from dateutil.parser import parse
import pandas as pd
import pickle

def run_analysis(df,dft,df_lt):
    fid = open('output.dat','r')

    glob_return=[]
    # dft = df.rolling(5).mean()
    pnls = []
    real_pnls = []
    modified_pnls = []
    dirs = []
    lens = []
    dates = []
    period = []
    cnt=0
    winner=0
    loser=0
    dirs_exit_count = 0
    close = asarray(df['close'])
    for i in fid:
        cnt += 1
        # if cnt < 1200:
            # continue
            # break

        if 'pnl' in i:
            l = i.split('|')
            try:
                PNL=float(l[4].split()[1].replace('%',''))
                PNLREAL=float(l[4].split()[3].replace('%',''))
                DIRECTION=int(i.split('pos')[-1].split()[0])
                LEN=int(i.split('len')[-1].split()[0])
                pnls.append(PNL)
                real_pnls.append(PNLREAL)
                dirs.append(DIRECTION)
                lens.append(LEN)
                dates.append(parse(i.split('|')[-1]))
                period.append(int(l[3].split()[1]))







                exit_date_index = (where(df.index==dates[-1]))[0]
                entry_date_index = exit_date_index - period[-1]
                a = close[entry_date_index]
                b = close[exit_date_index]
                real_pnl_calculated = (b-a)/a*dirs[-1]
                # modified_pnls.append(real_pnl_calculated * 100)

                '''
                seperate buys and sells
                '''
                item = [parse(i.split('|')[-1]),dirs[-1],PNL,PNLREAL]

                '''This calculates the forward pnl if we are not in the right direction of the sma'''
                print(dft['close'][exit_date_index])
                if (dirs[-1]>0 and float(dft['close'][exit_date_index])>b[0]):
                    for t in range(min(lens[-1],15)):
                        real_pnl_calculated = (close[exit_date_index+t]-a[0])/a[0]*dirs[-1]
                        if  float(dft['close'][exit_date_index+t])<close[exit_date_index+t]:
                            break
                    print(t)
                elif (dirs[-1]<0 and float(dft['close'][exit_date_index])<b[0]):
                    for t in range(min(lens[-1],15)):
                        real_pnl_calculated = (close[exit_date_index+t]-a[0])/a[0]*dirs[-1]
                        if  float(dft['close'][exit_date_index+t])>close[exit_date_index+t]:
                            break
                    print(t)

                pnl_diff = (float(dft['close'][entry_date_index])-a[0])
                # if ((dirs[-1]>0 and float(dft['close'][entry_date_index])>a[0]) and float(df_lt['close'][entry_date_index])<float(dft['close'][entry_date_index]) or \
                        # (dirs[-1]<0 and float(dft['close'][entry_date_index])<a[0]) and float(df_lt['close'][entry_date_index])>float(dft['close'][entry_date_index])):
                if ((dirs[-1]>0 and float(dft['close'][entry_date_index])>a[0]) or (dirs[-1]<0 and float(dft['close'][entry_date_index])<a[0])):
                    modified_pnls.append(real_pnl_calculated * 100)
                    item.append(real_pnl_calculated * 100)
                else:
                    modified_pnls.append(0)
                    item.append(0)

                print(l)
                glob_return.append(item)

            except:
                print(l,traceback.format_exc())


    n_longs = sum(array(dirs)[array(dirs)>0])
    n_shorts = sum(array(dirs)[array(dirs)<0])
    print('**********************************************************************')
    print('Number of longs/shorts:',n_longs,n_shorts)
    print('profit per trade for SMA pnls: %.4f bps '%(pnls[-1]/len(pnls)))
    # print(std(diff(pnls)/100),std(diff(real_pnls)/100))
    # print(len(where(array(modified_pnls)==0)[0]), len(where(array(modified_pnls)!=0)[0]))
    print('Win ratio for SMA: ',len(diff(pnls)[diff(pnls)>0])/len(diff(pnls)))
    # print('Win ratio for real: ',len(array(modified_pnls)[array(modified_pnls)>0])/len(modified_pnls),mean(modified_pnls),median(modified_pnls))
    print('Mean pnl of modified PnLs: %.5f bps'%(mean(array(modified_pnls)[array(modified_pnls)!=0])))
    # print('Fraction of actual trades for Mod PnL:',len(array(modified_pnls)[array(modified_pnls)!=0])/len(modified_pnls))
    print('**********************************************************************')
    '''
    plt.subplot(311)
    plt.plot(dates,array(pnls)-pnls[0],label='sma')
    plt.plot(dates,array(real_pnls)-real_pnls[0],label='real')
    plt.plot(dates,cumsum(modified_pnls),label='modified')
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid()
    plt.subplot(312)
    plt.plot(dates,array(pnls)-array(real_pnls))
    plt.plot(array(dates)[array(modified_pnls)!=0],(array(pnls)-cumsum(array(modified_pnls)))[array(modified_pnls)!=0])
    plt.subplot(313)
    plt.hist(diff(array(pnls))-diff(array(real_pnls)),25)
    plt.show()
    '''
    buy_sell=pd.DataFrame(glob_return,columns=['DATE','POS','SMAPL','REALPL','MODPL'])
    buy_sell['DATE']=pd.to_datetime(buy_sell['DATE'])

    buy_sell=buy_sell.set_index(['DATE'])

    buy_sell['SMAPL']=buy_sell['SMAPL'].diff()
    buy_sell['REALPL']=buy_sell['REALPL'].diff()
    buy_sell=buy_sell.dropna()
    buy_sell_buy=buy_sell[buy_sell['POS']==1]
    buy_sell_sell=buy_sell[buy_sell['POS']==-1]

    buy_sell_buy['SMAPL']=buy_sell_buy['SMAPL'].cumsum()
    buy_sell_buy['REALPL']=buy_sell_buy['REALPL'].cumsum()
    buy_sell_buy['MODPL']=buy_sell_buy['MODPL'].cumsum()

    buy_sell_sell['SMAPL']=buy_sell_sell['SMAPL'].cumsum()
    buy_sell_sell['REALPL']=buy_sell_sell['REALPL'].cumsum()
    buy_sell_sell['MODPL']=buy_sell_sell['MODPL'].cumsum()

    plot1=pd.DataFrame()
    plot1['DATE']=dates
    plot1['REAL']=array(real_pnls)-real_pnls[0]
    plot1['SMA']=array(pnls)-pnls[0]
    plot1['MOD']=cumsum(modified_pnls)
    print(plot1)
    print(plot1.dtypes)
    plot1=plot1.set_index(['DATE'])
    plot1=plot1.loc[plot1.index >= '2015-01-01 00:00:00']
    buy_sell_sell=buy_sell_sell.loc[buy_sell_sell.index >= '2015-01-01 00:00:00']
    buy_sell_buy=buy_sell_buy.loc[buy_sell_buy.index > '2015-01-01 00:00:00']

    plot1['REAL']=plot1['REAL']-plot1['REAL'].iloc[0]
    plot1['SMA']=plot1['SMA']-plot1['SMA'].iloc[0]
    plot1['MOD']=plot1['MOD']-plot1['MOD'].iloc[0]

    buy_sell_sell['SMAPL']=buy_sell_sell['SMAPL'].iloc[0]
    buy_sell_sell['REALPL']=buy_sell_sell['REALPL'].iloc[0]
    #buy_sell_sell['MODPL']=buy_sell_sell['MODPL'].iloc[0]

    buy_sell_buy['SMAPL']=buy_sell_buy['SMAPL'].iloc[0]
    buy_sell_buy['REALPL']=buy_sell_buy['REALPL'].iloc[0]
   # buy_sell_buy['MODPL']=buy_sell_buy['MODPL'].iloc[0]

    pickle.dump(plot1,open('comp_result.pick','wb'))
    pickle.dump(buy_sell_buy,open('compbuy_result.pick','wb'))
    pickle.dump(buy_sell_sell,open('compsell_result.pick','wb'))

    plt.subplot(311)
    plt.plot(plot1['SMA'],label='sma')
    plt.plot(plot1['REAL'],label='real')
    plt.plot(plot1['MOD'],label='modified')
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid()


    plt.subplot(312)
    plt.title('SELL ONLY')
    plt.plot(buy_sell_sell['SMAPL'],label='sma')
    plt.plot(buy_sell_sell['REALPL'],label='real')
    #plt.plot(buy_sell_sell['MODPL'],label='modified')
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid()


    plt.subplot(313)
    plt.title('BUY ONLY')
    plt.plot(buy_sell_buy['SMAPL'],label='sma')
    plt.plot(buy_sell_buy['REALPL'],label='real')
    #plt.plot(buy_sell_buy['MODPL'],label='modified')
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid()
    plt.show()

def get_data():
    df=pd.read_csv("EURUSD.csv")
    df.columns = ['Time','open','high','low','close']
    df=df.set_index(['Time'])
    df.index=pd.to_datetime(df.index)
    df['volume']=0
    df = df['close'].resample('60T',label='right',closed='right').ohlc().dropna()
    df['volume']=0
    df=df.loc[df.index > '2005-01-01 00:00:00']
    return df

def get_data_AAPL():
    dateparse = lambda x,y: pd.datetime.strptime(x+' '+y, '%m/%d/%Y %H:%M')
    df = pd.read_csv('AAPL.txt',parse_dates=[[0,1]],index_col=0,skiprows=0,date_parser=dateparse)
    df.index.rename('Time',inplace=True)
    df.columns = ['open','high','low','close','volume']
    dft = df['close'].rolling(5).mean()
    df=df.loc[df.index > '2000-01-01 00:00:00']
    return df

# def
my_data=pd.read_csv('SPY.csv')
my_data.columns=['Time','bid','aa','ask','bb']
my_data['close']=(my_data['bid']+my_data['ask'])/2
my_data=my_data[['close','Time']]
my_data['Time']=pd.to_datetime(my_data['Time'])
my_data=my_data.set_index(['Time'])
my_data_res = my_data['close'].resample('2T',label='right',closed='right').ohlc().dropna()
print(my_data_res)
df=my_data_res.copy()
df['volume']=0
pickle.dump(df,open('data.pick','wb'))
df = pickle.load(open('data.pick','rb'))
dft = pd.read_csv('KAL.csv')
pickle.dump(dft,open('data_ma.pick','wb'))
dft = pickle.load(open('data_ma.pick','rb'))
# df_lt = pickle.load(open('data_lt_ma.pick','rb'))
# dft = df.rolling(5).mean()
# pickle.dump(dft,open('data_ma.pick','wb'))
df_lt = df.rolling(20).mean()
pickle.dump(df_lt,open('data_lt_ma.pick','wb'))

print(df.dtypes)
print(dft.dtypes)
run_analysis(df,dft,df_lt)
