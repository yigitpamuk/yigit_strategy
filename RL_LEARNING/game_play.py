import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from datetime import datetime, timedelta
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import sgd
import talib
import traceback
from pykalman import KalmanFilter


#DIFFERENT SMOOTHING FUNTIONS
def OnlineKalman(dataframe,obs_covariance):

    response=pd.DataFrame(columns=dataframe.columns,index=dataframe.index)

    for x in dataframe.columns:
        kf = KalmanFilter(
            transition_matrices=[1],
            observation_matrices=[1],
            initial_state_mean=0,
            initial_state_covariance=1,
            observation_covariance=obs_covariance,
            transition_covariance=0.01,
            random_state=0)

        my_data=dataframe[str(x)].values
        n_timesteps = my_data.shape[0]
        n_dim_state = np.array([1]).shape[0]
        filtered_state_means = np.zeros((n_timesteps, n_dim_state))
        filtered_state_covariances = np.zeros((n_timesteps, n_dim_state, n_dim_state))

        for t in range(n_timesteps - 1):
            if t == 0:
                filtered_state_means[t] = 0
                filtered_state_covariances[t] =1
            filtered_state_means[t + 1], filtered_state_covariances[t + 1] = (
                kf.filter_update(
                    filtered_state_means[t],
                    filtered_state_covariances[t],
                    my_data[t + 1],
                    transition_offset=np.array([0]),
                )
            )
        response[x]=filtered_state_means
    return response

def rolling_window(a, step):
    shape   = a.shape[:-1] + (a.shape[-1] - step + 1, step)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def get_kf_value(y_values):
    kf = KalmanFilter()
    Kc, Ke = kf.em(y_values, n_iter=1).smooth(0)
    return Kc




def ZMA(data, period = 5):
        '''
        ZMA - Ehler's Zero-lag Moving Average, an EMA with a correction term for removing lag.
        http://www.financial-hacker.com/trend-delusion-or-reality/
        '''
        # close = df[['close']].values.ravel()
        a = 2.0 / (1 + period)
        ema = talib.EMA(data, period)
        error = 10000000
        gain = 5
        gain_limit = 5
        best_gain = 0

        zma = np.zeros(data.shape)
        for i in range(len(zma)):
            if i < period - 1:
                continue
            else:

                gain = -gain_limit
                while gain < gain_limit:
                    gain += 0.1
                    zma[i] = a * (ema[i] + gain * (data[i] - zma[i - 1])) + (1 - a) * zma[i - 1]
                    new_error = data[i] - zma[i]
                    if np.abs(error) < new_error:
                        error = np.abs(new_error)
                        best_gain = gain

                zma[i] = a * (ema[i] + best_gain * (data[i] - zma[i - 1])) + (1 - a) * zma[i - 1]

        # df_zma = pd.DataFrame(data=zma, index=df.index, columns=['zma_{i}'.format(i=period)])
        return zma[-1]

def SuperSmooth(data, period=5) :
        '''
        SuperSmooth - proposed by John Ehler to get rid of undersample noise.
        '''
        # close = df[['close']].values.ravel()
        f = (1.414*np.pi)/period
        a = np.exp(-f)
        c2 = 2 * a * np.cos(f)
        c3 = - a*a
        c1 = 1 - c2 - c3

        ss = np.zeros(data.shape)
        for i in range(len(data)) :
            if i < 2:
                continue
            else :
                ss[i] = c1 * (data[i] + data[i - 1]) * 0.5 + c2 * ss[i - 1] + c3 * ss[i - 2]
        # df_ss = pd.DataFrame(data=ss, index=df.index, columns=['ss_{i}'.format(i=period)])
        return ss[-1]



def LowPass(data, period = 5):
    '''
    LowPass filter - John Elher's smoothing filter to get the trend of the time series
    '''
    # close = df[['close']].values.ravel()
    a = 2.0 / (1+period)
    lp = data.copy()

    for i in range(len(data)) :
        if i < 2:
            continue
        else :
            lp[i] = (a - 0.25*a*a) * data[i] \
                + 0.5*a*a * data[i - 1] \
                - (a-0.75*a*a) * data[i - 2] \
                + 2*(1.0-a)*lp[i-1] \
                - (1.0 -a)*(1.0 -a)*lp[i-2]
    # df_lp = pd.DataFrame(data=lp, index=df.index, columns=['lp_{i}'.format(i=period)])
    return lp[-1]
def ALMA(data, period=5) :
    '''
    ALMA - Arnaud Legoux Moving Average,
    http://www.financial-hacker.com/trend-delusion-or-reality/
    '''
    # data = df[['close']].values.ravel()
    m = np.floor(0.85 * (period - 1))
    s = period  / 6.0
    alma = np.zeros(data.shape)
    w_sum = np.zeros(data.shape)

    for i in range(len(data)):
        if i < period - 1:
            continue
        else:
            for j in range(period):
                w = np.exp(-(j-m)*(j-m)/(2*s*s))
                alma[i] += data[i - period + j] * w
                w_sum[i] += w
            alma[i] = alma[i] / w_sum[i]

    # df_alma = pd.DataFrame(data=alma, index=df.index, columns=['alma_{i}'.format(i=period)])
    return alma[-1]

class ExperienceReplay(object):
    '''This class gathers and delivers the experience'''
    def __init__(self, max_memory=100, discount=.9):
        self.max_memory = max_memory
        self.memory = list()
        self.discount = discount

    def remember(self, states, game_over):
        # memory[i] = [[state_t, action_t, reward_t, state_t+1], game_over?]
        self.memory.append([states, game_over])
        if len(self.memory) > self.max_memory:
            del self.memory[0]

    def get_batch(self, model, batch_size=10):
        len_memory = len(self.memory)
        num_actions = model.output_shape[-1]
        env_dim = self.memory[0][0][0].shape[1]
        inputs = np.zeros((min(len_memory, batch_size), env_dim))
        targets = np.zeros((inputs.shape[0], num_actions))
        for i, idx in enumerate(np.random.randint(0, len_memory, size=inputs.shape[0])):
            state_t, action_t, reward_t, state_tp1 = self.memory[idx][0]
            game_over = self.memory[idx][1]

            inputs[i:i+1] = state_t
            # There should be no target values for actions not taken.
            # Thou shalt not correct actions not taken #deep
            targets[i] = model.predict(state_t)[0]
            Q_sa = np.max(model.predict(state_tp1)[0])
            if game_over:  # if game_over is True
                targets[i, action_t] = reward_t
            else:
                # reward_t + gamma * max_a' Q(s', a')
                targets[i, action_t] = reward_t + self.discount * Q_sa
        return inputs, targets



class Game(object):
    '''This is the game. It starts, then takes an action (buy or sell) at some point and finally the reverse
    action, at which point it is game over. This is where the reward is given. The state consists of a vector
    with different bar sizes for OLHC. They are just concatenated.
    lkbk: determines how many bars to use - larger lkbk - bigger state
    '''
    def __init__(self, df, df_real, lkbk=20, max_game_len=1000, run_mode='sequential', init_idx=None):
        self.df = df
        self.df_real = df_real
        self.lkbk = lkbk
        self.max_game_len = max_game_len

        self.is_over = False
        self.reward = 0
        self.run_mode =  run_mode
        self.pnl_sum = 0
        if run_mode == 'sequential' and init_idx == None:
            print('------No init_idx set for "sequential": stopping------')
            return
        else:
            self.init_idx = init_idx
        self.reset()

    def _update_state(self, action):

        '''Here we update our state'''
        self.curr_idx += 1

        self.curr_time = self.df.index[self.curr_idx]
        self.curr_price = self.df['close'][self.curr_idx]
        self.curr_price_real = self.df_real['close'][self.curr_idx]
        self.pnl = (-self.entry + self.curr_price)*self.position/self.entry
        self.pnl_real = (-self.entry_real + self.curr_price_real)*self.position/self.entry_real
        self._assemble_state()
        _k = list(map(float,str(self.curr_time.time()).split(':')[:2]))
        self._time_of_day = (_k[0]*60 + _k[1])/(24*60)
        self._day_of_week  = self.curr_time.weekday()/6
        self.norm_epoch = (df.index[self.curr_idx]-df.index[0]).total_seconds()/self.t_in_secs

        '''This is where we define our policy and update our position'''
        if action == 0:
            pass

        elif action == 2:
            if self.position == -1:
                self.is_over = True
                self._get_reward()
                self.trade_len = self.curr_idx - self.start_idx

            elif self.position == 0:
                self.position = 1
                self.entry = self.curr_price
                self.entry_real = self.curr_price_real
                self.start_idx = self.curr_idx
            else:
                pass

        elif action == 1:
            if self.position == 1:
                self.is_over = True
                self._get_reward()
                self.trade_len = self.curr_idx - self.start_idx

            elif self.position == 0:
                self.position = -1
                self.entry = self.curr_price
                self.entry_real = self.curr_price_real
                self.start_idx = self.curr_idx
            else:
                pass


    def _assemble_state(self):
        '''Here we can add other things such as indicators and times'''
        self._get_last_N_timebars()
        bars = [self.last5m,self.last1h,self.last1d]
        state = []
        candles = {j:{k:np.array([]) for k in ['open','high','low','close']} for j in range(len(bars))}
        for j,bar in enumerate(bars):
            for col in ['open','high','low','close']:
                candles[j][col] = np.asarray(bar[col])
                state += (list(np.asarray(bar[col]))[-10:])


        self.state = np.array([])
        self.state = np.append(self.state,state)
        self.state = np.append(self.state,self.position)
        np.append(self.state,np.sign(self.pnl_sum))
        self.state = np.append(self.state,self._time_of_day)
        self.state = np.append(self.state,self._day_of_week)


        for c in candles:
            try:
                sma1 = talib.SMA(candles[c]['close'],self.lkbk-1)[-1]
                sma2 = talib.SMA(candles[c]['close'],self.lkbk-8)[-1]
                self.state = np.append(self.state,(sma1-sma2)/sma2)
                self.state = np.append(self.state,sma1)
                self.state = np.append(self.state,talib.RSI(candles[c]['close'],self.lkbk-1)[-1])
                self.state = np.append(self.state,talib.MOM(candles[c]['close'],self.lkbk-1)[-1])
   #             self.state = np.append(self.state,talib.BOP(candles[c]['open'],
#                                               candles[c]['high'],
 #                                              candles[c]['low'],
  #                                             candles[c]['close'])[-1])

    #            self.state = np.append(self.state,talib.AROONOSC(candles[c]['high'],
     #                                          candles[c]['low'],
      #                                         self.lkbk-3)[-1])
            except: print(traceback.format_exc())
        #print('-->',self.state)
        self.state = (np.array(self.state)-np.mean(self.state))/np.std(self.state)





    def _get_last_N_timebars(self):
        '''The lengths of the time windows are currently hardcoded.'''
        # TODO: find better way to calculate window lengths
        wdw5m = 9
        wdw1h = np.ceil(self.lkbk*15/24.)
        wdw1d = np.ceil(self.lkbk*15)

        self.last5m = self.df[self.curr_time-timedelta(wdw5m):self.curr_time].iloc[-self.lkbk:]
        self.last1h = self.bars1h[self.curr_time-timedelta(wdw1h):self.curr_time].iloc[-self.lkbk:]
        self.last1d = self.bars1d[self.curr_time-timedelta(wdw1d):self.curr_time].iloc[-self.lkbk:]

        '''Making sure that window lengths are sufficient'''
        try:
            assert(len(self.last5m)==self.lkbk)
            assert(len(self.last1h)==self.lkbk)
            assert(len(self.last1d)==self.lkbk)

        except:
            print('****Window length too short****')
            print(len(self.last5m),len(self.last1h),len(self.last1d))
            print(self.last5m,self.last1d,self.last1h)
            if self.run_mode == 'sequential':
                self.init_idx = self.curr_idx
                self.reset()
            else:
                self.reset()


    def _get_reward(self):
        if self.position == 1 and self.is_over:
            pnl = (-self.curr_price + self.entry)/self.entry
            self.reward = (pnl)#-(self.curr_idx - self.start_idx)/1000.
        elif self.position == -1 and self.is_over:
            pnl = (self.curr_price - self.entry)/self.entry
            self.reward = (pnl)#-(self.curr_idx - self.start_idx)/1000.
        return self.reward

    def observe(self):
        return np.array([self.state])

    def act(self, action):
        self._update_state(action)
        reward = self.reward
        game_over = self.is_over
        return self.observe(), reward, game_over

    def reset(self):
        self.pnl = 0
        self.pnl_real = 0
        self.entry = 0
        self.entry_real = 0
        self._time_of_day = 0
        self._day_of_week = 0

        if self.run_mode == 'random':
            self.curr_idx = np.random.randint(0,len(df)-3000)

        elif self.run_mode == 'sequential':
            self.curr_idx = self.init_idx

        self.t_in_secs = (df.index[-1]-df.index[0]).total_seconds()
        self.start_idx = self.curr_idx
        self.curr_time = self.df.index[self.curr_idx]
        self.bars1h = df['close'].resample('1H',label='right',closed='right').ohlc().dropna()
        self.bars1d = df['close'].resample('1D',label='right',closed='right').ohlc().dropna()
        self._get_last_N_timebars()
        self.state = []
        self.position = 0
        self._update_state(0)


def run(df,df_real,fname):
    # parameters
    epsilon_0 = .001

    MAX_GAME_LEN=100
    num_actions = 3
    epoch = 115000
    max_memory = 10000

    batch_size = 50
    lkbk = 10
    START_IDX = 3000

    env = Game(df, df_real, lkbk=lkbk, max_game_len=1000,init_idx=START_IDX,run_mode='sequential')
    #Game is init here
    #bars are prepared
    print("STATE")
    print(env.state)
    hidden_size = len(env.state)*2

    model = Sequential()
    model.add(Dense(hidden_size, input_shape=(len(env.state),), activation='relu'))
    model.add(Dense(hidden_size, activation='relu'))
    model.add(Dense(num_actions))
    model.compile(sgd(lr=.005), "mse")

    # If you want to continue training from a previous model, just uncomment the line bellow
    #model.load_weights("indicator_model.h5")

    # Initialize experience replay object
    exp_replay = ExperienceReplay(max_memory=max_memory)

    # Train
    win_cnt = 0
    loss_cnt = 0
    wins = []
    losses = []
    pnls = []
    pnls_real = []
    for e in range(epoch):
        epsilon = epsilon_0**(np.log10(e))
        if np.random.rand()<0.01: epsilon = epsilon_0


        #PREVENT IDX ERROR WHEN THERE IS NO MORE DATA
        if (env.curr_idx+MAX_GAME_LEN+1) > len(df):
            raise Exception("DataFinished")

        env = Game(df, df_real, lkbk=lkbk, max_game_len=10000,init_idx=env.curr_idx,run_mode='sequential')



        loss = 0.
        env.reset()
        game_over = False
        # get initial input
        input_t = env.observe()

        cnt = 0
        while not game_over:
            cnt += 1
            input_tm1 = input_t
            # get next action

            if np.random.rand() <= epsilon:# and env.position == 0:
                action = np.random.randint(0, num_actions, size=1)[0]
                if env.position == 0:
                    if action == 2:
                        exit_action = 1
                    elif action == 1:
                        exit_action = 2


            elif env.position == 0:
                q = model.predict(input_tm1)
                print('PREDICTED')
                print(q)
                action = np.argmax(q[0])
                print('ACTION')
                print(action)
                if action:
                    #print(cnt)
                    exit_action = np.argmin(q[0][1:])+1

            elif cnt > MAX_GAME_LEN:
                #print('***Time Exit***')
                action = exit_action

            elif env.position:
                q = model.predict(input_tm1)
                action = np.argmax(q[0])

            # apply action, get rewards and new state
            input_t, reward, game_over = env.act(action)
            if reward > 0:
                win_cnt += 1
            elif reward < 0:
                loss_cnt += 1

            # store experience
            #QQQ-> why we are remembering if random is below 10%?

            if action or len(exp_replay.memory)<20 or np.random.rand() < 0.1:
                exp_replay.remember([input_tm1, action, reward, input_t], game_over)

            inputs, targets = exp_replay.get_batch(model, batch_size=batch_size)
            env.pnl_sum = sum(pnls)

            zz = model.train_on_batch(inputs, targets)
            loss += zz
        #QQQ - > Why we are reporting pnl before appending it.
        prt_str = ("Epoch {:03d} | Loss {:.2f} | pos {} | len {} | pnl {:.2f}% - {:.2f}% | eps {:,.4f} | {}".format(e,
                                                                                      loss,
                                                                                      env.position,
                                                                                      env.trade_len,
                                                                                      sum(pnls)*100,
                                                                                      sum(pnls_real)*100,
                                                                                      epsilon,
                                                                                      env.curr_time



                                                                                      ))

        print(prt_str)
        fid = open(fname,'a')
        fid.write(prt_str+'\n')
        fid.close()
        pnls.append(env.pnl)
        pnls_real.append(env.pnl_real)
        if not e%10:
            print('----saving weights-----')
            model.save_weights("indicator_model.h5", overwrite=True)


if __name__ == "__main__":
    my_data=pd.read_csv('~/yigit/experiments/2000_SPY/SPY.csv')
    my_data.columns=['Time','bid','aa','ask','bb']
    my_data['close']=(my_data['bid']+my_data['ask'])/2
    my_data=my_data[['close','Time']]
    my_data['Time']=pd.to_datetime(my_data['Time'])
    my_data=my_data.set_index(['Time'])

    #my_data_res = my_data['close'].resample('1T').ohlc().dropna()
    #print(my_data_res)

    df=my_data.copy()
    df['volume']=0
    #df=df.loc[df.index > '2000-01-01 00:00:00']
    #time filter
    #get online kf data
    plt.plot(df['close'])
    plt.show()
    print(asda)
    dft=OnlineKalman(df,0.05)

    dft.to_csv('KAL.csv')

    #uncomment below to preload KF data
    #dft=pd.read_csv('KAL.csv')
    #dft=dft.set_index(['Time'])
    #dft.index=pd.to_datetime(dft.index)



    np.random.seed(34)
    fname = 'output.dat'
    fid = open(fname,'w')
    fid.close()
    try:
        run(dft,df,fname)
    except: print(traceback.format_exc())


